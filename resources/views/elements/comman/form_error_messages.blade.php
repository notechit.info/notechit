<?php
/**
 * Form Error Message
 * This is used for display all the error message of the form server side validation
 */
?>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@include('elements.comman.flash_message')