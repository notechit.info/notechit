<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCategory extends Model
{
    protected $fillable = [
        'user_id',
        'category_id',
        'activated',
        'deleted'
    ];

    public function scopeActive($query)
    {
        return $query->where('user_categories.activated',self::ACTIVE);
    }

    public static function getUserCategories($nUserId)
    {
        return UserCategory::from('user_categories as uc')
                ->leftJoin('categories as c','uc.category_id','=','c.id')
                ->where('uc.activated',1)
                ->where('uc.deleted',0)
                ->where('uc.user_id',$nUserId)
                ->select(
                    'c.id as category_id',
                    'c.category_name as category_name'
                )->get();

    }
}
