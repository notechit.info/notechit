<!-- <li class="" data-action-type="simple" onClick="selectBucket(this)">Saved Notes</li> -->

@if(count($oBuckets) > 0)
@foreach($oBuckets as $oBucket)
<li class="" data-bucket-name="{{ $oBucket->bucket_name }}" data-action-type="folder" onClick="selectBucket(this,{{ $oBucket->id }})">
    <img class="bucket-icon" src="{{asset('web/img/bucket-icon.svg')}}" alt="Bucket-icon">
    <span class="d-inline-block align-top">
        {{ $oBucket->bucket_name }}
        <div class="nc-light-color nc-xs-text">{{ $oBucket->bucketNotes()->count() }} Notes</div>
    </span>
</li>
@endforeach
@else
<li style="text-align:center;">
    <span class="d-inline-block align-top">
        No folders available!
    </span>
</li>
@endif