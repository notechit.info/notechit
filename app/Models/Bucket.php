<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bucket extends Model
{
    const IN_ACTIVE = 0 , ACTIVE = 1;
    const DEFAULT_BUCKET_NAME = "My Bucket";
    
    protected $fillable = [
        'user_id',
        'bucket_name',
        'default_bucket',
        'activated',
        'deleted'
    ];

    public function scopeActive($query)
    {
        return $query->where('buckets.activated',self::ACTIVE);
    }

    public function bucketNotes(){
        return $this->hasMany(BucketNotes::class)->where('activated','=', 1);
    }
    
    /**
    * Create default bucket
    */
    public static function defaultBucket($userId){
        self::create([
            'user_id'       => $userId,
            'bucket_name'   => self::DEFAULT_BUCKET_NAME,
            'default_bucket'=> 1
        ]);
    }

    /**
     * get Default bucket data
     * @param int $userId
     * 
     * @return Collection $data 
     */
    public static function getDefaultBucket($userId){
        return self::where([
                'user_id'           =>  $userId,
                'bucket_name'       =>  self::DEFAULT_BUCKET_NAME,
                'default_bucket'    =>  1,
            ])
            ->latest()
            ->first();
    }

}
