@if(count($allBuckets) > 0)
    @foreach($allBuckets as $bucket)
        @if($bucket->id != $currentNoteBucket->bucket_id)
            <li class="" data-bucket-name="{{ $bucket->bucket_name }}" data-action-type="folder" onClick="selectForMoveBucket(this,{{ $bucket->id }})">
                <img class="bucket-icon" src="{{asset('web/img/bucket-icon.svg')}}" alt="Bucket-icon">
                <span class="d-inline-block align-top">
                    {{ $bucket->bucket_name }}
                    <div class="nc-light-color nc-xs-text">{{ $bucket->bucketNotes()->count() }} Notes</div>
                </span>
            </li>
        @endif
    @endforeach
@else
    <li style="text-align:center;">
        <span class="d-inline-block align-top">
            No folders available!
        </span>
    </li>
@endif