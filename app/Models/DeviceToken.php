<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeviceToken extends Model
{
    const ANDROID = 'A' ,IOS = 'I';

    protected $fillable = [
        'device_token',
        'user_id',
        'device_type',
        'activated',
        'deleted'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeAndroid($query)
    {
        return $query->where("device_tokens.device_type",self::ANDROID);
    }

    public function scopeIos($query)
    {
        return $query->where("device_tokens.device_type",self::IOS);
    }
}
