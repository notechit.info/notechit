<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aNames = array('HINDI','ENGLISH','MATHS','SCIENCE','HISTORY','ECONOMICS','ACCOUNT AND FINANCE','SANSKRIT AND VEDAS','AYURVEDA','HOMEOPATHY','UNANI','TECHNOLOGY','ART AND CRAFT','ENTERTAINMENT','QUOTES','FOOD & RECIPES','MUSIC','YOGA','SPORTS','PUZZLE','PUBLICATION','HEALTH','MEDICINES','LEGAL','CURRENT AFFAIRS','FORMATS AND PROFORMA','PLANTATION AND ENVIROMENT','IDEAS AND INNOVATION','DESIGNS & LAYOUTS','TRAVELS & MAPS','ASTROLOGY','SPORTS','BUISNESS','RELIGION AND SPIRITUALITY','BEAUTY AND MAKE OVER','PETS','PROGRAMMING','CASE STUDY');

        foreach ($aNames as $name) {
            
            $oUser = \App\Models\Category::firstOrNew([
                'category_name'  => $name,
                'activated' => 1,
                'deleted'   => 0
            ]);

            $oUser->save(); 
        }
    }
}