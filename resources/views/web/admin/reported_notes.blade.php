@extends('web.admin.layout')
@section('title')
    Reported Notes
@endsection
@section('page_title')
    Reported Notes
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table id="reported-notes-datatable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Note Title</th>
                            <th>No of Reportes</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="reportModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Note Reports</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="reportModalBody"></div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    var dt = $('#reported-notes-datatable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: {
            url:'{{route("admin.reported.notes.datatable")}}',
            type:'POST',
        },
        columns: [
            {data: 'DT_RowIndex'},
            {data: 'title'},
            {data: 'reports_count'},
            {data: 'action'},
        ]
    });
    $(document).on('click', '.viewNoteReport', function(){
        $('#reportModal').modal('hide');
        $('#reportModalBody').empty();
        $.ajax({
            type:'POST',
            url: "{{route('admin.reported.notes.get_data')}}",
            data: {
                id:$(this).data('id'),
            },
            success: (data) => {
                str = '<div class="table-responsive"><table class="table table-striped"><thead><tr><th>Name</th><th>E-mail</th><th>Problem</th><th>Reason</th></tr></thead><tbody>'
                $.each(data, function(index, value){
                    str += '<tr><td>'+value.user.first_name+' '+value.user.last_name+'</td><td>'+value.user.email+'</td><td>'+value.problem+'</td><td>'+value.reason+'</td></tr>';
                });
                str += '</tbody></table></div>';
                
                $('#reportModalBody').html(str);
                $('#reportModal').modal('show');
            },
            error: function(data){
                toastr.warning(data);
            }
        });
    });
</script>
@endsection