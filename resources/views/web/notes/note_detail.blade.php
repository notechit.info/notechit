@extends('web.main')

@section('title')
    {{ trans('messages.note_detail') }}
@endsection

@section('content')

<div class="container">
    <div class="note-wrapper">

        <?php
            $bIsNoteDetailVisible = false;
            if($oNote->note_type == \App\Models\Note::PVT)
            {   
                if(\Auth::user())
                {
                    if($oNote->user_id == \Auth::user()->id)
                    {
                        $bIsNoteDetailVisible = true; 
                    }
                }
            }

            if($oNote->note_type == \App\Models\Note::FO && $isFollwed == true)
            {
                $bIsNoteDetailVisible = true;
            }

            if($oNote->note_type == \App\Models\Note::PUB)
            {
                $bIsNoteDetailVisible = true;
            }

            if($isSelfNote){
                $bIsNoteDetailVisible = true;
            }
        ?>
       
        @if($bIsNoteDetailVisible)
        <div class="container-fluid">

            <div class="row justify-content-between">
                <div class="col-md-10 col-sm-12 note-detail-title nc-lg-text">
                    {{ $oNote->title }}
                </div> 
                <div class="col-md-2 col-sm-12 note-date float-md-right float-sm-none text-right">{{ \Carbon\Carbon::parse($oNote->created_at)->format('d/m/Y') }}</div>

            </div>

            <div class="row note-info-wrapper">
                <div class="col-lg-8 col-md-12">
                    <ul class="note-secondary-details">
                        <li class="author-info "> 
                            <span class="author-thumbnail-wrapper align-middle">
                                @if(!empty($oNote->user_file_name) || !empty($oNote->user_profile_pic_url))
                                    <a href="{{ route('web.user.profile',['id' => $oNote->user_id]) }}">
                                        <img class="img-fluid" src="{{ getUserImageUrl($oNote->user_file_name,$oNote->user_profile_pic_url) }}" alt="note-cover">
                                    </a>
                                @else
                                    <a href="{{ route('web.user.profile',['id' => $oNote->user_id]) }}" class="">
                                        <span class="user-initials">{{ mb_substr($oNote->first_name, 0, 1) . mb_substr($oNote->last_name, 0, 1) }}</span>
                                    </a>
                                @endif
                            </span>

                            <span class="author-name nc-sm-text align-middle">
                                <a href="{{ route('web.user.profile',['id' => $oNote->user_id]) }}">
                                 <span>{{ getUserName($oNote->first_name,$oNote->last_name) }}</span>
                                </a>
                            </span>
                        </li>
                        
                        @if($oNote->total_views > 0)
                        <li class="note-views  ">
                            <a href="javascript:void(0)" class="note-view-users cursor-pointor d-inline-block" data-id="{{$oNote->id}}" data-count="{{ $oNote->total_views }}">{{ $oNote->total_views }} @lang('messages.views')</a>
                        </li>
                        @endif

                        @if($oNote->total_downloads > 0)
                        <li class="">{{ $oNote->total_downloads }} @lang('messages.downloads')</li>
                        @endif
                        
                        <li class="note-rating  nc-xs-text">
                            
                            @if($nAvgRating > 0)

                                <?php 
                                $nTotalR = 5;
                                $nRemainingTotal = $nTotalR - $nAvgRating;
                                for($i=0;$i<$nAvgRating;$i++)
                                { 
                                ?>
                                    <span class="fa fa-star nc-black-color"></span>
                                <?php 
                                }
                                ?>

                                <?php 
                                for($i=0;$i<$nRemainingTotal;$i++)
                                { 
                                ?>
                                    <span class="fa fa-star-o "></span>
                                <?php 
                                }
                                ?>

                            @else
                            <!-- <span class="fa fa-star nc-black-color"></span> -->
                            <span class="fa fa-star-o "></span>
                            <span class="fa fa-star-o"></span>
                            <span class="fa fa-star-o"></span>
                            <span class="fa fa-star-o"></span>
                            <span class="fa fa-star-o"></span>
                            @endif
                        </li>

                        <!--if(isset($oNote->bucket_name) && !empty($oNote->bucket_name))
                            <li>
                                <span class=" nc-xs-text font-weight-bold ">@lang('messages.note_is_saved')</span>
                                <button class="nc-black-btn nc-xs-text nc-radius n-js-fav" data-action="remove" data-id="{{ $oNote->id }}">@lang('messages.remove')</button>
                            </li>
                        endif-->
                        
                    </ul>
                </div>
                <div class="col-lg-4 col-md-12">
                    <ul class="note-actions float-lg-right float-md-none">
                        @if(isset($oNote->bucket_name) && !empty($oNote->bucket_name)  && isset($oNote->bn_deleted) && $oNote->bn_deleted != 1)
                        <li><span class="nc-xs-text font-weight-bold" data-note-id="{{ $oNote->note_id }}" data-action="add" data-id="{{ $oNote->note_id }}"> <span class="fa fa-folder mr-1"></span>{{ $oNote->bucket_name }}</span></li>
                        @else
                        <li>
                                <button class="nc-black-btn nc-xs-text nc-radius js-bucket-modal" 
                                        data-note-id="{{ $oNote->id }}" data-action="add" data-id="{{ $oNote->id }}"
                                        data-toggle="tooltip" data-placement="bottom" data-save-type="details" title="@lang('messages.save_note_tooltip')">
                                @lang('messages.save')</button>
                        </li>
                        @endif 

                        @if($oNote->note_type != 'PVT')
                        <li class="cursor-pointor jsClassClipboard" data-toggle="tooltip" data-placement="bottom" title="@lang('messages.share_note')">
                            <!-- <img class="img-fluid" src="{{asset('web/img/share.svg')}}" alt="@lang('messages.share_note')"> -->
                            <i class="fas fa-share-alt"></i>
                        </li>
                        @endif

                        @if(Auth::user() && Auth::user()->id == $oNote->user_id)
                        <li>
                            <a href="{{ route('web.note.add-note',['nNoteId' => $oNote->id]) }}"
                            data-toggle="tooltip" data-placement="bottom" title="@lang('messages.edit_note_tooltip')">
                                <span class=" nc-sm-text nc-radius" data-action="edit">
                                    <i class="fas fa-pencil-alt"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <span class=" cursor-pointor nc-sm-text nc-radius n-js-delete" data-action="delete" data-id="{{ $oNote->id }}"
                                    data-toggle="tooltip" data-placement="bottom" title="@lang('messages.delete_note_tooltip')">
                                <i class="fas fa-trash-alt"></i>
                            </span>
                        </li>
                        @endif

                        <!-- <li class="cursor-pointor" data-toggle="tooltip" data-placement="bottom" title="@lang('messages.download_note')">
                            <a href="{{ route('web.note.download-note',['nNoteId' => $oNote->id]) }}" target="_blank"> 
                            <img class="img-fluid" src="{{asset('web/img/download.svg')}}" alt="@lang('messages.download')"></a>
                        </li> -->

                        @if(Auth::user() && Auth::user()->id != $oNote->user_id)
                            <li class="cursor-pointor js-report" data-toggle="tooltip" data-placement="bottom" title="@lang('messages.report_note')">
                                <img class="" src="{{asset('web/img/flag.svg')}}" alt="@lang('messages.report_note')">
                            </li>
                        @else
                            @if(!Auth::user())
                            <li class="cursor-pointor js-report" data-toggle="tooltip" data-placement="bottom" title="@lang('messages.report_note')">
                                <img class="img-fluid" src="{{asset('web/img/flag.svg')}}" alt="@lang('messages.report_note')">
                            </li>
                            @endif
                        @endif

                    </ul>
                </div>
            </div>

            <div class="row">   
                <div class="note-tag-wrapper my-3">
                    <?php 
                        $ran = array('nc-tag-bg-1', 'nc-tag-bg-2', 'nc-tag-bg-3');
                    ?>
                    @foreach($oNote->note_categories as $category)
                        <a href="{{route('web.home.category', $category->url)}}" class="note-tag {{ $ran[array_rand($ran, 1)] }}"><img src="{{getCategoryImageUrl($category->image)}}" alt="{{$category->category_name }}" width="30px" height="30px"> {{$category->category_name }}</a>
                    @endforeach
                </div>
            </div>
              
            <div class="row note-detail-content">
                <div class="col">
                    {!! $oNote->description !!}
                </div>
            </div>

            @if(count($oNote->attachments) != 0)
                <hr>
                <div class="form-group">
                    <div>
                        <b>Attachments ({{ count($oNote->attachments) }})</b> 
                        @auth
                            <a href="{{ route('web.note.download-note',['nNoteId' => $oNote->id]) }}" target="_blank">
                            <span class="float-md-right d-inline-block download-btn">
                                    <i class="fas fa-download"></i>
                                    @lang('messages.download_all_attachments')
                            </span>
                            </a>
                        @endauth
                    </div>
                    <ul class="note-attachments-list">
                        <?php 
                            $array = array('PNG','png','JPG','JPEG','jpg','jpeg','GIF','gif');
                            $videoArray = array('video/mp4'=>'mp4', 'video/webm'=>'webm');
                        ?>
                        @foreach($oNote->attachments as $i => $oAttachment)
                        <li class=""  data-toggle="modal" data-target="#displayFileModal">
                            <div class="file-type">
                                <i class="fas fa-file" ></i>
                                {{ pathinfo($oAttachment->display_file_name, PATHINFO_EXTENSION) }}</div>
                            <br>
                            
                            @if(in_array(pathinfo($oAttachment->display_file_name, PATHINFO_EXTENSION),$array))
                                <img class="img-fluid" src="{{ getNoteAttachmentUrl($oAttachment->file_name) }}" alt=""/>
                            @elseif(in_array(pathinfo($oAttachment->display_file_name, PATHINFO_EXTENSION), $videoArray))
                                @php 
                                    $videoType = array_search(pathinfo($oAttachment->display_file_name, PATHINFO_EXTENSION), $videoArray)
                                @endphp
                                <div id="video_container">
                                    <video controls>
                                        <source src="{{ getNoteAttachmentUrl($oAttachment->file_name) }}" type="{{$videoType}}">
                                        Your browser does not support the video tag.
                                    </video>
                                </div> 
                            @else
                                {{ $oAttachment->display_file_name }}
                            @endif
                            
                            @auth
                                <a href="{{ getNoteAttachmentUrl($oAttachment->file_name) }}" target="_blank" download>
                                    <img class="img-fluid" src="{{asset('web/img/download.svg')}}" alt="@lang('messages.download')"
                                    data-toggle="tooltip" data-placement="bottom" title="@lang('messages.download')">
                                </a>
                            @endauth

                            <!-- <a href="{{ route('web.note.download',['id' => $oAttachment->id]) }}" target="_blank">
                                <img class="img-fluid" src="{{asset('web/img/download.svg')}}" alt="@lang('messages.download')"
                                data-toggle="tooltip" data-placement="bottom" title="@lang('messages.download')">
                            </a> -->
                        </li>
                        @endforeach
                        <!-- <li class="">
                        <div class="file-type">png</div>

                            <img class="img-fluid" src="{{asset('web/img/chit-recepi.png')}}" alt=""/>
                            
                            <a href="#">
                                <img class="img-fluid" src="{{asset('web/img/download.svg')}}" alt="@lang('messages.download')"
                                data-toggle="tooltip" data-placement="bottom" title="@lang('messages.download')">
                                
                            </a>
                        </li> -->
                    </ul>
                </div>
            @endif
            
            @if(\Auth::user() && $oNote->user_id != \Auth::user()->id)
            <div class="row">
                <div class="col">
                <div class="text-center nc-xs-text font-weight-bold position-relative rate-title mt-4">
                    <div class="devider"></div>    
                    <span class="">
                        @lang('messages.rate_this_note')
                    </span>
                </div>
                <div class="text-center nc-lg-text mt-2"> 
                    @lang('messages.your_rating_encourage_author')
                </div>
                <div class="rating"> 
                    <input type="radio" class="n-js-rating" name="rating" value="5" id="5" {{ $nRating == 5 ? 'checked' : '' }}><label for="5">☆</label> 
                    <input type="radio" class="n-js-rating" name="rating" value="4" id="4" {{ $nRating == 4 ? 'checked' : '' }}><label for="4">☆</label> 
                    <input type="radio" class="n-js-rating" name="rating" value="3" id="3" {{ $nRating == 3 ? 'checked' : '' }}><label for="3">☆</label> 
                    <input type="radio" class="n-js-rating" name="rating" value="2" id="2" {{ $nRating == 2 ? 'checked' : '' }}><label for="2">☆</label> 
                    <input type="radio" class="n-js-rating" name="rating" value="1" id="1" {{ $nRating == 1 ? 'checked' : '' }}><label for="1">☆</label>
                </div>
                </div>
            </div>
            @endif

        </div>
        @endif

        @if($oNote->note_type == \App\Models\Note::PVT && $bIsNoteDetailVisible == false)
            <div class="text-center">Access denied!</div>
        @endif

        @if($oNote->note_type == \App\Models\Note::FO && $bIsNoteDetailVisible == false)
            <div class="text-center">
                This note is for followers only by
                @if(Auth::user()) 
                    <a href="{{ route('web.user.profile',['id' => $oNote->user_id]) }}" class="mb-3 d-inline-block">
                        <span><u>  {{ getUserName($oNote->first_name,$oNote->last_name) }} </u></span>
                    </a>
                    <br>
                    @if($isFollowRequested)
                        <button class=" nc-black-btn btn-sm mb-2" >Request Sent</button>
                    @else
                        <button class=" nc-black-btn btn-sm mb-2 n-js-follow" data-id="{{$oNote->user_id}}" data-action="follow">Follow </button>
                    @endif
                @endif
        @endif
    </div>
</div>

<!-- ===== Report Note Popup ===== START -->

<!-- Modal -->
<div class="modal fade md-modal" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="reportModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

        <div class="nc-xlg-text modal-title text-center ">@lang('messages.report_note')</div>

        <div id="report-alert"></div>
        <form id="report_form" action="{{ route('web.note.add-report') }}" method="POST">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="">@lang('messages.select_the_problem_with_note')</label>
                <select name="problem" id="problem" class="form-control">
                    <option value="" selected disabled>Please select</option>
                    <option value="Abuse Content">Abuse Content</option>
                    <option value="Violence">Violence</option>
                    <option value="Spam">Spam</option>
                    <option value="Sales">Sales</option>
                    <option value="Other">Other</option>
                </select>
            </div>

            <div class="form-group">
                <label for="">@lang('messages.write_your_reason')</label>
                <textarea class="form-control" name="reason" id="" cols="30" rows="4"></textarea>
                <input type="hidden" name="note_id" value="{{ $oNote->id }}">
            </div>
            
            <button id="report_form_submit_btn" class="nc-black-btn nc-register-btn">@lang('messages.submit')</button>

        </form>
      </div>
      
    </div>
  </div>
</div>
<!-- ===== Report Note Popup ===== END -->

<!-- ===== Display File Popup ===== START -->

<!-- Modal -->
<div class="modal fade md-modal display-file-modal" id="displayFileModal" tabindex="-1" role="dialog" aria-labelledby="displayFileModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="docume    nt">
    <div class="modal-content">
      <div class="modal-body">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

       
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                @php
                    $i = 0;
                @endphp
                @foreach($oNote->attachments as $i => $oAttachment)
                    <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}" ></li>
                    @php
                        $i++;
                    @endphp
                @endforeach

            </ol>
            <div class="carousel-inner">
                <?php 
                    $array = array('PNG','png','JPG','JPEG','jpg','jpeg','GIF','gif');
                    $videoArray = array('video/mp4'=>'mp4', 'video/webm'=>'webm');
                ?>

                @foreach($oNote->attachments as $i => $oAttachment)
                <div class="carousel-item"  >
                    @if(in_array(pathinfo($oAttachment->display_file_name, PATHINFO_EXTENSION),$array))
                        <img class="img-fluid" src="{{ getNoteAttachmentUrl($oAttachment->file_name) }}" alt=""/>
                    @elseif(in_array(pathinfo($oAttachment->display_file_name, PATHINFO_EXTENSION), $videoArray))
                        @php 
                            $videoType = array_search(pathinfo($oAttachment->display_file_name, PATHINFO_EXTENSION), $videoArray)
                        @endphp
                        <div id="video_container">
                            <video controls>
                                <source src="{{ getNoteAttachmentUrl($oAttachment->file_name) }}" type="{{$videoType}}">
                                Your browser does not support the video tag.
                            </video>
                        </div> 
                    @else
                        <div class="file-type-preview">
                            <div class="file-type">
                                <i class="fas fa-file"></i>
                                {{ pathinfo($oAttachment->display_file_name, PATHINFO_EXTENSION) }}</div>
                            <br>
                            {{ $oAttachment->display_file_name }}

                            @auth
                                <a href="{{ getNoteAttachmentUrl($oAttachment->file_name) }}" target="_blank" download>
                                    <i class="fas fa-download"></i>
                                </a>
                            @endauth
                        </div>
                    @endif
                    <!-- <a href="{{ route('web.note.download',['id' => $oAttachment->id]) }}" target="_blank">
                        <img class="img-fluid" src="{{asset('web/img/download.svg')}}" alt="@lang('messages.download')"
                        data-toggle="tooltip" data-placement="bottom" title="@lang('messages.download')">
                    </a> -->
                </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <!-- <span class="carousel-control-prev-icon" aria-hidden="true"></span> -->
                <i class="fa fa-arrow-circle-left fa-2x " aria-hidden="true"></i>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <!-- <span class="carousel-control-next-icon" aria-hidden="true"></span> -->
                <i class="fa fa-arrow-circle-right fa-2x" aria-hidden="true"></i>
                <span class="sr-only">Next</span>
            </a>
            </div>
     
       
      </div>
      
    </div>
  </div>
</div>
<!-- ===== Display File Popup ===== END -->

<div class="modal fade" id="viewUserList" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 id="exampleModalLabel">Views</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body view-user-list-modal-body" id="viewUserListData">

        </div>
      </div>
    </div>
</div>
@include('web.notes._bucket_modal')

<script>
    $("body").on("click",".js-report",function(e){
        e.preventDefault();
        var auth = "{{ \Auth::user() ? 1 : 0}}";
        if(auth == 0)
        {
            showToast("Please sign in to report this note!");
            return false;
        }
        $('#reportModal').modal('toggle');
    });
    // ======== Add active class to image preview slider start ============
    
    $('#displayFileModal').on('show.bs.modal', function (event) {
        console.log("hello modal");
        var nTriggerElementIndex = $(event.relatedTarget).index()+1;
        console.log(nTriggerElementIndex);
        $(".carousel-inner .carousel-item:nth-child("+nTriggerElementIndex+")").addClass('active');
        $(".carousel-indicators li:nth-child("+nTriggerElementIndex+")").addClass('active');
    });
    // ======== Add active class to image preview slider end ============
    // ======== copy to clipboard js start ============
    var $temp = $("<input>");
    var $url = $(location).attr('href');

    $('.jsClassClipboard').on('click', function() {
        $("body").append($temp);
        $temp.val($url).select();
        document.execCommand("copy");
        $temp.remove();
        $("p").text("");
        showToast("URL copied!");
    })
    // ======== copy to clipboard js end ============

    $('body').on("click", '.n-js-delete', function(e) {
        var nNoteId = $(this).attr('data-id');
        _deleteNote(nNoteId);
    });

    function _deleteNote(nNoteId) {

        swal({
            title: "Are you sure you want to delete?",
            text: "You will not be able to recover note!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {

                $.ajax({
                    method : "POST",
                    url: "{{ route('web.note.delete-note') }}",
                    data : {
                        _token : "{{ csrf_token() }}",
                        note_id : nNoteId
                    },
                    cache: false,
                    success: function(response){
                        swal("Note has been deleted!",{
                            icon: "success",
                        }).then(function() {
                            window.location.href = "{{ route('web.note.my-notes') }}";
                        });
                    },error : function(error){ 
                        console.log('Error on delete note');
                    }
                });
                return false;

            } else {
                swal("Your note is safe!");
            }
        });
    }


    $("body").on("click",".n-js-rating",function(){
        var auth = "{{ \Auth::user() ? 1 : 0}}";
        if(auth == 0)
        {
            showToast("Please sign in to rate this note!");
            return false;
        }

        var star = $(this).val();
        $.ajax({
            method : "GET",
            url: "{{ route('web.note.add-rating') }}",
            data : { 
                star : star,
                note_id : {{ $oNote->id }}
            },
            cache: false,
            success: function(response){
                if(response.status == 1 && response.bRatingExists == 0)
                {
                    showToast("{{trans('messages.add_rating_success') }}");
                }
                else if(response.bRatingExists == 1)
                {
                    showToast("{{trans('messages.already_rated') }}");
                }
            },error : function(error){ console.log('Error');}
        });

    });//End of click

    /** Report Form */
    var reportOptions = { 
            beforeSubmit:  showReportRequest,
            success:       showReportResponse,
            error:showReportError,
            dataType: 'json' 
    }; 

    function showReportRequest() {
        $("#validation-errors").hide().empty();
        return true;
    }

    function showReportResponse(response) {
        if(response.status == 0)
        {
          $.each(response.errors, function(key,val){
            $('#'+key).addClass('error');
          });

          return false;
        }

        var sHtml = '';
        sHtml +='<div class="alert alert-success" id="success-alert">'
        sHtml +='<button type="button" class="close" data-dismiss="alert">x</button>'
        sHtml +='<strong>Success!</strong>'
        sHtml +=' '+response.message+' '
        sHtml +='</div>'

        $('#report-alert').html(sHtml);

        $('#report_form').trigger("reset");

        $('#problem').prop('selectedIndex',0);

        setTimeout(() => {
            $('#success-alert').remove();
        }, 2000);

        setTimeout(() => {
            $('#reportModal').modal('toggle');
        }, 1000);
        
    }

    function showReportError(xhr) {
        console.log("inside error");
    }

    function removeReportFormErrorClass()
    {
        $('#fo_email').removeClass('error');
    }

    $('body').on('click','#report_form_submit_btn',function(e){
        e.preventDefault();
        var auth = "{{ \Auth::user() ? 1 : 0}}";
        if(auth == 0)
        {
            showToast("{{ trans('messages.sign_in_validation') }}");
            return false;
        }

        removeFpFormErrorClass();
        $('#report_form').ajaxForm(reportOptions).submit();
        return false;
    });
    /**Submit Fp form*/
    $('.note-view-users').click(function(){
        nNoteId = $(this).data('id');
        count = $(this).data('count');
        if(count != 0){
            $.ajax({
                method : "POST",
                url: "{{ route('web.note.note-view-data') }}",
                data : {
                    _token : "{{ csrf_token() }}",
                    note_id : nNoteId
                },
                cache: false,
                success: function(response){
                    htmlContent = '<ul>';
                    $.each(response, function(index, value){
                        htmlContent += '<li class="author-info border-bottom p-1"><span class="author-thumbnail-wrapper align-middle mr-2 "><a href="/profile/'+value.id+'"><img class="img-fluid" src="'+value.image+'" alt="Profile Picture '+value.name+'"></a></span><span class="author-name nc-sm-text align-middle"><a href="/profile/'+value.uid+'"><span>'+value.name+'</span></a></span></li>';
                    });
                    htmlContent += '</ul>';
                    $('#viewUserListData').html(htmlContent);
                    $('#viewUserList').modal('show');
                },error : function(error){ 
                    console.log('Error on getting views data');
                }
            });
        }
    });

    /*Follow User Logic*/
    $("body").on("click",".n-js-follow",function(){
        var auth = "{{ \Auth::user() ? 1 : 0}}";
        if(auth == 0)
        {
            showToast("Please sign in to follow!");
        }

        var user_id = $(this).attr('data-id');
        var action = $(this).attr('data-action');

        $.ajax({
            method : "GET",
            url: "{{ route('web.user.follow') }}",
            data : { 
                user_id : user_id,
                action : action
            },
            cache: false,
            success: function(response){
                if(response.status == 1)
                {
                    showToast(response.message);
                    setTimeout(() => {
                        window.location.reload();
                    }, 1000);
                }
            },error : function(error){ console.log('Error');}
        });
    });

</script>

@endsection