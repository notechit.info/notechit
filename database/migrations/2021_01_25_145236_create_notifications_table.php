<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('from_user_id')->unsigned();
            $table->bigInteger('entity_id')->unsigned()->nullable();
            $table->enum('action',['FOLLOW','UNFOLLOW','FOLLOWING','REJECT'])->nullable();
            $table->enum('type',['FF', 'NOTE'])->nullable()->comment('FF : For follow, NOTE: for shared note');
            // $table->enum('ff_status',['PENDING','ACCEPT','DECLINE'])->nullable();
            $table->boolean('attempt')->nullable();
            $table->boolean('activated')->default(1)->index();
            $table->enum('is_read', ['0','1'])->default('0');
            $table->boolean('deleted')->default(0)->index();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('from_user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
