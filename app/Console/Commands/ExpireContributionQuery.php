<?php

namespace App\Console\Commands;

use App\Models\ContributionQuery;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ExpireContributionQuery extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expire:query';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Function will expire contribution query after specific time on daily basis';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ContributionQuery::where('is_expired',0)
            ->whereDate('created_at', '<=', Carbon::now()->subDay(config('constants.CONTRIBUTION_QUERY_EXPIRE_DAYS')))
            ->update([
                'is_expired'=>1,
            ]);
        return 0;
    }
}
