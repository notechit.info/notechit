<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;
use Exception;

class UserController extends Controller
{
    public function index(){
        return view('web.admin.users');
    }

    public function datatable(Request $request){
        $data = User::select(
                'id',
                'username',
                'email',
                'verified',
                'activated',
                'created_at',
            )
            ->withCount('notes')
            ->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('date', function ($data) {
                return $data->created_at->format('d-m-Y');
            })
            ->addColumn('action', function ($data) {
                $str = '';
                if($data->verified != User::VERIFIED){
                    $str .= '<button class="btn btn-primary btn-sm mr-2 verifyEmail" data-id="'.$data->id.'"><i class="fa-solid fas fa-check"></i></button>';
                }
                if($data->activated == User::ACTIVE)
                {
                    $str .= '<button class="btn btn-success btn-sm changeStatus" data-id="'.$data->id.'" data-type="Active"><i class="fa-solid fas fa-user-check"></i></button>';
                }else{
                    $str .= '<button class="btn btn-danger btn-sm changeStatus" data-id="'.$data->id.'" data-type="Inactive"><i class="fa-solid fas fa-ban"></i></button>';
                }
                return $str;
            })
            ->make(true);
    }

    public function verifyEmail(Request $request){
        try{
            $user = User::find($request->id);
            if($user->verified != User::VERIFIED){
                $user->verified = User::VERIFIED;
                $user->email_verified_at = Carbon::now();
                $user->save();
            }
            return response()->json('User Email Verified.', 200);    
        }catch(Exception $e){
            return response()->json('Something went wrong.', 500);
        }
    }

    public function changeStatus(Request $request){
        try{
            $user = User::find($request->id);
            if($request->type == "Active"){
                $user->activated = User::INACTIVE;
            }else{
                $user->activated = User::ACTIVE;
            }
            $user->save();
            return response()->json('Success', 200);    
        }catch(Exception $e){
            return response()->json('Something went wrong.', 500);
        }
    }
}
