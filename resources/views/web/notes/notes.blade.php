@php
    $i = 1;
@endphp
@foreach($oNotes as $oNote)
<div class="col-lg-4 col-md-6 col-sm-12 note-card-bg-{{$i}}">
    @php
    if($i > 11){
        $i=1;
    }
    else{
        $i++;
    }
    @endphp
    @include('web.notes.single_note_card')
</div>
@endforeach

@include('web.notes._bucket_modal')
