<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContributionQueryCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contribution_query_comments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('contribution_query_id')->constrained();
            $table->foreignId('user_id')->constrained();
            $table->text('comment')->comment('Notechit Note URL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contribution_query_comments');
    }
}
