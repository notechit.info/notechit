<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Language;
use Illuminate\Http\Request;
use DataTables;
use Exception;

class LanguageController extends Controller
{
    public function index()
    {
        return view('web.admin.language.index');
    }

    public function datatable(){
        $data = Language::withCount('notes')
            ->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('image', function ($data) {
                return '-';
            })
            ->addColumn('action', function ($data) {
                $str = '';
                if($data->deleted == 0){
                    $str .= '<button class="btn btn-info btn-sm mr-2 editLanguage" data-id="'.$data->id.'" data-name="'.$data->name.'"><i class="fa-solid fas fa-edit"></i></button>';
                    $str .= '<button class="btn btn-danger btn-sm mr-2 deleteLanguage" data-id="'.$data->id.'"><i class="fa-solid fas fa-trash"></i></button>';
                }
                return $str;
            })
            ->make(true);

    }

    public function store(Request $request)
    {
        $language = Language::where(['name'=>$request->language])->active()->latest()->first();
        if(! $language){
            Language::create([
                'name' => $request->language,
            ]);

            return response()->json([
                'status' => 'Success',
                'message' => 'Language Created Successfully.'
            ], 200);
        }
        return response()->json([
            'status' => 'Error',
            'message' => 'Language Already Exists'
        ],200);
    }

    public function update(Request $request, $id)
    {
        $language = Language::where(['name'=>$request->language])
            ->where('id','!=',$id)
            ->active()
            ->latest()
            ->first();

        if(! $language){
            $language = Language::where('id',$id)->update(['name'=>$request->language]);
            return response()->json([
                'status' => 'Success',
                'message' => 'Language Updated Successfully.'
            ], 200);
        }
        return response()->json([
            'status' => 'Error',
            'message' => 'Language Name Already Exists'
        ],200);
    }

    public function destroy($id)
    {
        try{
            $data = Language::find($id);
            $data->deleted = 1;
            $data->save();

            return response()->json('Language Deleted Successfully.', 200);
        }catch(Exception $e){
            return response()->json('Something went wrong', 500);
        }
    }
}
