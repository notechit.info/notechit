<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::group(['namespace' => 'Web','as' => 'web.'], function () {

    //Home Page Routes
    Route::group(['as' => 'home.'], function () {

        Route::get('facebook', function () {
            return view('facebook');
        });
        Route::get('auth/facebook', 'FacebookController@redirectToFacebook')->name('facebook.login');
        Route::get('auth/facebook/callback', 'FacebookController@handleFacebookCallback');

        Route::get('/redirect', 'LoginController@redirectToProvider')->name('google.login');;
        Route::get('/callback', 'LoginController@handleProviderCallback');

        Route::get('/',     'HomeController@home')->name('index');

        Route::get('page/home',  'HomeController@home')->name('index');

        Route::get('/chits',             'HomeController@noteChits')->name('chits');

        Route::get('/about-us',      'HomeController@aboutUs')->name('about-us');

        Route::get('/contact-us',      'HomeController@contactUs')->name('contact-us');
        Route::post('/add-contact-us',      'HomeController@createContactUs')->name('add-contact');

        Route::get('/404',      'HomeController@notFound')->name('404');
        Route::get('/all-queries',      'HomeController@allQueries')->name('all-queries');
        Route::get('/my-queries',      'HomeController@myQueries')->name('my-queries');
        Route::get('/new-query',      'HomeController@newQuery')->name('new-query');
        Route::post('/add-query',      'HomeController@addQuery')->name('add-query');
        Route::get('/remove-query/{id}', 'HomeController@deleteQuery')->name('delete-query');

        Route::get('/category/{url}', 'HomeController@categoryNotes')->name('category');
    });

    //Notes without login
    Route::group(['as' => 'note.'], function () {
        Route::get('note/detail/{nNoteId?}',       'NoteController@noteDetailAction')->name('note-detail');

        Route::get('note/my-notes',        'NoteController@myNotesAction')->name('my-notes');
        Route::get('note/following-users-notes',  'NoteController@followingUsersNotesAction')->name('following-users-notes');
        Route::get('note/saved-notes',     'NoteController@savedNotesAction')->name('saved-notes');
        Route::get('search-notes/{keyword?}',     'NoteController@searchNoteAction')->name('search-notes');
        Route::post('note/get-note-view-data',     'NoteController@getNoteViewData')->name('note-view-data');
    });

    //USER
    Route::group(['as' => 'user.'], function () {
        Route::post('signup',               'UserController@SignUpAction')->name('signup');
        Route::post('login',                'UserController@SignInAction')->name('signin');
        Route::get('verification/{key?}',   'UserController@EmailVerificationAction')->name('verification');
        Route::post('forgot-password',      'UserController@ForgotPasswordAction')->name('forgot-password');
        Route::match(['GET','POST'],'reset-password/{key?}','UserController@ResetPasswordAction')->name('reset-password');

        Route::get('user/view-profile/{id}',     'UserController@userProfileForFollowAction')->name('user-view-profile');
        Route::match(['GET','POST'],'profile/{id}',  'UserController@userProfileAction')->name('profile');
        Route::match(['GET','POST'],'followers',  'UserController@getFollowersAction')->name('get-followers');
    });

    /**
     * After Login All routes
     */
    Route::group(['middleware' => 'webAuth'], function () {

        //User After Login
        Route::group(['as' => 'user.'], function () {

            Route::get('logout','UserController@LogoutAction')->name('logout');

            Route::match(['GET','POST'],'change-password',  'UserController@changePasswordAction')->name('change-password');
            Route::post('upload-profile-picture',           'UserController@uploadProfilePictureAction')->name('upload-profile-picture');

            Route::get('user/follow',                       'UserController@followAction')->name('follow');
            Route::get('user/add-area-of-interest',         'UserController@userAddAreaOfInterestAction')->name('add-area-of-interest');

            Route::get('user/update-area-of-interest',      'UserController@userUpdateAreaOfInterestAction')->name('update-area-of-interest');
            Route::get('user/get-notifications',            'UserController@getNotifications')->name('get-notifications');
            Route::post('user/update-notification-status',  'UserController@updateNotificationStatus')->name('update-notification-status');

        });

        //Note After Login
        Route::group(['as' => 'note.'], function () {

            Route::match(['GET','POST'],'note/add-note/{nNoteId?}', 'NoteController@addNoteAction')->name('add-note');

            Route::get('note/add-to-favourite','NoteController@addNoteToFavouriteAction')->name('add-to-favourite');
            Route::get('note/add-rating',      'NoteController@addRatingAction')->name('add-rating');

            Route::post('note/add-report',      'NoteController@addReportAction')->name('add-report');
            Route::get('note/download-note/{nNoteId}',      'NoteController@callDownloadZipAction')->name('download-note');

            Route::post('note/delete-note',      'NoteController@callDeleteNoteAction')->name('delete-note');
            Route::post('note/delete-note-attachment',      'NoteController@callDeleteNoteAttachmentAction')->name('delete-attachment');

            Route::get('note/download-action',      'NoteController@downloadFileFromS3')->name('download');

            Route::get('note/remove-from-folder','NoteController@removeNoteFromFolderAction')->name('remove-note-from-folder');
        });

        Route::group(['as' => 'bucket.'], function () {
            Route::get('user-buckets','BucketController@userBucket')->name('user-buckets');
            Route::get('get-user-buckets','BucketController@getUserBucket')->name('get-user-buckets');
            Route::post('bucket/create','BucketController@createBucket')->name('create-bucket');
            Route::post('bucket/delete','BucketController@deleteBucket')->name('delete-bucket');
            Route::post('bucket/save-note-to-user-bucket','BucketController@saveNoteToUserBucket')->name('save-to-user-bucket');
            Route::get('bucket/get-bucket-notes','BucketController@getBucketNotes')->name('get-bucket-notes');
            Route::get('get-user-buckets-list-move','BucketController@getUserBucketToMove')->name('get-user-buckets-list-move');
            Route::post('move-note-bucket','BucketController@moveNoteBucket')->name('move-note-bucket');
        });


        //Contribute Query suggestion
        Route::post('/add-query-suggestion', 'ContributionQueryCommentController@add')->name('contribution-query-comment-add');
        Route::post('/remove-query-suggestion', 'ContributionQueryCommentController@delete')->name('contribution-query-comment-remove');
        Route::get('/get-query-suggestion-comments', 'ContributionQueryCommentController@getCommentData')->name('get-query-suggestion-comments');
       

    }); //End of middleware check

});

/**
 * Admin Routes
 */
Route::get('/admin/login', 'Admin\AuthController@showLogin')->name('admin.show_login');
Route::post('/admin/login', 'Admin\AuthController@login')->name('admin.login');

Route::group(['namespace' => 'Admin','as' => 'admin.', 'prefix' => 'admin/', 'middleware' => 'auth:admin'], function () {
    Route::get('/', 'HomeController@index')->name('home');

    //Users
    Route::get('/users', 'UserController@index')->name('users');
    Route::post('/users/datatable', 'UserController@datatable')->name('users.datatable');
    Route::post('/users/verify-email', 'UserController@verifyEmail')->name('users.verify_email');
    Route::post('/users/change-status', 'UserController@changeStatus')->name('users.change_status');
   
    //Notes
    Route::get('/notes', 'NoteController@index')->name('notes');
    Route::post('/notes/datatable', 'NoteController@datatable')->name('notes.datatable');
    Route::post('/notes/delete', 'NoteController@delete')->name('notes.delete');
    Route::get('/reported-notes', 'NoteController@reportedNotes')->name('reported.notes');
    Route::post('/reported-notes-datatable', 'NoteController@reportedNoteDatatable')->name('reported.notes.datatable');
    Route::post('/get-reported-note-data', 'NoteController@getReportedNoteData')->name('reported.notes.get_data');

    //Category
    Route::get('/category', 'CategoryController@index')->name('category');
    Route::post('/category/datatable', 'CategoryController@datatable')->name('category.datatable');
    Route::post('/category/store', 'CategoryController@store')->name('category.store');
    Route::get('/category/edit/{id}', 'CategoryController@edit')->name('category.edit');
    Route::post('/category/update', 'CategoryController@update')->name('category.update');
    Route::post('/category/delete', 'CategoryController@destroy')->name('category.delete');
    Route::post('/category/check-name', 'CategoryController@checkName')->name('category.check_name');

    //Language
    Route::post('/language/datatable', 'LanguageController@datatable')->name('language.datatable');
    Route::resource('language', 'LanguageController')->only([
        'index', 'store', 'update', 'destroy'
    ]);;

    Route::get('/logout', 'AuthController@logout')->name('logout');
});