<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NoteDownload extends Model
{
    const IN_ACTIVE = 0 , ACTIVE = 1;

    protected $fillable = [
        'note_id',
        'user_id',
        'activated',
        'deleted'
    ];

    public function scopeActive($query)
    {
        return $query->where('note_downloads.activated',self::ACTIVE);
    }

    /**
     * Add download - insert data
     */
    public static function saveDownloadEntry($nNoteId = NULL,$nUserId = NULL)
    {
        if(empty($nNoteId) || empty($nUserId))
        {
            return false;
        } 

        $oNoteView = NoteDownload::firstOrNew([
            'note_id'   => $nNoteId,
            'user_id'   => $nUserId
        ]);

        $oNoteView->activated = self::ACTIVE;
        $oNoteView->deleted   = self::IN_ACTIVE;
        $oNoteView->save();
        
        return true;
    }
}
