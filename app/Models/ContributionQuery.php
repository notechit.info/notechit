<?php

namespace App\Models;

use App\ContributionQueryComment;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Category;
use App\Models\ContributionQueryCategory;

class ContributionQuery extends Model
{
    const IN_ACTIVE = 0 , ACTIVE = 1;

    protected $fillable = [
        'user_id',
        'query_text',
        'activated',
        'deleted'
    ];

    protected $appends = [
        'expiry_date',
    ];

    public function scopeActive($query)
    {
        return $query->where('activated',self::ACTIVE);
    }

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Get the categories for the query post.
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, ContributionQueryCategory::class,
      'contribution_query_id', 'category_id');
    }

    /**
     * Add query expiry date to model data
     */
    public function getExpiryDateAttribute(){
        return $this->created_at->addDays(config('constants.CONTRIBUTION_QUERY_EXPIRE_DAYS')); 
    }

    public function comments(){
        return $this->hasMany(ContributionQueryComment::class);
    }
}
