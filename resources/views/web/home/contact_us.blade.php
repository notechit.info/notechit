@extends('web.custom_layout')

@section('title')
    {{ trans('messages.home_page') }}
@endsection

@section('header')
    @include('web.primary_header')
    @include('web.login_popup')

@endsection

@section('content')
  <div class="container-fluid">
    
    <div class="row">
        <div class="col about-text-wrapper text-center p-5">
          <div class="sub-title-text nc-vibrant-color font-weight-bold">
              We would love to hear from you ..!
          </div>
          <div class="sub-text">
            For any Queries, Suggestions and Feedback, Feel free to get in touch
          </div>
          <div class="p-3">
              <span class="px-3">
                  <i class="nc-vibrant-color pr-1 fas fa-mobile-alt"></i>
                  +91-7828101007 
              </span>
              <span class="px-3">
              <i class="nc-vibrant-color pr-1 far fa-envelope"></i>
                <a href="mailto:notechit.info@gmail.com" class="nc-black-color nc-hover-vibrant">notechit.info@gmail.com</a>  
              </span>
          </div>
        </div>
    </div>

    <div class="row bordered-section">
      <div class="col-md-6 offset-md-3 p-5">
          <form id="contact_form" class="px-5" add-contact-form" action="{{ route('web.home.add-contact') }}" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}

            <div class="form-group">
              <label for="note_title"> @lang('messages.name') :</label>
              <input type="text" autocomplete="off" name="name" class="form-control" id="name" autoFocus
                  aria-describedby="emailHelp" placeholder="" value="{{ old('name') }}">
                  @foreach($errors->get('name') as $error)
                    <span class="help-block text-danger">{{ $error }}</span>
                  @endforeach
            </div>

            <div class="form-group">
              <label for="note_title"> @lang('messages.email') :</label>
              <input type="text" autocomplete="off" name="email" class="form-control" id="email" autoFocus
                  aria-describedby="emailHelp" placeholder="" value="{{ old('email') }}">
                  @foreach($errors->get('email') as $error)
                    <span class="help-block text-danger">{{ $error }}</span>
                  @endforeach
            </div>

            <div class="form-group">
              <label for="choose_one"> @lang('messages.choose_one') :</label>
              <select name="query" id="" class="form-control">
                  <option value="Query">Query</option>
                  <option value="Suggestion">Suggestion</option>
                  <option value="Feedback">Feedback</option>
                  <option value="Other">Other</option>
              </select>
              @foreach($errors->get('query') as $error)
                    <span class="help-block text-danger">{{ $error }}</span>
                  @endforeach
            </div>

            <div class="form-group">
              <label for="message_box">@lang('messages.your_message') :</label>
              <textarea name="message" class="form-control" id="message_box" rows="3">{{ old('message') }}</textarea>
              @foreach($errors->get('message') as $error)
                    <span class="help-block text-danger">{{ $error }}</span>
                  @endforeach
            </div>

            <input type="submit" id="" class="nc-black-btn nc-save-btn my-3 mx-auto" value="@lang('messages.submit')" >

          </form>
      </div>
    </div>

    
   
  </div>
  
@endsection