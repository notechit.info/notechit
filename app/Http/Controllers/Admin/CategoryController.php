<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use DataTables;
use App\AWS\AwsS3;

class CategoryController extends Controller
{
    public function index(){
        return view('web.admin.category');
    }

    public function datatable(){
        $data = Category::get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('image', function ($data) {
                return '<img src="'.getCategoryImageUrl($data->image).'" class="categroy-image-datatable">';
            })
            ->addColumn('action', function ($data) {
                $str = '';
                if($data->deleted == 0){
                    $str .= '<button class="btn btn-info btn-sm mr-2 editCategory" data-id="'.$data->id.'"><i class="fa-solid fas fa-edit"></i></button>';
                    $str .= '<button class="btn btn-danger btn-sm mr-2 deleteCategory" data-id="'.$data->id.'"><i class="fa-solid fas fa-trash"></i></button>';
                }
                return $str;
            })
            ->rawColumns(['image','action'])
            ->make(true);
    }

    public function store(Request $request){
        $file = $request->file('image');
        $attachmentFolder = config('constants.CATEGORYFOLDER');
        $fileName = AwsS3::s3UploadImage($file, $attachmentFolder, $sFileName = '');

        $category = Category::create([
            'category_name' =>  $request->category_name,
            'image'         =>  $fileName,
            'url' =>  $request->category_name,
        ]);

        return response()->json('Category created successfully.', 200);
    }

    public function edit($id){
        $category = Category::find($id);
        if($category){
            return response()->json($category, 200);
        }else{
            return response()->json('Category not found.', 404);
        }
    }

    public function update(Request $request){
        $category = Category::find($request->id);
        if($category){
            if($request->hasFile('image')){
                $file = $request->file('image');
                $attachmentFolder = config('constants.CATEGORYFOLDER');
                $fileName = AwsS3::s3UploadImage($file, $attachmentFolder, $sFileName = '');
                $category->image = $fileName;                
            }
    
            $category->category_name = $request->category_name;
            $category->url = $request->category_name;
            $category->save();

            return response()->json('Category updated.', 200);
        }else{
            return response()->json('Category not found.', 404);
        }
    }

    public function destroy(Request $request){
        $data = Category::find($request->id);
        if($data){
            $data->deleted = 1;
            $data->save();
            return response()->json('Category Deleted Successfully.', 200);
        }else{
            return response()->json('Category not found.', 404);
        }
    }

    public function checkName(Request $request){
        $data = Category::where([
                'category_name' =>  $request->category_name,
                'activated'     =>  1,
                'deleted'     =>  0,
            ])
            ->when(!empty($request->id) && $request->id != null , function ($query) use($request){
                return $query->where('id', '!=', $request->id);
            })
            ->latest()
            ->first();
        
        if($data){
            return response()->json(false, 200);
        }else{
            return response()->json(true, 200);
        }
    }
}
