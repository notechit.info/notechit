@extends('web.admin.layout')
@section('title')
    Notes
@endsection
@section('page_title')
    Notes
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table id="notes-datatable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Note Title</th>
                            <th>Author Name</th>
                            <th>Note Type</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    var dt = $('#notes-datatable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: {
            url:'{{route("admin.notes.datatable")}}',
            type:'POST',
        },
        columns: [
            {data: 'DT_RowIndex'},
            {data: 'title'},
            {data: 'author_name'},
            {data: 'is_video_note'},
            {data: 'action'},
        ]
    });

    $(document).on('click', '.deleteNote',function(){
        $.ajax({
            url: '{{route('admin.notes.delete')}}',
            type: "POST",
            data:  {
                id:$(this).data('id'),
            },
            success: function(data){
                dt.ajax.reload();
                toastr.success(data);
            },
            error: function(){
                toastr.warning('Something went wrong while deleting note.')
            }
        });
    });
</script>
@endsection