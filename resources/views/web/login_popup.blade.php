<div class="modal fade " id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered  md-modal " role="document">
    <div class="modal-content">
      <div class="modal-body modal-body-padding">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>

        <div class="jsClassLoginForm">
          <div class="nc-xlg-text modal-title text-center ">@lang('messages.login_with_notechit')</div>


            <div class="modal-from-wrapper">
              <div class="" id="signin-alert"></div>

              <form id="signin_form" action="{{ route('web.user.signin') }}" method="POST">
                {{ csrf_field() }}

                <div class="form-group">
                    <input type="text" class="form-control" autocomplete="off" id="signin_email" name="signin_email" placeholder="@lang('messages.email')">
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" autocomplete="off" id="signin_password" name="signin_password" placeholder="@lang('messages.password')">
                </div>

                <div class="nc-xs-text cursor-pointor text-center jsClassShowForgetPassword">
                  @lang('messages.forgot_password')?
                </div>

                <button type="button" id="signin_form_submit_btn" class=" nc-black-btn nc-register-btn">@lang('messages.login')</button>

                <div class="nc-xs-text text-center go-to-signup-btn jsClassBackShowSignup">
                  @lang('messages.signup_with_notechit')
                  <i class="fa fa-chevron-right"></i>
                </div>
              </form>


            </div>
            <div class="other-login-options">
              <div class="or-wrapper">
                <div class="bg-line"></div>
                <span class="or">OR</span>
              </div>
                <?php /* <button type="submit" class="login-btn  google-login-btn">
                    <img class="" src="{{asset('web/img/google.svg')}}" alt="note-cover">

                    @lang('messages.continue_google')
                </button> */ ?>

                <a href="{{ route('web.home.google.login') }}" class="login-btn google-login-btn">
                    <img class="" src="{{asset('web/img/google.svg')}}" alt="note-cover">
                    @lang('messages.continue_google')
                </a>

                <a href="{{ route('web.home.facebook.login') }}" class="login-btn fb-login-btn">
                    <img class="" src="{{asset('web/img/facebook.svg')}}" alt="note-cover">
                    @lang('messages.continue_facebook')
                </a>
                <?php /* <button type="submit" class="login-btn  fb-login-btn">
                <img class="" src="{{asset('web/img/facebook.svg')}}" alt="note-cover">
                @lang('messages.continue_facebook')
                </button> */ ?>

              </div>
        </div>


        <div class="jsClassSignupForm" style="display:none;">
          <div class="nc-xlg-text modal-title text-center jsClassModalTitle">@lang('messages.signup_with_notechit')</div>


          <div class="modal-from-wrapper">
            <div class="" id="signup-alert"></div>
            <form id="signup_form" action="{{ route('web.user.signup') }}" method="POST">

              {{ csrf_field() }}

              <div class="form-row">

                <div class="form-group col-md-6">
                    <input type="text" class="form-control" autocomplete="off" id="first_name" name="first_name" placeholder="@lang('messages.first_name')">
                </div>

                <div class="form-group col-md-6">
                    <input type="text" class="form-control" autocomplete="off" id="last_name" name="last_name" placeholder="@lang('messages.last_name')">
                </div>

              </div>

              <div class="form-group">
                  <input type="text" class="form-control" autocomplete="off" id="email" name="email" placeholder="@lang('messages.email')">
              </div>

              <div class="form-group">
                  <input type="password" class="form-control" autocomplete="off" id="password" name="password" placeholder="@lang('messages.password')">
              </div>

              <div class="form-group">
                  <input type="password" class="form-control" autocomplete="off" id="conf_password" name="conf_password" placeholder="@lang('messages.confirm_password')">
              </div>

              <button type="button" id="signup_form_submit_btn" class="nc-black-btn nc-register-btn">@lang('messages.signup')</button>

              <div class="nc-xs-text text-center login-back-btn jsClassShowLogin">
                  <i class="fa fa-chevron-left"></i>
                  @lang('messages.go_back_to_login')
              </div>
            </form>

          </div>

        </div>

        <div class="jsClassForgetPassword" style="display:none;">
          <div class="nc-xlg-text modal-title text-center">@lang('messages.forgot_password')?</div>

          <div class="nc-sm-text">@lang('messages.please_enter_email_forget_password')</div>

          <p>&nbsp;</p>

          <div class="col-md-12" id="fp-alert"></div>

          <form id="fp_form" action="{{ route('web.user.forgot-password') }}" method="POST">

              {{ csrf_field() }}

                <div class="form-group">
                    <input type="text" autocomplete="off" class="form-control" id="fo_email" name="fo_email" placeholder="@lang('messages.email')">
                </div>

                <button type="button" id="fp_form_submit_btn" class="nc-black-btn nc-register-btn">@lang('messages.reset')</button>

                <div class="nc-xs-text text-center login-back-btn jsClassShowLogin">
                  <i class="fa fa-chevron-left"></i>
                  @lang('messages.go_back_to_login')
                </div>

          </form>

        </div>

      </div>

    </div>
  </div>
</div>

<!-- <script src="http://malsup.github.com/jquery.form.js"></script> -->

<script>
  $(".jsClassBackShowSignup").click(function(){
      $('.jsClassLoginForm').hide( "fast" );
      $( ".jsClassSignupForm" ).show( "fast" );
  });
  $(".jsClassShowLogin").click(function(){
      $('.jsClassSignupForm').hide( "fast" );
      $('.jsClassForgetPassword').hide( "fast" );
      $( ".jsClassLoginForm" ).show( "fast" );
  });
  $(".jsClassShowForgetPassword").click(function(){
      $( ".jsClassLoginForm" ).hide( "fast" );
      $( ".jsClassForgetPassword" ).show( "fast" );
  });

  function clearSignupFormData()
  {
      $('#first_name').val('');
      $('#last_name').val('');
      $('#email').val('');
      $('#password').val('');
      $('#conf_password').val('');
  }

  function removeSignUpFormErrorClass()
  {
      $('#first_name').removeClass('error');
      $('#last_name').removeClass('error');
      $('#email').removeClass('error');
      $('#password').removeClass('error');
      $('#conf_password').removeClass('error');
  }

  /**Submit Signup form*/
  $.ajaxSetup({
        headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
    });

    var signupOptions = {
            beforeSubmit:  showSignupRequest,
            success:       showSignupResponse,
            error:showSignupError,
            dataType: 'json'
    };

    function showSignupRequest() {
        $("#validation-errors").hide().empty();
        return true;
    }

    function showSignupResponse(response) {
        if(response.status == 0)
        {
          $.each(response.errors, function(key,val){
            $('#'+key).closest('.form-group').append('<p class="signUpError">'+val+'</p>');
            $('#'+key).addClass('error');
          });

          return false;
        }
        // var sHtml = '';
        var sHtml = '<div class="text-center py-3 jsClassSignupSuccess"> <i class="success-mail-icon animate__animated animate__swing fas fa-envelope"></i>'
                    +'<h4 class="py-2">You have Signed up successfully</h4><p>Thankyou for choosing Notechit, Please check your Email for activation</p>'
                    +'<button class="nc-black-btn nc-save-btn mx-auto js-signup-suc" data-dismiss="modal" aria-label="Close">Ok</button></div>';

        // sHtml +='<div class="alert alert-success p-1" id="success-alert">'
        // sHtml +='<button type="button" class="close" data-dismiss="alert">&times;</button>'
        // sHtml +='<strong>Success!</strong>'
        // sHtml +=' '+response.message+' '
        // sHtml +='</div>'

        $('#signup-alert').html(sHtml);
        $('#signup_form').css('display','none');
        $('.jsClassModalTitle').css('display','none');
        //alert(response.message);

        //setTimeout(() => {
          //window.location.reload();
        //}, 1000);
    }

    function showSignupError(xhr) {
        console.log("inside error");
    }

    function clearSignUpAlert()
    {
      $('#signup-alert').html('');
    }

    $('body').on('click','#signup_form_submit_btn',function(){
      $('.signUpError').remove();
      removeSignUpFormErrorClass();
      $('#signup_form').ajaxForm(signupOptions).submit();
    });

    $('body').on('click','.js-signup-suc',function(){
      clearSignUpAlert();
      $('#signup_form').css('display','block');
      $('.jsClassModalTitle').css('display','block');
      $('#signup_form').trigger("reset");
    });

    $('body').on('click','.close',function(){
      $('#signin_form').trigger("reset");
      $('#signin-alert').html('');
    });
    /**Submit Signup form*/

    /**Submit Sign-in form*/

    function removeSignInFormErrorClass()
    {
        $('#signin_email').removeClass('error');
        $('#signin_password').removeClass('error');
    }

    var signinOptions = {
            beforeSubmit:  showSigninRequest,
            success:       showSigninResponse,
            error:showSigninError,
            dataType: 'json'
    };

    function showSigninRequest() {
        $("#validation-errors").hide().empty();
        return true;
    }

    function showSigninResponse(response) {
        if(response.status == 0)
        {
          $.each(response.errors, function(key,val){
            $('#'+key).addClass('error');
          });

          return false;
        }

        if(response.status == 1)
        {
          if(response.login_count == 1)
          {
            $('#loginModal').modal('toggle');
            $('#interestModal').modal('toggle');
          }
          else{
            window.location.href = "{{route('web.home.index')}}";
            return false;
          }
        }

        var sHtml = '';
        sHtml +='<div class="alert alert-danger p-2 nc-sm-text" id="success-alert">'
        sHtml +='<button type="button" class="close" data-dismiss="alert">&times;</button>'
        sHtml +='<strong>Error!</strong>'
        sHtml +=' '+response.message+' '
        sHtml +='</div>'

        $('#signin-alert').html(sHtml);
        //alert(response.message);

    }

    function showSigninError(xhr) {
        console.log("inside error");
    }

    function clearSignInAlert()
    {
      $('#signin-alert').html('');
    }

    $('body').on('click','#signin_form_submit_btn',function(){
      clearSignInAlert();
      removeSignInFormErrorClass();
      $('#signin_form').ajaxForm(signinOptions).submit();
    });
    /**Submit Sign-in form*/


    /**Submit Fp form*/
    var fpOptions = {
            beforeSubmit:  showFpRequest,
            success:       showFpResponse,
            error:showFpError,
            dataType: 'json'
    };

    function showFpRequest() {
        $("#validation-errors").hide().empty();
        return true;
    }

    function showFpResponse(response) {
        if(response.status == 0)
        {
          $.each(response.errors, function(key,val){
            $('#'+key).addClass('error');
          });

          return false;
        }

        var sHtml = '';
        sHtml +='<div class="alert alert-success" id="success-alert">'
        sHtml +='<button type="button" class="close" data-dismiss="alert">x</button>'
        sHtml +='<strong>Success!</strong>'
        sHtml +=' '+response.message+' '
        sHtml +='</div>'

        $('#fp-alert').html(sHtml);

    }

    function showFpError(xhr) {
        console.log("inside error");
    }

    function removeFpFormErrorClass()
    {
        $('#fo_email').removeClass('error');
    }

    function clearFpAlert()
    {
      $('#fp-alert').html('');
    }

    $('body').on('click','#fp_form_submit_btn',function(){
      removeFpFormErrorClass();
      $('#fp_form').ajaxForm(fpOptions).submit();
    });
    /**Submit Fp form*/

</script>
