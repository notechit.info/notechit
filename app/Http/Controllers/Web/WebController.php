<?php

namespace App\Http\Controllers\Web;

use App\Models\Notification;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class WebController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function prepareJsonResponse($aResponse)
    {
        $this->sResponse = preg_replace('/\"[ ]{0,}:[ ]{0,}(\d+[\/\d. ]*|\d)/', '":"$1"', json_encode($aResponse, JSON_HEX_QUOT));
        return response($this->sResponse)->header('Content-Type', 'application/json');
    }

    function RandomString($length) {
        $keys = array_merge(range(0,9), range('a', 'z'));

        $key = "";
        for($i=0; $i < $length; $i++) {
            $key .= $keys[mt_rand(0, count($keys) - 1)];
        }
        return $key;
    }



}
