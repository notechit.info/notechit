<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContributionQueryCategory extends Model
{
    protected $fillable = [
        'contribution_query_id',
        'category_id',
    ];


    /**
     * Get the categories for the query post.
     */
    public function categories()
    {
        return $this->hasMany(Category::class, 'id', 'category_id');
    }
}
