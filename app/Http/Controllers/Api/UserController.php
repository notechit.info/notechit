<?php
namespace App\Http\Controllers\Api;

//use App\MechanicLibrary\MFileUpload;
use App\Models\DeviceToken;
use App\Models\User;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Hash;

use App\Mail\ActivationEmail;
use App\Mail\ResetPasswordEmail;
use Illuminate\Support\Facades\Mail;

use GuzzleHttp\Client;
use Laravel\Passport\Client as OClient; 
use Laravel\Passport\PersonalAccessTokenFactory as PersonalAccessTokenFactory; 

use App\AWS\AwsS3;

//use Illuminate\Support\Facades\Storage;

class UserController extends ApiController
{
    public $successStatus = 200;

    public function __construct(Request $oRequest)
    {
        parent::__construct($oRequest);
    }

    public function callLoginRedirect()
    {
        return $this->prepareResponse([
            'data'          => null,
            'message'       => trans('messages.need_to_login'),
            'errors'        => ['message' => [trans('messages.need_to_login')]],
            'status_code'   => config('constants.APILOGINERROR')
        ]);
    }

    private function _createUser($oRequest)
    {
        $oUser = User::create([
            'first_name'    => $oRequest->get('first_name'),
            'last_name'     => $oRequest->get('last_name'),
            'email'         => $oRequest->get('email'),
            'password'      => bcrypt($oRequest->get('password')),
            'verified'      => User::UNVERIFIED
        ]);

        return $oUser;
    }

    private function _responseOfUser($oUser)
    {
        $oUserDetail = [
            //'user_id'   => $oUser->id,
            'username'  => $oUser->username,
            'first_name'=> $oUser->first_name,
            'last_name' => $oUser->last_name,
            'email'     => $oUser->email, 
            'mobile_no' => $oUser->mobile,
            'about_me' => $oUser->about_me,
            //'file_name' => getUserImageUrl($oUser->file_name,$oUser->profile_pic_url),
            'profile_pic_url' => getUserImageUrl($oUser->file_name,$oUser->profile_pic_url),
            'login_id'  => $oUser->login_id,
            'login_with'=> $oUser->login_with,
            'verified'  => $oUser->verified,
            'activated' => $oUser->activated,
            'deleted'   => $oUser->deleted
        ];


        return $oUserDetail;
    }

    /**
     * @param Request $oRequest
     * @return $this
     * Sign up
     */
    public function signup(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:6'
            ];  

            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                return $this->prepareResponse([
                    'data'          => null,
                    'message'       => null,
                    'errors'        => $oValidator->errors(),
                    'status_code'   => config('constants.APIERROR')
                ]);
            }

            $sEmail = $oRequest->get('email');
            $oCreatedUser = $this->_createUser($oRequest);

            /**
             * Send verification Email to user
             */
            try
            {
                $key = $this->RandomString(100).date('dymhis').rand(0000,9999);
                $oCreatedUser->unique_key = $key;
                $oCreatedUser->save();
                $link = env('APP_URL')."/verification/".$key;
                \Mail::to($sEmail)->send(new ActivationEmail($link));
            }
            catch (\Exception $e) {}

            $oUser = $this->_responseOfUser($oCreatedUser);


            return $this->prepareResponse([
                'data'          => $oUser,
                'message'       => trans('messages.api_signup_success'),
                'errors'        => $this->emptyObject(),
                'status_code'   => config('constants.APISUCCESS')
            ]);

        }

        return $this->prepareResponse([
            'data'          => null,
            'message'       => null,
            'errors'        => $this->emptyObject(),
            'status_code'   => config('constants.APIFAIL')
        ]);
    }

    /**
     * Resend email verification email
     */
    public function callResendVerificationEmail(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                'email' => 'required|email',
            ];  

            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                return $this->prepareResponse([
                    'data'          => null,
                    'message'       => null,
                    'errors'        => $oValidator->errors(),
                    'status_code'   => config('constants.APIERROR')
                ]);
            }

            $sEmail = $oRequest->get('email');
            if(!$oUser = User::where('email',$sEmail)->first())
            {
                return $this->prepareResponse([
                    'data'          => null,
                    'message'       => null,
                    'errors'        => array('message' => array('User Not Exists!')),
                    'status_code'   => config('constants.APIERROR')
                ]);
            }

            /**
             * Send verification Email to user
             */
            try
            {
                $key = $this->RandomString(100).date('dymhis').rand(0000,9999);
                $oUser->unique_key = $key;
                $oUser->save();
                $link = env('APP_URL')."/verification/".$key;
                \Mail::to($sEmail)->send(new ActivationEmail($link));
            }
            catch (\Exception $e) {}

            return $this->prepareResponse([
                'data'          => null,
                'message'       => trans('messages.verification_email_send_success'),
                'errors'        => $this->emptyObject(),
                'status_code'   => config('constants.APISUCCESS')
            ]);

        }

        return $this->prepareResponse([
            'data'          => null,
            'message'       => null,
            'errors'        => $this->emptyObject(),
            'status_code'   => config('constants.APIFAIL')
        ]);
    }

    /**
     * Forgot password
     */
    public function callForgotPassword(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                'email' => 'required|email',
            ];  

            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                return $this->prepareResponse([
                    'data'          => null,
                    'message'       => null,
                    'errors'        => $oValidator->errors(),
                    'status_code'   => config('constants.APIERROR')
                ]);
            }

            $sEmail = $oRequest->get('email');
            if(!$oUser = User::where('email',$sEmail)->first())
            {
                return $this->prepareResponse([
                    'data'          => null,
                    'message'       => null,
                    'errors'        => array('message' => array('Please enter your registered email')),
                    'status_code'   => config('constants.APIERROR')
                ]);
            }

            if($oUser->verified == User::UNVERIFIED)
            {
                return $this->prepareResponse([
                    'data'          => null,
                    'message'       => null,
                    'errors'        => array('message' => array('Please verify your account first!')),
                    'status_code'   => config('constants.APIERROR')
                ]);
            }

            /**
             * Send verification Email to user
             */
            try
            {
                $key = $this->RandomString(100).date('dymhis').rand(0000,9999);
                $oUser->unique_key = $key;
                $oUser->save();
                $link = env('APP_URL')."/reset-password/".$key;
                \Mail::to($sEmail)->send(new ResetPasswordEmail($link));
            }
            catch (\Exception $e) {}

            return $this->prepareResponse([
                'data'          => null,
                'message'       => trans('messages.forgot_password_email_send_success'),
                'errors'        => $this->emptyObject(),
                'status_code'   => config('constants.APISUCCESS')
            ]);

        }

        return $this->prepareResponse([
            'data'          => null,
            'message'       => null,
            'errors'        => $this->emptyObject(),
            'status_code'   => config('constants.APIFAIL')
        ]);
    }

    // public function getTokenAndRefreshToken(OClient $oClient, $email, $password) { 
    //     $oClient = OClient::where('password_client', 1)->first();
    //     $http = new Client;
    //     $response = $http->request('POST', 'http://dev.notechit.kp/oauth/token', [
    //         'form_params' => [
    //             'grant_type' => 'password',
    //             'client_id' => $oClient->id,
    //             'client_secret' => $oClient->secret,
    //             'username' => $email,
    //             'password' => $password,
    //             'scope' => '*',
    //         ],
    //     ]);

    //     $result = json_decode((string) $response->getBody(), true);
    //     return response()->json($result, $this->successStatus);
    // }

    private function _saveRefreshToken($oUser){

        $sEmail = $oUser->email ? $oUser->email : "refreshtoken@gmail.com";
        $sUserRefreshTokenString = $oUser->id ."_".date('dmyhis') ."_".\base64_encode($sEmail);
        $sRefreshToken = \base64_encode($sUserRefreshTokenString);

        $sFinalRefreshToken = \base64_encode($sRefreshToken);
        $sFinalRefreshTokenExpiresAt = Carbon::now()->addWeeks(2)->toDateTimeString();

        $oUser->refresh_token = $sFinalRefreshToken;
        $oUser->refresh_token_expires_at = $sFinalRefreshTokenExpiresAt;
        $oUser->save();

        $aResponse['refresh_token'] = $sFinalRefreshToken;
        $aResponse['refresh_token_expires_at'] = $sFinalRefreshTokenExpiresAt;

        return $aResponse;
    }

    public function callLogin(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                'type'        => 'required|in:1,2,3',
                
                'email'             => 'required_if:type,==,1',
                'password'          => 'required_if:type,==,1',

                'device_token'      => 'required',
                'device_type'       => 'required|in:'.DeviceToken::ANDROID.','.DeviceToken::IOS,

                'profile_pic_url'   => 'required_if:type,==,2|required_if:type,==,3',
                'login_id'          => 'required_if:type,==,2|required_if:type,==,3',
            ];

            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                return $this->prepareResponse([
                    'data'          => null,
                    'errors'        => $oValidator->errors(),
                    'status_code'   => config('constants.APIERROR')
                ]);
            }

            if($oRequest->get('type') == 1) { // If normal login
 
                if($oUser = User::where('email', $oRequest->get('email'))->first())
                {
                    if($oUser->verified == User::VERIFIED && $oUser->activated == User::YES) {
                        
                        if(Hash::check($oRequest->get('password'), $oUser->password)) {
                            
                            /** Save Refresh token */
                            /*$sEmail = $oUser->email ? $oUser->email : "refreshtoken@gmail.com";
                            $sUserRefreshTokenString = $oUser->id ."_".date('dmyhis') ."_".\base64_encode($sEmail);
                            $sRefreshToken = \base64_encode($sUserRefreshTokenString);
                            $oUser->refresh_token = $sRefreshToken;
                            $oUser->save();*/
                            /** Save Refresh token */

                            $dToken = new DeviceToken();
                            $dToken->updateOrCreate([
                                'device_token' => $oRequest->get('device_token'),
                            ], [
                                'user_id' => $oUser->id,
                                'device_type' => $oRequest->has('device_type') ? $oRequest->get('device_type') : DeviceToken::ANDROID,
                            ]);

                            $aResponse = $this->_saveRefreshToken($oUser);
                            $sRefreshToken = $aResponse['refresh_token'];
                            $sRefreshTokenExpiresTime = $aResponse['refresh_token_expires_at'];

                            // Create passport token
                            $sTokenResult = $oUser->createToken('token');
                            $oToken = $sTokenResult->token;
                            //if ($oRequest->get('remember_me'))
                            $oToken->expires_at = Carbon::now()->addWeeks(1);
                            $oToken->save();

                            //$oClient = OClient::where('password_client', 1)->first();
                            //return $this->getTokenAndRefreshToken($oClient, request('email'), $oRequest->get('password'));

                            $oUser = [
                                'user_detail' => $this->_responseOfUser($oUser),
                                //'user_image_url' => getUserImageUrl($oUser->file_name),
                                'user_token' => [
                                    'access_token' => $sTokenResult->accessToken,
                                    'refresh_token' => $sRefreshToken,
                                    'refresh_token_expires_at' => $sRefreshTokenExpiresTime,
                                    'token_type' => 'Bearer',
                                    'expires_at' => Carbon::parse(
                                        $sTokenResult->token->expires_at
                                    )->toDateTimeString()
                                ]
                            ];

                            return $this->prepareResponse([
                                'data'          => $oUser,
                                'verified'      => 1,
                                'errors'        => $this->emptyObject(),
                                'status_code'   => config('constants.APISUCCESS')
                            ]);

                        }
                        
                        return $this->prepareResponse([
                            'data'          => null,
                            'verified'      => null,
                            'errors'        => array('error_message' => array(trans('messages.invalid_credential')) ),
                            'status_code'   => config('constants.APIERROR')
                        ]);

                    }
                    else if($oUser->verified == User::UNVERIFIED) 
                    {
                        $oUser = [
                            'user_detail' => $this->_responseOfUser($oUser),
                            'user_token' => $this->emptyObject()
                        ];

                        return $this->prepareResponse([
                            'data'          => $oUser,
                            'verified'      => 0,
                            'errors'        => $this->emptyObject(),
                            'status_code'   => config('constants.APISUCCESS'),
                        ]);
                    }
                }

                return $this->prepareResponse([
                    'data'          => null,
                    'verified'      => null,
                    'errors'        => array('error_message' => array(trans('messages.invalid_credential')) ),
                    'status_code'   => config('constants.APIERROR'),
                ]);

            }
            else {

                if (empty($oRequest->get('email'))) {
                    $aValidationRequiredFor = [
                        'mobile'             => 'required',
                    ];
    
                    $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
                    if ($oValidator->fails()) {
                        return $this->prepareResponse([
                            'data'          => null,
                            'errors'        => $oValidator->errors(),
                            'status_code'   => config('constants.APIERROR')
                        ]);
                    }
                }

                $sLoginWith = User::NORMAL;
                if($oRequest->type == 2)
                    $sLoginWith = User::FB;
                else if($oRequest->type == 3)
                    $sLoginWith = User::GMAIL;

                if($oUser = User::where('email',$oRequest->get('email'))->orWhere('mobile',$oRequest->get('mobile'))->first())
                {
                    $this->_updateUser($oRequest,$oUser,$sLoginWith);
                }
                else
                {
                    $oUser = User::create([
                        'first_name'  => $oRequest->has('first_name') ? $oRequest->get('first_name') : null,
                        'last_name'   => $oRequest->has('last_name') ? $oRequest->get('last_name') : null,
                        'email'       => $oRequest->has('email') ? $oRequest->get('email') : null,
                        'mobile'      => $oRequest->has('mobile') ? $oRequest->get('mobile') : null,
                        'profile_pic_url' => $oRequest->has('profile_pic_url') ? $oRequest->get('profile_pic_url') : null,
                        'login_id'    => $oRequest->get('login_id'),
                        'login_with'  => $sLoginWith,
                        'verified'    => User::VERIFIED
                    ]);
                }

                $dToken = new DeviceToken();
                $dToken->updateOrCreate([
                    'device_token' => $oRequest->get('device_token'),
                ], [
                    'user_id' => $oUser->id,
                    'device_type' => $oRequest->has('device_type') ? $oRequest->get('device_type') : DeviceToken::ANDROID,
                ]);

                $aResponse = $this->_saveRefreshToken($oUser);
                $sRefreshToken = $aResponse['refresh_token'];
                $sRefreshTokenExpiresTime = $aResponse['refresh_token_expires_at'];

                // Create passport token
                $sTokenResult = $oUser->createToken('token');
                $oToken = $sTokenResult->token;
                //if ($oRequest->get('remember_me'))
                $oToken->expires_at = Carbon::now()->addWeeks(1);
                $oToken->save();

                $oUser = [
                    'user_detail' => $this->_responseOfUser($oUser),
                    //'user_image_url' => $oUser->profile_pic_url != '' ? $oUser->profile_pic_url : getUserImageUrl($oUser->file_name),
                    'user_token' => [
                        'access_token' => $sTokenResult->accessToken,
                        'refresh_token' => $sRefreshToken,
                        'refresh_token_expires_at' => $sRefreshTokenExpiresTime,
                        'token_type' => 'Bearer',
                        'expires_at' => Carbon::parse(
                            $sTokenResult->token->expires_at
                        )->toDateTimeString()
                    ]
                ];

                return $this->prepareResponse([
                    'data'          => $oUser,
                    'verified'      => null,
                    'errors'        => $this->emptyObject(),
                    'status_code'   => config('constants.APISUCCESS')
                ]);

            }// End of other login

            return $this->prepareResponse([
                'data'          => null,
                'verified'      => null,
                'errors'        => array('error_message' => array(trans('messages.invalid_credential'))),
                'status_code'   => config('constants.APIERROR')
            ]);
        }

        // 9909042792 n L pramar SBI ahemdabad

        return $this->prepareResponse([
            'data'          => null,
            'verified'      => null,
            'errors'        => $this->emptyObject(),
            'status_code'   => config('constants.APIFAIL')
        ]);
    }

    public function _updateUser($oRequest,$oUser,$sLoginWith){
        $oUser->first_name  = $oRequest->has('first_name') ? $oRequest->get('first_name') : $oUser->first_name;
        $oUser->last_name   = $oRequest->has('last_name') ? $oRequest->get('last_name') : $oUser->last_name;
        $oUser->email       = $oRequest->has('email') ? $oRequest->get('email') : $oUser->email;
        $oUser->mobile      = $oRequest->has('mobile') ? $oRequest->get('mobile') : $oUser->mobile;
        $oUser->profile_pic_url = $oRequest->has('profile_pic_url') ? $oRequest->get('profile_pic_url') : $oUser->profile_pic_url;
        $oUser->login_id    = $oRequest->has('login_id') ? $oRequest->get('login_id') : $oUser->login_id;
        $oUser->login_with  = $sLoginWith;
        $oUser->verified    = User::VERIFIED;
        $oUser->save();
    }

    public function callRefreshToken(Request $oRequest)
    {
        if ($oRequest->isMethod("POST")) {
            $aValidationRequiredFor = [
                'refresh_token' => 'required',
            ];

            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if ($oValidator->fails()) {
                return $this->prepareResponse([
                    'data'          => null,
                    'errors'        => $oValidator->errors(),
                    'status_code'   => config('constants.APIERROR')
                ]);
            }
            
            if($oUser = User::where('refresh_token',$oRequest->refresh_token)->first())
            {

                \DB::table('oauth_access_tokens')
                    ->where('user_id', $oUser->id)
                    ->update([
                        'revoked' => true
                    ]);

                if($oUser->refresh_token_expires_at < Carbon::now()->toDateTimeString())
                {
                    return $this->prepareResponse([
                        'data'          => null,
                        'errors'        => array('error_message' => array(trans('messages.invalid_refresh_token'))),
                        'status_code'   => config('constants.APIERROR')
                    ]);
                }

                $aResponse = $this->_saveRefreshToken($oUser);
                $sRefreshToken = $aResponse['refresh_token'];
                $sRefreshTokenExpiresTime = $aResponse['refresh_token_expires_at'];

                $sTokenResult = $oUser->createToken('token');
                $oToken = $sTokenResult->token;
                //if ($oRequest->get('remember_me'))
                $oToken->expires_at = Carbon::now()->addWeeks(1);
                $oToken->save();

                $oUser = [
                    'user_detail' => $this->_responseOfUser($oUser),
                    //'user_image_url' => getUserImageUrl($oUser->file_name),
                    'user_token' => [
                        'access_token' => $sTokenResult->accessToken,
                        'refresh_token' => $sRefreshToken,
                        'refresh_token_expires_at' => $sRefreshTokenExpiresTime,
                        'token_type' => 'Bearer',
                        'expires_at' => Carbon::parse(
                            $sTokenResult->token->expires_at
                        )->toDateTimeString()
                    ]
                ];

                return $this->prepareResponse([
                    'data'          => $oUser,
                    'verified'      => 1,
                    'errors'        => $this->emptyObject(),
                    'status_code'   => config('constants.APISUCCESS')
                ]);
            }

            return $this->prepareResponse([
                'data'          => null,
                'errors'        => array('error_message' => array(trans('messages.invalid_refresh_token'))),
                'status_code'   => config('constants.APIERROR')
            ]);
        }

        return $this->prepareResponse([
            'data'          => null,
            'errors'        => $this->emptyObject(),
            'status_code'   => config('constants.APIFAIL')
        ]);
    }

    /**
     * @param Request $oRequest
     * @return $this
     * User edit profile
     */
    public function callEditProfile(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $oUser = $oRequest->user();

            $aValidationRequiredFor = [
                'first_name' => 'required',
                'last_name' => 'required',
                'mobile' => 'required|numeric|unique:users,mobile,'.$oUser->id,
                'file' => 'sometimes|image',
            ];

            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                return $this->prepareResponse([
                    'data'          => null,
                    'errors'        => $oValidator->errors(),
                    'status_code'   => config('constants.APIERROR')
                ]);
            }

            $sFileName = $oUser->file_name;
            if($oRequest->file('file')) {
                $sFolder = config('constants.USERMEDIAFOLDER');
                $sFileName = AwsS3::s3UploadImage($oRequest->file('file'),$sFolder,$sFileName = '');
            }

            $oUser->first_name  = $oRequest->get('first_name');
            $oUser->last_name  = $oRequest->get('last_name');
            $oUser->mobile    = $oRequest->get('mobile');
            $oUser->file_name = $sFileName;
            $oUser->save();

            $oUser = User::find($oUser->id);

            $oUser = [
                'user_detail' => $this->_responseOfUser($oUser),
                'user_token' => $this->emptyObject()
            ];
            
            return $this->prepareResponse([
                'data'          => $oUser,
                'verified'      => 1,
                'errors'        => $this->emptyObject(),
                'status_code'   => config('constants.APISUCCESS')
            ]);
        }

        return $this->prepareResponse([
            'data'          => null,
            'errors'        => $this->emptyObject(),
            'status_code'   => config('constants.APIFAIL')
        ]);
    }

    public function callLogout(Request $oRequest)
    {
        if($oRequest->isMethod("GET"))
        {
            //Logout passport token revoke
            $oRequest->user()->token()->revoke();

            return $this->prepareResponse([
                'data'          => ['success_message' => trans('messages.logout_success')],
                'verified'      => 1,
                'errors'        => $this->emptyObject(),
                'status_code'   => config('constants.APISUCCESS')
            ]);

        }

        return $this->prepareResponse([
            'data'          => null,
            'errors'        => $this->emptyObject(),
            'status_code'   => config('constants.APIFAIL')
        ]);
    }

    
    public function callCheckUserMobileNumber(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                'mobile' => 'required|numeric'
            ];

            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'check_mobile_number';
                $aMessages['response_message'] = 'check_mobile_number_fail';

                return $this->prepareResponse($aMessages);
            }

            $bMobile = FALSE;
            if($oUser = User::where('mobile',$oRequest->get('mobile'))->first())
            {
                $bMobile = TRUE;
            }

            $bEmail = FALSE;
            if($oRequest->has('email') && $oRequest->get('email') != null) {
                if ($oUser = User::where('email', $oRequest->get('email'))->first()) {
                    $bEmail = TRUE;
                }
            }

            $aMessages['data']              = ['registered_user' => $bMobile,'email_exist' => $bEmail, 'mobile_exist' => $bMobile];
            $aMessages['errors']            = array();
            $aMessages['request_type']      = 'check_mobile_number';
            $aMessages['response_status']   = config('constants.APISUCCESS');
            $aMessages['response_message']  = 'check_mobile_number_success';

            return $this->prepareResponse($aMessages);
        }

        $aMessages['data']              = array();
        $aMessages['errors']            = array();
        $aMessages['request_type']      = 'check_mobile_number';
        $aMessages['response_status']   = config('constants.APIFAIL');
        $aMessages['response_message']  = 'not_valid_method';

        return $this->prepareResponse($aMessages);
    }

    public function callResetPasswordWithMobilePassword(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                'mobile' => 'required|numeric|exists:users,mobile',
                'password' => 'required'
            ];

            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'reset_forgot_password';
                $aMessages['response_message'] = 'reset_forgot_password_fail';

                return $this->prepareResponse($aMessages);
            }

            if($oUser = User::where('mobile',$oRequest->get('mobile'))->first())
            {
                $oUser->password  = bcrypt($oRequest->get('password'));
                $oUser->save();
            }            

            $aMessages['data']              = ['success_message' => trans('m.password_reset_success')];
            $aMessages['errors']            = array();
            $aMessages['request_type']      = 'reset_forgot_password';
            $aMessages['response_status']   = config('constants.APISUCCESS');
            $aMessages['response_message']  = 'reset_forgot_password_success';

            return $this->prepareResponse($aMessages);
        }

        $aMessages['data']              = array();
        $aMessages['errors']            = array();
        $aMessages['request_type']      = 'reset_forgot_password';
        $aMessages['response_status']   = config('constants.APIFAIL');
        $aMessages['response_message']  = 'not_valid_method';

        return $this->prepareResponse($aMessages);
    }    
}
