<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NoteView extends Model
{
    const IN_ACTIVE = 0 , ACTIVE = 1;

    protected $fillable = [
        'note_id',
        'user_id',
        'activated',
        'deleted'
    ];

    public function scopeActive($query)
    {
        return $query->where('note_views.activated',self::ACTIVE);
    }

    /**
     * Add view - insert data
     */
    public static function saveViewEntry($nNoteId = NULL,$nUserId = NULL)
    {
        if(empty($nNoteId) || empty($nUserId))
        {
            return false;
        } 

        $oNoteView = NoteView::firstOrNew([
            'note_id'   => $nNoteId,
            'user_id'   => $nUserId
        ]);

        $oNoteView->activated = self::ACTIVE;
        $oNoteView->deleted   = self::IN_ACTIVE;
        $oNoteView->save();

        return true;
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
