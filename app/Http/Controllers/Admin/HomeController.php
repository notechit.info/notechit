<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Note;
use App\Models\NoteReport;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index(){
        $usersCount = User::active()->count();
        $notesCount = Note::active()->count();
        $reportedNotesCount = NoteReport::select('note_id')
            ->distinct()
            ->count();

        return view('web.admin.home', compact('usersCount', 'notesCount','reportedNotesCount'));
    }
}
