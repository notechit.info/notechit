<?php

namespace App\Http\Controllers\Api;

use App\Models\Note;

use Carbon\Carbon;

use Validator;
use Illuminate\Http\Request;

class NoteController extends ApiController
{
    public function __construct(Request $oRequest)
    {
        parent::__construct($oRequest);
    }

    public function getNotes(Request $oRequest){
        if($oRequest->isMethod("GET"))
        {
            $oNotesQuery = Note::getNotes(Note::PUB,true);

            $oNotes = $oNotesQuery->setCollection(
                $oNotesQuery->getCollection()->map(function ($oNote) {
                    return [
                        'note_id'           => $oNote->note_id,
                        'title'             => $oNote->title,
                        'created_at'        => Carbon::parse($oNote->created_at)->format('d/m/Y') ,
                        'file_name'         => getNoteCoverImageUrl($oNote->file_name),
                        'first_name'        => $oNote->first_name,
                        'last_name'         => $oNote->last_name,
                        'user_profile_pic_url' =>  getUserImageUrl($oNote->user_file_name,$oNote->user_profile_pic_url)
                    ];
                })
            );

            return $this->prepareResponse([
                'data'          => $oNotes,
                'errors'        => $this->emptyObject(),
                'status_code'   => config('constants.APISUCCESS')
            ]);
        }

        return $this->prepareResponse([
            'data'          => null,
            'errors'        => $this->emptyObject(),
            'status_code'   => config('constants.APIFAIL')
        ]);
    } 
}
