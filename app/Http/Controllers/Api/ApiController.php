<?php

namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Http\Request;

class ApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $sResponse;
    protected $aAllowedAgents;
    protected $nUpdateRequired       = 0;   //0=not_require, 1=update_required, 2=update_optional
    protected $nLatestIOSVersion     = 0;
    protected $nLatestAndroidVersion = 1.0;

    public function __construct(Request $oRequest)
    {
        $this->aAllowedAgents = [config('constants.IOS_USERAGENT'), config('constants.ANDROID_USERAGENT')];
        /*if(!in_array(base64_decode($oRequest->header('Api-Agent')), $this->aAllowedAgents))
        {
            exit('Access denied!');
        }*/
    }

    protected function prepareResponse($aResponse)
    {
        $aResponse['force_update_required'] = $this->nUpdateRequired;
        $aResponse['ios_version'] = $this->nLatestIOSVersion;
        $aResponse['android_version'] = $this->nLatestAndroidVersion;
        $this->sResponse = preg_replace('/\"[ ]{0,}:[ ]{0,}(\d+[\/\d. ]*|\d)/', '":"$1"', json_encode($aResponse, JSON_HEX_QUOT));
        return response($this->sResponse)->header('Content-Type', 'application/json');
    }

    protected function emptyObject()
    {
        return new \stdClass();
    }

    function RandomString($length) {
        $keys = array_merge(range(0,9), range('a', 'z'));

        $key = "";
        for($i=0; $i < $length; $i++) {
            $key .= $keys[mt_rand(0, count($keys) - 1)];
        }
        return $key;
    }

}
