<?php
/**
 * Delete
 * This is used for display delete confirmation popup
 */
?>
<div class="modal fade" id="{{ isset($id) ? $id : 'confirm' }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete {{ $name }}</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <svg> ... </svg>
                </button> -->
            </div>
            <div class="modal-body">
                <p class="modal-text">
                    @if(isset($message))
                        {{ $message }}
                    @else
                        Are you sure you want to delete this {{ $name }}?
                    @endif
                </p>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" id="cancel"><i class="flaticon-cancel-12"></i> Cancel</button>
                <button type="button" class="btn btn-primary" id="delete">Delete</button>
            </div>
        </div>
    </div>
</div>
