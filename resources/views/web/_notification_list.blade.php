@if(count($oNotifications) > 0)
@foreach($oNotifications as $oNotification)
<li>    
    @if($oNotification->type == \App\Models\Notification::FF)
        <!-- <a href=""> -->
            <!-- <span class="user-thumbnail-sm nc-tag-bg-2">
                <span class="user-initials">{{ substr($oNotification->youUser->first_name,0,1) ." ". substr($oNotification->youUser->last_name,0,1) }}</span>
                <img class="w-100" src="" alt="">
            </span>
            <span  class="notification-text ml-1" style="color:white;">

                @if($oNotification->ff_status == \App\Models\FollowRequest::PENDING)
                {{ getUserName($oNotification->youUser->first_name,$oNotification->youUser->last_name) }} Wants to follow you.
                @elseif($oNotification->ff_status == \App\Models\FollowRequest::ACCEPT)
                {{ getUserName($oNotification->youUser->first_name,$oNotification->youUser->last_name) }} followed you.
                @else
                You have declined {{ getUserName($oNotification->youUser->first_name,$oNotification->youUser->last_name) }} follow request.
                @endif
                
                @if($oNotification->ff_status == \App\Models\FollowRequest::PENDING)
                <div>
                    <span class="notification-btn" data-id="{{ $oNotification->id }}" data-action="accept">Accept</span>
                    <span class="notification-btn" data-id="{{ $oNotification->id }}" data-action="decline">Decline</span>
                </div>
                @endif
            </span>   -->
        <!-- </a> -->
        

        <span class="user-thumbnail-sm p-1">
            <img class="w-100" src="{{ getUserImageUrl($oNotification->youUser->file_name,$oNotification->youUser->profile_pic_url) }}" alt="Profile Picture {{$oNotification->youUser->full_name}}">
        </span>
        <span  class="notification-text ml-1" >

            @if($oNotification->action == \App\Models\Notification::FOLLOW)
                
                {{ getUserName($oNotification->youUser->first_name,$oNotification->youUser->last_name) }} wants to follow you.

                @if($oNotification->attempt == 0)
                <div>
                    <span class="notification-btn" data-notification-id="{{ $oNotification->id }}" data-id="{{ $oNotification->entity_id }}" data-action="accept" data-user-name="{{ getUserName($oNotification->youUser->first_name,$oNotification->youUser->last_name) }}">Allow</span>
                    <span class="notification-btn" data-notification-id="{{ $oNotification->id }}" data-id="{{ $oNotification->entity_id }}" data-action="decline" data-user-name="{{ getUserName($oNotification->youUser->first_name,$oNotification->youUser->last_name) }}">Deny</span>
                </div>
                @endif

            @elseif($oNotification->action == \App\Models\Notification::FOLLOWING)
                
                {{ getUserName($oNotification->youUser->first_name,$oNotification->youUser->last_name) }} has accepted your follow request.

                @elseif($oNotification->action == \App\Models\Notification::UNFOLLOW)

                {{ getUserName($oNotification->youUser->first_name,$oNotification->youUser->last_name) }} has stopped following you.

                @elseif($oNotification->action == \App\Models\Notification::REJECT)

                {{ getUserName($oNotification->youUser->first_name,$oNotification->youUser->last_name) }} has declined your follow request.
                
            @endif

           <div class="nc-xs-text nc-light-gray-color"> {{ $oNotification->created_at->diffForHumans() }} </div>
        </span>
    @else
        {{-- For shared with followers note --}}
        <span class="user-thumbnail-sm p-1">
            <img class="w-100" src="{{ getUserImageUrl($oNotification->youUser->file_name,$oNotification->youUser->profile_pic_url) }}" alt="Profile Picture {{$oNotification->youUser->full_name}}">
        </span>
        <span  class="notification-text ml-1" style="color:white;">
            {{ getUserName($oNotification->youUser->first_name,$oNotification->youUser->last_name) }} shared new note with you".
        </span>
    @endif


</li>
@endforeach
@else
<li>
    <a href="">
        <span class="user-thumbnail-sm p-1">
            <span class="user-initials">-</span>
            <img class="w-100" src="" alt="">
        </span>
        <span  class="notification-text ml-1">
            No Notifications available!
        </span>  
    </a>
</li>
@endif