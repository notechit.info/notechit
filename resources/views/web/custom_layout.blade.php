<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ trans("messages.app_name") }} - @yield('title')</title>

        @include('web.includes')
        @yield('custom-header-css')
        @yield('custom-header-js')

        <style>
        .error{
            border:1px solid red;
        }

        #snackbar {
        visibility: hidden;
        min-width: 250px;
        margin-left: -125px;
        background-color: #333;
        color: #fff;
        text-align: center;
        border-radius: 2px;
        padding: 16px;
        position: fixed;
        z-index: 10000;
        left: 50%;
        bottom: 30px;
        font-size: 17px;
        }

        #snackbar.show {
        visibility: visible;
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
        }

        @-webkit-keyframes fadein {
        from {bottom: 0; opacity: 0;}
        to {bottom: 30px; opacity: 1;}
        }

        @keyframes fadein {
        from {bottom: 0; opacity: 0;}
        to {bottom: 30px; opacity: 1;}
        }

        @-webkit-keyframes fadeout {
        from {bottom: 30px; opacity: 1;}
        to {bottom: 0; opacity: 0;}
        }

        @keyframes fadeout {
        from {bottom: 30px; opacity: 1;}
        to {bottom: 0; opacity: 0;}
        }
        </style>

    </head>
    <body>
        <div id="snackbar">Some text some message..</div>

        @if(Session::has('error'))
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                <span class="text-semibold">Error!</span> {{ Session::get('error') }}.
            </div>
        @endif

        @if(Session::has('success'))
        <div class="success-login-wrapper" id="success-alert">
            <i class="d-block far fa-check-circle"></i>
            <div class=""> {{ Session::get('success') }}</div>
            @if(!Auth::user())
                <a href="#" data-toggle="modal" data-target="#loginModal" class="cta-btn">@lang('messages.login')</a>
            @else
                <a href="{{ route('web.home.index') }}" class="cta-btn">@lang('messages.home_page')</a>
            @endif
        </div>
        @endif





        @yield('header')
        @yield('content')
        @include('web.footer')
        @yield('custom-footer-js')
        @include('web.common_footer_scripts')
    </body>
</html>
