@extends('web.main')

@section('title')
    {{ trans('messages.home_page') }}
@endsection

@section('content')

<div class="container profile-wrapper">
  <div class="row">
    <div class="col-9">
        <div class="row align-items-center profile-top-section area">
            <ul class="circles">
                @foreach($oMyNotesTitle as $oTitle)
                  <li> {{ $oTitle->title }} </li>
                @endforeach
            </ul>
            <div class="col-auto m-t-b-15">
                <div class="user-pic-wrapper nc-cover-pic-wrapper">
                  <!-- <img class="jsClassPlaceHolderImg" src="{{asset('web/img/author.png')}}"/> -->
                  <img class="jsClassPlaceHolderImg" src="{{ getUserImageUrl($oUserDetail->file_name,$oUserDetail->profile_pic_url) }}"/>
                </div>
            </div>
            <div class="col m-t-b-15">
              <div class="user-name">
                {{ $oUserDetail->first_name ." ". $oUserDetail->last_name }}
              </div>

              @if($isFollwed)
                <button class=" nc-black-btn btn-sm mb-2 n-js-follow" data-id="{{ $oUserDetail->id }}" data-action="unfollow">Following</button>
              @else
                <button class=" nc-black-btn btn-sm mb-2 n-js-follow" data-id="{{ $oUserDetail->id }}" data-action="follow">Follow</button>
              @endif

              <!-- <button class=" unfollow-btn btn-sm mb-2">Unfllow</button> -->
              <div class="about-user scrollbar-pills">
                {{ $oUserDetail->about_me }}
              </div>
            </div>
        </div>
    </div>
      
    <div class="col-3 px-0 m-t-b-15">
      <span class="d-inline-block align-top followers cursor-pointor" onclick="switchProfileTab('followers_tab');">
        {{ $nFollowersCount }} <br> @lang('messages.followers')
        </span>
        <span class="d-inline-block align-top">
          <span class="following cursor-pointor" onclick="switchProfileTab('following_tab');">
            {{ $nFollowingCount }} <br> @lang('messages.following')
          </span> 
          <span class="d-block profile-option-3 cursor-pointor" onclick="switchProfileTab('notes_tab');">
            <i class="fa fa-cog"></i>  <div>{{ $nMyNoteCount }} @lang('messages.notes')</div> 
          </span>
        </span>

    </div>
  </div>
</div>

<div class="container">

  <div id="followers_tab" class="row jsClassProfielTab d-none">
    @include('web.user.followers')
  </div>
  <div id="following_tab" class=" row jsClassProfielTab d-none">
    @include('web.user.following')
  </div>
  <div id="notes_tab" class=" row jsClassProfielTab d-none">
    User notes will go here
  </div>

</div>

<script>
  // ===== Switch Profile Tab show / Hide =====
  function switchProfileTab(eleId){
    $(".jsClassProfielTab").addClass('d-none');
    $("#"+eleId).removeClass('d-none');
  }

  $("body").on("click",".n-js-follow",function(){
      var auth = "{{ \Auth::user() ? 1 : 0}}";
      if(auth == 0)
      {
          showToast("Please sign in to follow!");
      }

      var user_id = $(this).attr('data-id');
      var action = $(this).attr('data-action');

      $.ajax({
          method : "GET",
          url: "{{ route('web.user.follow') }}",
          data : { 
              user_id : user_id,
              action : action
          },
          cache: false,
          success: function(response){
              if(response.status == 1)
              {
                  showToast(response.message);
                  setTimeout(() => {
                      window.location.reload();
                  }, 1000);
              }
          },error : function(error){ console.log('Error');}
      });

  });//End of click

</script>
@endsection