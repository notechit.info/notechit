<script>
    // ===== common js initializations =====

    // ===== Bootstrap Tooltip show =====
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    // ===== Hide Signup and Forget password When Popup Opens =====
    $('#loginModal').on('hide.bs.modal', function () {
        $('.jsClassSignupForm').hide();
        $('.jsClassForgetPassword').hide();
        $( ".jsClassLoginForm" ).show();
    });

    function showToast(msg = "Note-Chit") {
        console.log(msg);
        $('#snackbar').html(msg);
        var x = document.getElementById("snackbar");
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }

    function showErrorToast(msg = "Note-Chit") {
        console.log(msg);
        $('#snackbar').html(msg);
        $('#snackbar').css("background-color","red");
        var x = document.getElementById("snackbar");
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }

    $("body").on("click",".n-js-fav",function(){
        selectedElement = $(this);
        mainDiv = selectedElement.parent('div').parent('div');

        var auth = "{{ \Auth::user() ? 1 : 0}}";
        if(auth == 0)
        {
            showToast("Please sign in to save note!");
        }

        var note_id = $(this).attr('data-id');
        var action = $(this).attr('data-action');

        swal({
        title: "Are you sure you want to remove?",
        //text: "You will not be able to recover note!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    method : "GET",
                    url: "{{ route('web.note.remove-note-from-folder') }}",
                    data : { 
                        note_id : note_id,
                        action : action
                    },
                    cache: false,
                    success: function(response){
                        if(response.status == 1)
                        {
                            showToast(response.message);
                            _getBucketNotes($('#currentBucketId').val());

                            // remove save note code removed from dashboard
                            // mainDiv.children('.nc-xs-text').remove();
                            // mainDiv.append('<span class="note-card-save-btn nc-black-btn js-bucket-modal" data-note-id="'+note_id+'" data-action="add" data-id="'+note_id+'">Save</span>');
                        }
                    },error : function(error){ console.log('Error');}
                });
            } else {
                //swal("Your note is safe!");
            }
        });
    });//End of click
</script>