<!-- ===== Bucket list Popup ===== START -->

<!-- Modal -->
<div class="modal fade md-modal" id="bucketListModal" tabindex="-1" role="dialog" aria-labelledby="bucketListModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

        <div class="nc-xlg-text modal-title text-center ">@lang('messages.save_note_to')</div>
        <div class="bucketListPopupWrapper">
            <ul class="jsClassBucketList">
                <li class="" data-action-type="simple" onClick="selectBucket(this)">Saved Notes</li>
                <!-- <li class="bucket-label nc-light-color nc-xs-text clearfix">@lang('messages.your_bucket_list')
                    <span class="float-right create-bucket-modal" onClick="createBucket(this)">    
                        <i class="fa fa-plus nc-xs-text nc-black-color"></i>   
                        @lang('messages.create_bucket')
                    </span>
                </li> -->
                
                <!-- <li class="" onClick="selectBucket(this)">
                    <img class="bucket-icon" src="{{asset('web/img/bucket-icon.svg')}}" alt="Bucket-icon">
                    <span class="d-inline-block align-top">
                        Physics
                        <div class="nc-light-color nc-xs-text">11 Notes</div>
                    </span>
                </li>

                <li class="active" onClick="selectBucket(this)">
                    <img class="bucket-icon" src="{{asset('web/img/bucket-icon.svg')}}" alt="Bucket-icon">
                    <span class="d-inline-block align-top">
                        Physics
                        <div class="nc-light-color nc-xs-text">11 Notes</div>
                    </span>
                </li> -->
               
            </ul>
        </div>
        
        <button id="save_note_to_bucket_btn" data-action-type="simple" data-bucket-id="0" data-note-id="0"  class="nc-black-btn nc-register-btn">@lang('messages.save')</button>
      </div>
      
    </div>
  </div>
</div>

<!-- ===== Report Note Popup ===== END -->

<script>

    $("body").on('click','.js-bucket-modal',function(){

        var auth = "{{ \Auth::user() ? 1 : 0}}";
        if(auth == 0)
        {
            showToast("Please sign in to save!");
            return false;
        }
        
        $('#bucketListModal').modal('show');
        var nNoteId = $(this).attr('data-note-id');
        _getFoldersForBucketPopup(nNoteId);
    });

    function selectBucket(ele,nBucketId){
        $(ele).siblings().removeClass('active');
        $(ele).addClass('active');
        var sActionType = $(ele).attr('data-action-type');

        if(sActionType == "simple")
        {
            $('#save_note_to_bucket_btn').attr("data-bucket-id",0);
        }
        else
        {
            $('#save_note_to_bucket_btn').attr("data-bucket-id",nBucketId);
        }

        $('#save_note_to_bucket_btn').attr("data-action-type",sActionType);
    }

    function createBucket(ele){
        var sNewBucketHtml = '<li class="active" onClick="selectBucket(this)"> <img class="bucket-icon" src="{{asset("web/img/bucket-icon.svg")}}" alt="Bucket-icon"><span class="d-inline-block align-top">'+
                             '   <input type="text" class="form-control bucket-name-input jsClassBucketName" value="Untitled Bucket" onblur = "saveBucket(this)"/> </span></li>'

        $(sNewBucketHtml).insertAfter('.jsClassBucketList li:nth-child(2)').fadeIn('slow');
        $(".jsClassBucketName").select();
    }

    function saveBucket(input){
        $(".jsClassBucketList li").removeClass('active');
        $(".jsClassBucketList li:nth-child(3)").addClass('active');
        if(input.value){
            var sHtml= "<div class='bucket-name jsClassBucketName'>"+input.value+"</div>";
            $( input ).replaceWith(sHtml);
        }
        else{
            var sHtml= "<div class='bucket-name jsClassBucketName'>Untitled Bucket</div>";
            $( input ).replaceWith(sHtml);
        }
    }

    function _getFoldersForBucketPopup(nNoteId)
    {
        // $('.jsClassBucketList').html('');
        $.ajax({
            method : "GET",
            url: "{{ route('web.bucket.get-user-buckets') }}",
            data : { nNoteId : nNoteId},
            cache: false,
            success: function(response){
                // $('.jsClassBucketList').append(response.sHtml);
                $('.jsClassBucketList').html(response.sHtml);
                $('#save_note_to_bucket_btn').attr("data-note-id",response.nNoteId);
            },error : function(error){ 
                console.log('Bucket Popup Folders Error');
            }
        });
    }

    $("body").on('click','#save_note_to_bucket_btn',function(){

        var auth = "{{ \Auth::user() ? 1 : 0}}";
        if(auth == 0)
        {
            showToast("Please sign in to save!");
            return false;
        }   

        nodeID = $(this).attr('data-note-id');
        folderId = $(this).attr('data-bucket-id');
        action = $(this).attr('data-action-type');

        if(action == 'simple')
        {
            showToast("Please select bucket!");
            return false;
        }

        _saveNoteToFolder(nodeID,folderId,action);
    });

    function _saveNoteToFolder(nodeID,folderId,action)
    {
        $.ajax({
            method : "POST",
            url: "{{ route('web.bucket.save-to-user-bucket') }}",
            data : { 
                _token : "{{ csrf_token() }}",
                nodeID : nodeID,
                folderId : folderId,
                action : action               
            },
            cache: false,
            success: function(response){
                showToast(response.message);
                if(response.status == 1)
                {
                    $('#bucketListModal').modal('hide');

                    //Removed remove option from home page
                    // var savedHtml = '<span class="nc-xs-text">Saved</span><div class="nc-xs-text font-weight-bold" data-note-id="'+nodeID+'" data-action="add" data-id="'+nodeID+'"><span class="fa fa-folder"></span> '+$('ul.jsClassBucketList li.active').data('bucket-name')+'<span class="note-card-save-btn nc-black-btn n-js-fav"data-action="remove" data-id="'+nodeID+'">Remove</span></div>';
                    var savedHtml = '<span class="nc-xs-text">Saved</span><div class="nc-xs-text font-weight-bold" data-note-id="'+nodeID+'" data-action="add" data-id="'+nodeID+'"><span class="fa fa-folder"></span> '+$('ul.jsClassBucketList li.active').data('bucket-name')+'</div>';

                    $('.js-bucket-modal').each(function(){
                        if($(this).data('note-id') == nodeID){
                            $(this).after(savedHtml);
                            $(this).remove();
                        }
                    });                    
                }
                return false;
            },error : function(error){ 
                console.log('Bucket Popup Folders Error');
            }
        });
    }
    
</script>