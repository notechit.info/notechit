<nav id="navbar" class="navbar  navbar-expand-sm navbar-dark nc-navbar-dark ">
    <a class="navbar-brand nc-white-color" href="{{ route('web.home.index') }}">
        <img src="{{asset('web/img/logo.png')}}" class="d-inline-block align-top" alt="">
        <span class="disp-ib align-middle">Notechit</span>
    </a>
    <div id="top_searchbar_wrapper" class="nc-top-searchbar-wrapper">
        <input id="nc-top-searchbar" class="nc-top-searchbar" type="search" placeholder="Search Notes" aria-label="Search" value="{{$searchData ?? ''}}">
    </div>

    @include('web.nav_header')
    <!-- @include('web.app_header') -->

</nav>

<style>
    .ui-autocomplete { left:15%!important;top:6% !important; position: absolute; cursor: default;z-index:5000 !important;}
</style>

<!-- <link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link> -->
<!-- <script src="YourJquery source path"></script> -->
<!-- <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script> -->

<link type="text/css" rel="stylesheet" href="{{ asset('web/js/autocomplete/css/jquery-ui.css') }}">

<script>
$( function() {
    var searchRequest = null;
    $( "#nc-top-searchbar").autocomplete({
        minLength: 1,
        appendTo: "#top_searchbar_wrapper",
        source: function(request, response) {
            if (searchRequest !== null) {
                searchRequest.abort();
            }

            searchRequest = $.ajax({
                url: '{{ route('web.note.search-notes') }}',
                method: 'get',
                dataType: "json",
                data: {term: request.term},
                success: function(data) {
                    searchRequest = null;
                    response($.map(data.response, function(response) {
                        return {
                            value: response.note_id,
                            label: response.title,
                            image: response.image,
                            user_name : response.name,
                            search_term: response.search_term
                        };
                    }));
                }
            }).fail(function() {
                searchRequest = null;
            });
        },
        select: function (e, ui) {
            if(ui.item.value == 0) {
                $("#nc-top-searchbar").val(ui.item.search_term);
                window.location.href = "{{ route('web.home.index') }}?q="+ui.item.search_term;
            }else if(ui.item.value == null){
                return;
            }
            else {
                $("#nc-top-searchbar").val(ui.item.label);
                redirectToUrl(ui.item.value);
            }
        },
        focus: function(event, ui) {
            event.preventDefault();
            $("#nc-top-searchbar").val(ui.item.label);
        }
    }).data('ui-autocomplete')._renderItem = function(ul, item){
        if(item.value == 0 || item.value == null){
            return $('<li class="ui-menu-item" role="presentation"><a id="ui-id-'+item.value+'" class="ui-corner-all" tabindex="-1"><span>'+item.label+'</span></a></li>')
                .data("item.autocomplete", item)
                .appendTo(ul);
        }
        return $('<li class="ui-menu-item" role="presentation"><a id="ui-id-'+item.value+'" class="ui-corner-all" tabindex="-1"><span class="user-thumbnail-sm nc-tag-bg-2"><img class="w-100" src="'+item.image+'"></span><span> '+item.label+' - '+item.user_name+'</span></a></li>')
            .data("item.autocomplete", item)
            .appendTo(ul);
    };
} );

function redirectToUrl(id)
{
    window.location.href = "{{ route('web.note.note-detail') }}/"+id;
}
</script>

