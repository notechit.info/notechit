<nav id="secondary_navbar" class="nc-tab-navbar">
    <ul>
        <li><a href="{{ route('web.home.index') }}" class="{{ \Request::route()->getName() == 'web.home.index' ? 'active' : '' }}">Home</a></li>
        <li><a href="{{ route('web.note.my-notes') }}" class="{{ \Request::route()->getName() == 'web.note.my-notes' ? 'active' : '' }} {{ \Request::route()->getName() == 'web.note.following-users-notes' ? 'active' : '' }} {{ \Request::route()->getName() == 'web.note.saved-notes' ? 'active' : '' }}">NoteChits</a></li>
        <li><a href="{{ route('web.bucket.user-buckets') }}" class="{{ \Request::route()->getName() == 'web.bucket.user-buckets' ? 'active' : '' }}">Buckets</a></li>
        <li><a href="{{ route('web.home.all-queries') }}" class="{{ \Request::route()->getName() == 'web.home.all-queries' ? 'active' : '' }} {{ \Request::route()->getName() == 'web.home.my-queries' ? 'active' : '' }} {{ \Request::route()->getName() ==  'web.home.new-query' ? 'active' : '' }}">Contribution</a></li>
        
    </ul>
</nav>
<script>
//=====  Set top nav bar hide & show on scroll =====
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
        var currentScrollPos = window.pageYOffset;
        if (prevScrollpos > currentScrollPos) {
            document.getElementById("navbar").style.top = "0";
            document.getElementById("navbar").classList.remove("nav-bar-shadow");
        } else {
            document.getElementById("navbar").style.top = "-50px";
            document.getElementById("navbar").classList.add("nav-bar-shadow");
        }
        prevScrollpos = currentScrollPos;
    }
</script> 