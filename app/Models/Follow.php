<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    const ACTIVE = 1, IN_ACTIVE = 0;

    protected $fillable = [
        'follower_user_id',
        'following_user_id',
        'activated',
        'deleted'
    ];

    public function scopeActive($query)
    {
        return $query->where('follows.activated',self::ACTIVE)
                    ->where('follows.deleted',self::IN_ACTIVE);
    }

    /**
     * Get followers list
     */
    public static function getFollowersUserList($nUserId)
    {
        return Follow::from('follows as f')
                    ->leftJoin('users as u','f.following_user_id','=','u.id')
                    ->where('f.follower_user_id',$nUserId)
                    ->where('f.activated',1)
                    ->where('f.deleted',0)
                    ->select(
                        'u.id as user_id',
                        'u.first_name as first_name',
                        'u.last_name as last_name',
                        'u.email as email',
                        'u.file_name as file_name',
                        'u.profile_pic_url as profile_pic_url'                        
                    )
                    ->get();
    }

    /**
     * Get following list
     */
    public static function getFollowingUserList($nUserId)
    {
        return Follow::from('follows as f')
                    ->leftJoin('users as u','f.follower_user_id','=','u.id')
                    ->where('f.following_user_id',$nUserId)
                    ->where('f.activated',1)
                    ->where('f.deleted',0)
                    ->select(
                        'u.id as user_id',
                        'u.first_name as first_name',
                        'u.last_name as last_name',
                        'u.email as email',
                        'u.file_name as file_name',
                        'u.profile_pic_url as profile_pic_url'                        
                    )
                    ->get();
    }

    
}
