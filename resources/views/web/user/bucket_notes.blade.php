<input type="hidden" name="currentBucketId" id="currentBucketId" value="{{$bucketId}}">
@if(count($oNotes) > 0)
    <?php $i = 1; ?>  
    @foreach($oNotes as $oNote)  
        <div class="col-lg-4 col-md-6 col-sm-12 note-card-bg-{{$i}}">
            <?php 
                if($i > 11){
                $i=1;
                }
                else{
                $i++; 
                }
            ?>  
            @include('web.notes.single_note_card')
        </div>
    @endforeach
@else
<div class="col no-data-wrapper py-5">
    <div class="info-text">@lang('messages.no_added_notes')</div>
    <a href="{{route('web.note.add-note')}}" class="cta-btn">@lang('messages.no_added_sub_notes')</a> 
</div>
@endif