@extends('web.admin.layout')
@section('title')
    Category
@endsection
@section('page_title')
    Category
@endsection
@section('css')
<style>
div.dt-buttons {
    float: right;
}
.has-error .error {
    color: red;
}

.categroy-image-datatable {
    width: 170px;
    height: 100px;
}
</style>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table id="category-datatable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Image</th>
                            <th>URL</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addCategoryModel" tabindex="-1" role="dialog" aria-labelledby="addCategoryModelLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form id="form" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="addCategoryModelLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="submitType" id="submitType" > 
                    <input type="hidden" name="id" id="id"> 
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Category Name:</label>
                            <input type="text" name="category_name" id="category_name" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Category Image:</label>
                            <input type="file" name="image" id="image" class="form-control" accept="image/*">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" id="btnSave" value="Save">
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('js')
<script>

    var dt = $('#category-datatable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: {
            url:'{{route("admin.category.datatable")}}',
            type:'POST',
        },
        columns: [
            {data: 'DT_RowIndex'},
            {data: 'category_name'},
            {data: 'image'},
            {data: 'url'},
            {data: 'action'},
        ],
        dom: 'Bfrtip',
        buttons: [
            {
                text: 'Add Category',
                className : 'btn-sm ml-2 mr-2 text-right',
                action: function ( e, dt, node, config ) {
                    showAdd();
                }
            }
        ]
    });

    function showAdd(){
        $('#addCategoryModel').modal('show');
        $('#addCategoryModelLabel').text('Add Category');
        $('#submitType').val('Add');
        $('#id').val('');
    }

    $(document).ready(function () {
        jQuery.validator.addMethod("imageRequired", function(value, element) {
            if($('#submitType').val() == "Add"){
                if ($("#image")[0].files.length === 0) {
                    return false;
                }
            }
            return true;
        }, "Please select image.");

        $("#form").validate({
            ignore: ":hidden",
            rules: {
                category_name: {
                    required: true,
                    remote: {
                        url: "{{route('admin.category.check_name')}}",
                        type: "POST",
                        data: {
                            _token: function(){ return "{{ csrf_token() }}"; },
                            id: function(){ return $("#id").val(); },
                            category_name: function(){ return $("#category_name").val(); },
                        }
                    },
                },
                image: {
                    imageRequired: true,
                    extension: "jpg|jpeg|png|ico|bmp"
                },
            },
            messages:{
                category_name: {
                    required: "Please enter category name",
                    remote: "Category already created" 
                },
                image: {
                    required: "Please upload category image",
                    extension: "Please valid image type",
                }
            },
            highlight: function (element, errorClass) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element, errorClass) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {
                var formData = new FormData(form);
                if($('#submitType').val() == "Add"){
                    url = "{{route('admin.category.store')}}"; 
                }else{
                    url = "{{route('admin.category.update')}}"; 
                }
                $.ajax({
                    type:'POST',
                    url: url,
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        toastr.success(data);
                        clearForm();
                        dt.ajax.reload();
                    },
                    error: function(data){
                        toastr.warning(data);
                    }
                });
            }
        });
    });

    function clearForm(){
        $('#addCategoryModel').modal('hide');
        $("#form").trigger('reset');
        $('#addCategoryModelLabel').text('');
        $('#category_name').val('');
        $('#image').val('');
        $('#btnSave').text('');
        $('#submitType').val('');
        $('#id').val('');

    }

    $(document).on('click', '.deleteCategory',function(){
        $.ajax({
            type:'post',
            url: "{{route('admin.category.delete')}}",
            data:{
                id:$(this).data('id'),
            },
            success:function(data){
                toastr.success(data);
                dt.ajax.reload();
            },
            error: function(data){
                toastr.warning(data);
            }
        });
    });

    $(document).on('click', '.editCategory', function(){
        clearForm();
        data_id = $(this).data('id');
        urlEdit = "{{ route('admin.category.edit', ':id') }}";
        urlEdit = urlEdit.replace(':id', data_id);        

        $.ajax({
            type:'get',
            url: urlEdit,
            success:function(data){
                $('#addCategoryModel').modal('show');
                $('#addCategoryModelLabel').text('Edit Category');
                $('#submitType').val('Update');
                $('#id').val(data.id);
                $('#category_name').val(data.category_name);
            },
            error: function(data){
                toastr.warning(data);
            }
        });

    });
</script>
@endsection