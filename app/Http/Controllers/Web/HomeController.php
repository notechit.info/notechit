<?php

namespace App\Http\Controllers\Web;

use App\Models\Note;
use App\Models\Category;
use App\Models\UserCategory;
use App\Models\NoteCategory;
use App\Models\Favourite;
use App\Models\ContributionQuery;
use App\Models\ContributionQueryCategory;
use App\Models\Rating;
use App\Mail\ContactEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\Web\WebController;
use Illuminate\Support\Facades\Auth;
use Exception;
use Session;

class HomeController extends WebController
{
    public function home(Request $oRequest)
    {
        $showCategorySelection = $isFirstLogin = false;
        $searchData = $oRequest->query('q');
        $oCurrentUserSavedNotes = [];

        if(\Auth::user()){ // Return notes as per user area of interest other wise return all public note
            $aCategoryIds = UserCategory::getUserCategories(\Auth::user()->id)->pluck('category_id')->toArray();
            if(count($aCategoryIds) > 0){
                $oNotes = NoteCategory::getHomePageNotesAsPerUserInterest($aCategoryIds, true, $searchData);

                //If no note found on user intreset then show public notes to user
                if($oNotes->count() == 0){
                    $oNotes = Note::getNotes(Note::PUB, true, $searchData);
                    $showCategorySelection = true;
                }
            }else{
                $oNotes = Note::getNotes(Note::PUB, true, $searchData);
                $showCategorySelection = true;
            }
            $oCurrentUserSavedNotes = Favourite::where([
                'user_id' => \Auth::user()->id,
                'activated' => 1,
                'deleted' => 0
            ])->get();
        }else{
            $oNotes = Note::getNotes(Note::PUB, true, $searchData);
        }

        $oCategories = Category::active()->get();

        $nAvgRating = 0;
        foreach($oNotes as $key => $oNote){
            $nAvgRating = 0;
            $nTotalStars = Rating::where('note_id',$oNote->note_id)->active()->sum('star');
            $nTotalUsers = Rating::where('note_id',$oNote->note_id)->active()->count();

            $nAvgRating = $nTotalStars > 0 ? round($nTotalStars / $nTotalUsers) : 0;
            $oNotes[$key]['nAvgRating'] = $nAvgRating;
        }

        if(Session::has('isRegistered')){
            Session::forget('isRegistered');
            $isFirstLogin = true;
        }

        return view("web.home.index", compact('oNotes','oCategories','oCurrentUserSavedNotes','showCategorySelection','isFirstLogin', 'searchData'));
    }

    public function userBucket()
    {
        return view("web.user.user_buckets");
    }

    public function aboutUs()
    {
        return view("web.home.about_us");
    }

    public function contactUs()
    {
        return view("web.home.contact_us");
    }

    public function createContactUs(Request $oRequest)
    {
        $this->validate($oRequest,[
            'name' => 'required',
            'email' => 'required|email',
            'query' => 'required',
            'message' => 'required'
        ]);

        // try
        // {
            $arr = [
                'name' => $oRequest->get('name'),
                'email' => $oRequest->get('email'),
                'query' => $oRequest->get('query'),
                'message' => $oRequest->get('message')
            ];

            \Mail::to(config('constants.CONTACT_EMAIL_RECEIVER'))->send(new ContactEmail($arr));
        // }
        // catch (\Exception $e) {}

        return redirect()->back()->with(['success' => 'Query successfully sent']);
    }

    public function notFound()
    {
        return view("errors.404");
    }

    public function allQueries()
    {
        $oQueries = array();
        if(\Auth::user())
        {
            $aCategoryIds = UserCategory::getUserCategories(\Auth::user()->id)->pluck('category_id')->toArray();
            $oQueriesFormCategories = ContributionQueryCategory::whereIn('category_id',$aCategoryIds)->get()->pluck('contribution_query_id')->toArray();
            $oQueries = ContributionQuery::with(['user', 'comments', 'comments.user:id,first_name,last_name,profile_pic_url,file_name'])->whereIn('id',$oQueriesFormCategories)
                ->orderBY('id', "DESC")
                ->active()
                ->where('is_expired', 0)
                ->get();
        }

        return view("web.user.all_queries", compact('oQueries'));
    }

    public function myQueries()
    {
        $oQueries = array();
        if(\Auth::user())
        {
            $oQueries = ContributionQuery::with(['user'])->where('user_id',\Auth::user()->id)->orderBY('id', "DESC")->active()->get();
        }

        return view("web.user.my_queries", compact('oQueries'));
    }

    public function newQuery()
    {
        $oCategories = Category::active()->get();

        return view("web.user.new_query", compact('oCategories'));
    }

    public function addQuery(Request $oRequest)
    {
        if (\Auth::user()) {
            $oRequest->validate([
            'query_text' => 'required|min:10',
            ], [
                'query_text.*'=> trans('messages.contribution_query_validation')
            ]);

                $oNewContributionQuery = ContributionQuery::firstOrNew([
                'user_id' => \Auth::user()->id,
                'query_text' => $oRequest->query_text
            ]);


            $oNewContributionQuery->deleted = 0;
            $oNewContributionQuery->activated = 1;
            $oNewContributionQuery->save();

            if ($oNewContributionQuery && $oNewContributionQuery->id) {
                if ($oRequest->has('categories') && count($oRequest->categories) > 0) {
                    foreach ($oRequest->categories as $category) {
                        $oContributionQueryCategory = ContributionQueryCategory::firstOrNew([
                        'contribution_query_id' => $oNewContributionQuery->id,
                        'category_id' => $category
                    ]);
                        $oContributionQueryCategory->save();
                    }
                }

                return redirect()->back()->with(['message'=> trans('messages.contribution_query_added_success')]);
            }
        }

        return redirect()->back()->with(['error'=> 'Please sign in']);
    }

    public function deleteQuery(Request $oRequest, $id) {

        try {

            $data = ContributionQuery::where([
                'id' => $id,
                'user_id' => Auth::user()->id
            ])->latest()->first();

            if(empty($data)) {
                return redirect()
                    ->back()
                    ->with([
                        'error' => 'Query not found.'
                    ]);
            }

            $data->activated = 0;
            $data->deleted = 1;
            $data->save();

            return redirect()
                    ->back()
                    ->with([
                        'success' => 'Query removed successfully.'
                    ]);
        }
        catch ( Exception $ex ) {

            return redirect()
                ->back()
                ->with([
                    'error' => __('messages.error_message')
                ]);
        }
    }

    public function categoryNotes($url){
        $category = Category::where([
            'url'       =>  $url,
                'activated'  =>  1,
                'deleted'   =>  0
            ])->latest()->first();

        if (! $category) {
            abort(404);
        }


        $noteIds = NoteCategory::where([
                'category_id'   =>  $category->id,
                'activated'     =>  1,
                'deleted'       =>  0
            ])->pluck('note_id');
        
        $oNotes = Note::whereIn('id', $noteIds)
            ->paginate(config("constants.WEB_PER_PAGE_RECORDS"));

        return view("web.category", compact('oNotes','category'));
    }
}
