<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    const IN_ACTIVE = 0 , ACTIVE = 1;
    
    protected $fillable = [
        'category_name',
        'image',
        'url',
        'activated',
        'deleted'
    ];

    public function scopeActive($query)
    {
        return $query->where('categories.activated',self::ACTIVE);
    }

    public function setUrlAttribute($value)
    {
        $urlArray = [];
        $words = explode(' ', strtolower($value));
        foreach($words as $word){
            $urlArray[] = preg_replace('/[^A-Za-z0-9\-]/', '', $word); 
        }
        $this->attributes['url'] = implode('-', $urlArray);
    }
}