<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FollowRequest extends Model
{
    const IN_ACTIVE = 0 , ACTIVE = 1;
    const PENDING = 'PENDING',ACCEPT = 'ACCEPT',DECLINE = 'DECLINE';
    const FF = 'FF';
    
    protected $fillable = [
        'user_id',
        'from_user_id',
        'to_user_id',
        'type',
        'ff_status',
        'activated',
        'deleted'
    ];

    public function scopeActive($query)
    {
        return $query->where('follow_requests.activated',self::ACTIVE);
    }

    public function youUser(){
        return $this->belongsTo(User::class,'from_user_id','id');
    }
}
