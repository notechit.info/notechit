@if(\Auth::user())
<div class="col border-right px-5">
  <h6 class="pb-3"> @lang('messages.edit_profile')</h6>
  
  <div class="" id="edit-profile-alert"></div>

  <form id="edit_profile_form" action="{{ route('web.user.profile',['id' => Auth::user()->id]) }}" method="POST">
              
    {{ csrf_field() }}
  
    <div class="form-row">
      
      <div class="form-group col-md-6">
        <label for="edit_first_name" class =" nc-xs-text">@lang('messages.first_name')</label>
        <input type="text" class="form-control" name="edit_first_name" id="edit_first_name" placeholder="" value="{{ Auth::user()->first_name }}">
      </div>

      <div class="form-group col-md-6">
        <label for="edit_last_namee" class="nc-xs-text">@lang('messages.last_name')</label>
        <input type="text" class="form-control" name="edit_last_name" id="edit_last_name" placeholder="" value="{{ Auth::user()->last_name }}">
      </div>

    </div>
    
    <div class="form-group">
      <label for="edit_about_me" class="nc-xs-text">@lang('messages.about') You</label>
      <textarea class="form-control" name="edit_about_me" id="edit_about_me" rows="3">{{ Auth::user()->about_me }}</textarea>
    </div>

    <button type="button" id="edit_profile_form_submit_btn" class=" nc-black-btn nc-register-btn">@lang('messages.save_changes')</button>

  </form>

</div>

<div class="col px-5">
  <h6 class="pb-3 profile-password-tab">@lang('messages.change_password')</h6>

  <div class="" id="change-password-alert"></div>

  <form id="change_password_form" action="{{ route('web.user.change-password') }}" method="POST">
                
      {{ csrf_field() }}

      <div class="form-group">
        <label for="current_password" class =" nc-xs-text">@lang('messages.current_password')</label>
        <input type="password" class="form-control" name="current_password" id="current_password" placeholder="" value="">
      </div>
      
      <div class="form-group">
        <label for="new_password" class="nc-xs-text">@lang('messages.new_password')</label>
        <input type="password" class="form-control"name="new_password" id="new_password" placeholder="" value="">
      </div>

      <button type="button" id="change_password_form_submit_btn" class=" nc-black-btn nc-register-btn">@lang('messages.save')</button>

  </form>

</div>

<script>

/**Submit Edit profile form*/
  $.ajaxSetup({
      headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
  });

  var editProfileOptions = { 
          beforeSubmit:  showEditProfileRequest,
          success:       showEditProfileResponse,
          error:showEditProfileError,
          dataType: 'json' 
  }; 

  function showEditProfileRequest() {
      $("#validation-errors").hide().empty();
      return true;
  }

  function showEditProfileResponse(response) {
    if(response.status == 0)
    {
      $.each(response.errors, function(key,val){
        $('#'+key).addClass('error');
      });

      return false;
    }

    var sHtml = '';
    sHtml +='<div class="alert alert-success">'
    sHtml +='<strong>Updated!</strong>'
    sHtml +='</div>'

    $('#edit-profile-alert').html(sHtml);
    
    //Auto Hide Success message
      $('#edit-profile-alert').show();
      setTimeout(function(){ 
        $('#edit-profile-alert').slideUp();
        
      }, 3000);

  }

  function showEditProfileError(xhr) {
      console.log("inside error");
  }

  $('body').on('click','#edit_profile_form_submit_btn',function(){
    $('#edit_profile_form').ajaxForm(editProfileOptions).submit();
  });
  /**Submit Edit profile form*/

  /** change password form */
  var changePasswordOptions = { 
          beforeSubmit:  showChangePasswordRequest,
          success:       showChangePasswordResponse,
          error:showChangePasswordError,
          dataType: 'json' 
  }; 

  function showChangePasswordRequest() {
      $("#validation-errors").hide().empty();
      return true;
  }

  function showChangePasswordResponse(response) {
    if(response.status == 0)
    {
      $.each(response.errors, function(key,val){
        $('#'+key).addClass('error');
      });

      return false;
    }

    var sHtml = '';
    sHtml +='<div class="alert alert-success">'
    sHtml +='<strong>Updated!</strong>'
    sHtml +='</div>'

    $('#change-password-alert').html(sHtml);
    //Auto Hide Success message
    $('#change-password-alert').show();
      setTimeout(function(){ 
        $('#change-password-alert').slideUp();
        
      }, 3000);

  }

  function showChangePasswordError(xhr) {
      console.log("inside error");
  }

  $('body').on('click','#change_password_form_submit_btn',function(){
    $('#change_password_form').ajaxForm(changePasswordOptions).submit();
  });

/** change password form */
</script>

@endif
