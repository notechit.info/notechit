<?php
namespace App\AWS;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

class AwsS3
{
    /**
     * Upload file on AWS S3 Bucket
     */
    public static function s3UploadImage($oFile,$sFolder,$sFileName = '')
    {
        $sExtension = mb_strtolower($oFile->getClientOriginalExtension());
        // $sFileName = !empty($sFileName) ? $sFileName : date('dmyhis').'_'.time().'.'.$sExtension;
        $sFileName = !empty($sFileName) ? $sFileName : rand(00000,99999).date('dmyhis').time().'.'.$sExtension;

        $oS3 = \Storage::disk('s3');
        $filePath = '/'.$sFolder.'/' . $sFileName;
        $oS3->put($filePath, fopen($oFile, 'r+'), 'public');

        return $sFileName;
    } 


    public static function downloadFile($sFolder,$sFileName,$path)
    {
        // // $file = Storage::disk(config('filesystems.default'))->get('uploads/csv/' . $id . '/' . $import->filename );
        // $filePath = '/'.$sFolder.'/' . $sFileName;
        // $file = \Storage::disk('s3')->get($filePath);

        // file_put_contents("Tmpfile.zip", fopen("http://someurl/file.zip", 'r'));

        // $headers = [
        //     // 'Content-Type' => 'text/csv', 
        //     // 'Content-Description' => 'File Transfer',
        //     'Content-Disposition' => "attachment; filename={$sFileName}",
        //     'filename'=> $sFileName
        // ];

        // return response($file, 200, $headers);

        // try{
        //     $file_url = $path;
        //     $file_name  = $sFileName;//"VoteMix-Event-Entry-Ticket.pdf";

        //     $mime = \Storage::disk('s3')->getDriver()->getMimetype($file_url);
        //     $size = \Storage::disk('s3')->getDriver()->getSize($file_url);

        //     $response =  [
        //     'Content-Type' => $mime,
        //     'Content-Length' => $size,
        //     'Content-Description' => 'File Transfer',
        //     'Content-Disposition' => "attachment; filename={$file_name}",
        //     'Content-Transfer-Encoding' => 'binary',
        //     ];

        //     ob_end_clean();

        //     return \Response::make(Storage::disk('s3')->get($file_url), 200, $response);
        // }
        // catch(Exception $e){
        // }

    } 


    public static function downloadFile2($file_path,$yourFileName)
    {
        if (\Storage::disk('s3')->exists($file_path)) {
            $file =  \Storage::disk('s3')->get($file_path);
  
            $headers = [
                'Content-Disposition' => "attachment; filename={$yourFileName}",
                'filename'=> $yourFileName
            ];
  
            return response($file, 200, $headers);
        }
        else
        {
            dd('no data found');
        }
    }

}