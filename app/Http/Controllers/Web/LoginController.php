<?php
namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Bucket;
use App\Models\User;
use Socialite;
use Exception;
use Illuminate\Support\Facades\Auth;
use Session;
class LoginController extends Controller
{

    /**
    * Redirect the user to the Google authentication page.
    *
    * @return \Illuminate\Http\Response
    */
    public function redirectToProvider() {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from Google.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(Request $request) {
        try {
            $user = Socialite::driver('google')->user();

            $name = explode(' ',$user['name']);
            $create['first_name'] = $name[0];
            $create['last_name'] = isset($name[1]) ? $name[1] : '';
            $create['email'] = $user->email;
            $create['username'] = $user->email;
            $create['file_name'] = $user->avatar_original;
            $create['login_with'] = 'GMAIL';
            $create['login_id'] = $user->id;
            $create['login_count'] = 1;

            $currentUsers = User::where('email', $create['email'])->first();

            if(empty($currentUsers)) {
                $currentUsers = User::create($create);
                Bucket::defaultBucket($currentUsers->id);
                Session::put('isRegistered', true);
            }
            else if($currentUsers->login_with != 'GMAIL' ) {
                $currentUsers->update([
                    'first_name' => $name[0],
                    'last_name' => $name[1],
                    'login_with' => 'GMAIL',
                    'login_id' => $create['login_id'],
                    'login_count' => ($currentUsers->login_count + 1)
                ]);
            }
            else {
                $currentUsers->first_name = $name[0];
                $currentUsers->last_name = $name[1];
                $currentUsers->login_count = ++$currentUsers->login_count;
                $currentUsers->save();
            }

            Auth::login(User::find($currentUsers->id));

            return redirect()
                ->route('web.home.index');
        }
        catch (Exception $e) {
            return redirect()
                ->route('web.home.index')
                ->with('error', __('messages.error_message'));
        }
    }
}
