<div class="col-md-12" id="suggestionComments{{$oQuery->id}}">
    @if($oQuery->comments->count() > 0)
        <h6>comments</h6>
        @foreach($oQuery->comments as $comment)
            <div id="CommentSingle{{$comment->id}}">
                <div style="float: left;">
                    <span class="author-thumbnail-wrapper ">
                        @if($comment->user->file_name == "" && $comment->user->profile_pic_url == '')
                            <img class="img-fluid" src="{{asset('web/img/author.png')}}" alt="Profile image {{$comment->user->full_name}}">
                        @else 
                            <img class="img-fluid" src="{{ getUserImageUrl($comment->user->file_name,$comment->user->profile_pic_url) }}" alt="Profile image {{$oQuery->user->full_name}}">
                        @endif
                    </span>
                    <span class="d-inline-block author-name nc-sm-text">    
                        <a href="{{ route('web.user.profile',['id' => $comment->user->id]) }}">
                            {{ $comment->user->first_name.' '.$comment->user->last_name  }}
                        </a>
                    </span>
                </div>
                <div style="float: left; padding-left:20px; width:100%">
                    <a href="{{$comment->comment}}">{{$comment->comment}}</a>
                    @if($comment->user_id == Auth::user()->id)
                        <button class="btn btn-sm btn-danger float-right rm-contribution-comment" data-id="{{$comment->id}}" data-query="{{$oQuery->id}}"><i class="fa fa-trash"></i></button>
                    @endif
                </div>
            </div>
        @endforeach
    @endif
</div>