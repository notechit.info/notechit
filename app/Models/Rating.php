<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    const ACTIVE = 1, IN_ACTIVE = 0;

    protected $fillable = [
        'note_id',
        'user_id',
        'star',
        'activated',
        'deleted'
    ];

    public function scopeActive($query)
    {
        return $query->where('ratings.activated',self::ACTIVE)
                    ->where('ratings.deleted',self::IN_ACTIVE);
    }
}
