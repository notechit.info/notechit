<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBucketNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bucket_notes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('note_id')->unsigned();
            $table->bigInteger('bucket_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->boolean('activated')->default(1)->index();
            $table->boolean('deleted')->default(0)->index();
            $table->timestamps();

            $table->foreign('note_id')->references('id')->on('notes')->onDelete('cascade');
            $table->foreign('bucket_id')->references('id')->on('buckets')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bucket_notes');
    }
}
