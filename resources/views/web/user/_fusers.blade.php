@if($nStep == 1)
    @if(count($oFollowers) > 0)
    @foreach($oFollowers as $oUser)
    <div class="col-auto text-center py-3">
        <div class="user-profile-wrapper-70"></div>

        <a href="{{ route('web.user.profile',['id' => $oUser->user_id]) }}">
            <div class="">{{ getUserName($oUser->first_name,$oUser->last_name) }}</div>
        </a>
    </div>
    @endforeach
    @else
    <div class="row text-center">
        <div class=" col no-data-wrapper py-5">
            <div class="info-text py-1">No {{ $sFrom }} Available!</div>
        </div>
    </div>
    @endif
@endif

@if($nStep == 2)
<!-- area of interest -->
<div class="col-12 text-center">
    <div class="nc-xlg-text modal-title text-center">@lang('messages.choose_your_interests')</div>
    <div class="nc-sm-text"> @lang('messages.interest_info')</div> <br>
    <input id="filter" autocomplete="off" type="text" class="animate-search" name="search" placeholder="@lang('messages.search_categories')">

    <div class="category-list-wrapper">
        <ul class="cat-checkbox-wrapper text-center">
            @foreach($oCategories as $nIndex => $oCategory)
            <li clas="">
                <input type="checkbox" class="js-cat" data-id="{{ $oCategory->id }}" name="user_area_of_interest" {{ Auth::user()->categories->contains('category_id',$oCategory->id) ? 'checked' : '' }} value="{{ $oCategory->id }}" id="myCheckbox{{ $nIndex + 1 }}" />
                <label for="myCheckbox{{ $nIndex + 1 }}">
                <img class="" src="{{getCategoryImageUrl($oCategory->image)}}" alt="{{ $oCategory->category_name }}">
                <div class="">{{ $oCategory->category_name }}</div> 
                </label>
            </li>
            @endforeach
        </ul>
    </div>
    <!-- <button class=" nc-black-btn nc-register-btn" id="nc-js-user-interest-btn">Save</button>
    <div class="nc-xs-text cursor-pointor text-center my-2" data-dismiss="modal">Skip</div> -->

</div>
@endif

@if($nStep == 3)
    <div class="col-12 row">
        @php
            $i = 1;
        @endphp

        @if(count($oNotes) > 0)

            @foreach($oNotes as $oNote)
            <div class="col-lg-4 col-md-6 col-sm-12 note-card-bg-{{$i}}">
                @php
                if($i > 11){
                    $i=1;
                }
                else{
                    $i++;
                }
                @endphp

                @include('web.notes.single_note_card')
            </div>
            @endforeach

        @else
        <div class="col no-data-wrapper py-5">
            <div class="info-text py-1">@lang('messages.no_notes_available')</div>
            <!-- <div class="info-text font-weight-bold"><span class="nc-vibrant-color">Follow</span> @lang('messages.no_shared_sub_notes')</div> -->
        </div>
        @endif

    </div>
@endif

@include('web.notes._bucket_modal')
