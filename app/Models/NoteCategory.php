<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NoteCategory extends Model
{
    protected $fillable = [
        'note_id',
        'category_id',
        'activated',
        'deleted'
    ];

    public static function getNoteCategories($nNoteId)
    {
        return NoteCategory::from('note_categories as nc')
                ->leftJoin('categories as c','nc.category_id','=','c.id')
                ->where('nc.activated',1)
                ->where('nc.deleted',0)
                ->where('nc.note_id',$nNoteId)
                ->select(
                    'c.category_name as category_name',
                    'c.image as image',
                    'c.url as url',
                )->get();
    }

    /**
     * Return notes as per user area of interest
     */
    public static function getHomePageNotesAsPerUserInterest($aCategoryIds,$bIsPaginate = false, $searchData = '')
    {

        $nLoggedInUserId = \Auth::user() ? \Auth::user()->id : null;

        $query = Note::from('note_categories as nc')
                ->leftJoin('notes as n','nc.note_id','=','n.id')
                ->leftJoin('users as u','n.user_id','=','u.id')
                ->leftJoin('bucket_notes', function($query) use ($nLoggedInUserId) {
                    $query->on('bucket_notes.note_id', '=', 'n.id')
                        ->where([
                            'bucket_notes.user_id' => $nLoggedInUserId,
                            'bucket_notes.deleted' => 0
                        ]);
                })
                ->leftJoin('buckets','bucket_notes.bucket_id','=','buckets.id')
                ->where(function($query) use ($searchData) {
                    if(!empty($searchData)) {
                        $query->where('n.title', 'LIKE', '%' . $searchData . '%');
                    }
                })
                ->whereIn('nc.category_id',$aCategoryIds)
                ->where('n.activated',1)
                ->where('n.deleted',0)
                ->where('nc.activated',1)
                ->where('nc.deleted',0)
                ->where('n.note_type',Note::PUB)
                ->groupBy('nc.note_id')
                ->select(
                    'n.id as note_id',
                    'n.user_id as user_id',
                    'n.title as title',
                    'n.is_video_note as is_video_note',
                    'n.created_at as created_at',
                    'n.file_name as file_name',
                    'u.first_name as first_name',
                    'u.last_name as last_name',
                    'u.file_name as user_file_name',
                    'u.profile_pic_url as user_profile_pic_url',
                    'bucket_notes.id as bn_id',
                    'bucket_notes.deleted as bn_deleted',
                    \DB::raw('(SELECT count(*) as note_id FROM note_views AS nv WHERE nv.note_id = n.id AND activated = 1 AND deleted = 0) as total_views'),
                    'buckets.bucket_name'
                )
                ->orderBy('n.id','DESC');

        if($bIsPaginate)
            return $query->paginate(config("constants.WEB_PER_PAGE_RECORDS"));

        return $query->get();
    }

    public function note(){
        return $this->belongsTo(Note::class);
    }
}
