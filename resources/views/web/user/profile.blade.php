@extends('web.main')

@section('title')
    {{ trans('messages.home_page') }}
@endsection

@section('content')

<div class="container profile-wrapper">
  <div class="row">
    <div class="col-12 col-md-9">
      <div class="row align-items-center profile-top-section area">
          <ul class="circles">
                <!-- User's added note title will be displayed here up to max-10 notes  -->
                @foreach($oMyNotesTitle as $oTitle)
                  <li> {{ $oTitle->title }} </li>
                @endforeach
          </ul>
          <div class="col-auto m-t-b-15">
              <div class="user-pic-wrapper nc-cover-pic-wrapper">
              
              <form id="upload_profile_pic_form" action="{{ route('web.user.upload-profile-picture') }}" method="POST" enctype="multipart/form-data">

                {{ csrf_field() }}
                
                @if(Auth::user())
                  @if(Auth::user()->id == $oUser->id)
                    <input type="file" name="file" id="browse_file"/>
                  @endif
                @endif

                <!-- <img class="jsClassPlaceHolderImg" src="{{asset('web/img/author.png')}}"/> -->
                <img class="jsClassPlaceHolderImg" src="{{ getUserImageUrl($oUser->file_name,$oUser->profile_pic_url) }}" alt="note-cover">
                <img id="preview_image" class="d-none thumbnail-img" src=""/>

                <script>$('#browse_file').change(function(e) {
                  e.preventDefault();
                  $('#upload_profile_pic_form').submit();
                    return false;
                  });
                </script>

                @if(Auth::user())
                  @if(Auth::user()->id == $oUser->id)
                    <span class="edit-picture">@lang('messages.edit')</span>
                  @endif
                @endif

              </form>

              </div>
          </div>
          <div class="col m-t-b-15">
              <div class="user-name">
                  {{ $oUser->first_name ." ". $oUser->last_name }}
                  
                  @if(Auth::user() && Auth::user()->id == $oUser->id)
                  <i class="fa fa-pen px-1 align-middle nc-xs-text cursor-pointor" 
                      data-toggle="tooltip" data-placement="top" title="@lang('messages.edit_profile')"
                      onClick="switchProfileTab('edit_profile');"></i>
                  @endif

              </div>

              @if(Auth::user())
                @if(Auth::user()->id != $oUser->id)
                  @if($isFollwed)
                    <!-- <button class=" nc-black-btn btn-sm mb-2" data-id="{{ $oUser->id }}" data-action="unfollow">Following</button> -->
                    <div class="nc-xs-text">You are following  {{ $oUser->first_name ." ". $oUser->last_name }}  <span class="ml-2 n-js-follow" data-id="{{ $oUser->id }}" data-action="unfollow"> <u>Unfollow</u> </span></div>
                    <!-- <button class=" nc-black-btn btn-sm mb-2" data-id="{{ $oUser->id }}" data-action="unfollow">Following</button> -->
                    <!-- <button class=" nc-black-btn btn-sm mb-2 n-js-follow" data-id="{{ $oUser->id }}" data-action="unfollow">Unfollow</button> -->
                  @else
                    @if($isFollwRequested)
                      <label class=" nc-black-btn btn-sm mb-2">Requested</label>
                    @else 
                      <button class=" nc-black-btn btn-sm mb-2 n-js-follow" data-id="{{ $oUser->id }}" data-action="follow">Follow</button>
                    @endif
                  @endif
                @endif
              @else
                @if($isFollwed)
                  <button class=" nc-black-btn btn-sm mb-2 n-js-follow" data-id="{{ $oUser->id }}" data-action="unfollow">Following</button>
                @else
                  <button class=" nc-black-btn btn-sm mb-2 n-js-follow" data-id="{{ $oUser->id }}" data-action="follow">Follow</button>
                @endif
              @endif

              <div class="about-user scrollbar-pills">
              {{ $oUser->about_me }}
              </div>

          </div>
      </div>
    </div>
    <div class="col-12 col-md-3 px-md-0 m-t-b-15">
        <span class="d-inline-block align-top followers cursor-pointor" onclick="switchProfileTab('followers_tab');">
          {{ $nFollowersCount > 0 ? $nFollowersCount : 'No' }} <br> @lang('messages.followers')
        </span>
        <span class="d-inline-block align-top">
          <span class="following cursor-pointor" onclick="switchProfileTab('following_tab');">
            {{ $nFollowingCount > 0 ? $nFollowingCount : 'No' }} <br> @lang('messages.following')
          </span> 

          @if(Auth::user())
            @if(Auth::user()->id == $oUser->id)
            <span class="d-block profile-option-3 cursor-pointor" onclick="switchProfileTab('interest_tab');">
              <i class="fa fa-cog"></i>  <div>@lang('messages.interests')</div> 
            </span>
            @else
            <span class="d-block profile-option-3 cursor-pointor" onclick="switchProfileTab('notes_tab');">
              <i class="fa fa-cog"></i>  <div>{{ count($oMyNotesTitle) > 0 ? count($oMyNotesTitle) : ''  }} @lang('messages.notes')</div> 
            </span>
            @endif
          @endif
          
          @if(!Auth::user())
          <span class="d-block profile-option-3 cursor-pointor" onclick="switchProfileTab('notes_tab');">
            <i class="fa fa-cog"></i>  <div>{{ count($oMyNotesTitle) > 0 ? count($oMyNotesTitle) : ''  }} @lang('messages.notes')</div> 
          </span>
          @endif
          
        </span>
    </div>
  </div>
  
</div>
<div class="container">
    <div id="edit_profile" class="row jsClassProfielTab">
      @include('web.user.edit_profile')
    </div>
    <div id="followers_tab" class="row jsClassProfielTab d-none">
      <!--include('web.user.followers')-->
    </div>
    <div id="following_tab" class=" row jsClassProfielTab d-none">
      <!-- include('web.user.following') -->
    </div>
    <div id="interest_tab" class=" row jsClassProfielTab d-none">
      @include('web.user.edit_interest_categories')
    </div>
    <div id="notes_tab" class=" row jsClassProfielTab d-none">
    </div>
</div>


<script>

$("body").on('keyup','#filter',function(){

    // Retrieve the input field text and reset the count to zero
    var filter = $(this).val().trim(), count = 0;
    if(!filter){ // hide is no text
        //$(".cat-checkbox-wrapper li").hide();
        //return;
    }

    var regex = new RegExp(filter, "i"); // Create a regex variable outside the loop statement

    // Loop through the comment list
    $(".cat-checkbox-wrapper li").each(function(){
          // If the list item does not contain the text phrase fade it out
          if ($(this).text().search(regex) < 0) { // use the variable here
              $(this).hide();

          // Show the list item if the phrase matches and increase the count by 1
          } else {
              $(this).show();
              count++;
          }
    });
});

$(document).ready(function(){
  @if(Auth::user())
    @if(Auth::user()->id == $oUser->id)
      switchProfileTab('edit_profile');
    @else
      switchProfileTab('notes_tab');
    @endif
  @else
    switchProfileTab('notes_tab');
  @endif
});

$(".edit-picture").click(function(){
  $( "#browse_file" ).trigger( "click" );
});
// ===== Switch Profile Tab show / Hide =====
function switchProfileTab(eleId){
  $(".jsClassProfielTab").addClass('d-none');
  $("#"+eleId).removeClass('d-none');

  if(eleId != 'edit_profile')
  {
    @if(Auth::user())
      $('#change_password_form')[0].reset();
    @endif

    callGetRelatedData(eleId);
  }
}

function callGetRelatedData(eleId)
{
  $.ajax({
        method : "GET",
        url: "{{ route('web.user.get-followers') }}",
        data : {
          type : eleId,
          userid : "{{ $oUser->id }}"
        },
        cache: false,
        success: function(response){
            if(response.status == 1)
            {
              $('#'+eleId).html(response.sHtml);
            }
        },error : function(error){ 
          console.log('Followers list');
        }
    });
}

@if(Auth::user())
 //=====  Pic Thumbnail Preview and Remove Functonality  Used in Add note & Profile Page START =====
/* document.getElementById("browse_file").onchange = function () {
        console.log("onChange Call");
        var reader = new FileReader();

        reader.onload = function (e) {
            // get loaded data and render thumbnail.
            document.getElementById("preview_image").src = e.target.result;
        };

        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
        $(".jsClassPlaceHolderImg").removeClass("d-none");
        $(".jsClassPlaceHolderImg").addClass("d-none");
        $(".jsClassRemoveImg").removeClass("d-none");
        $("#preview_image").removeClass("d-none");


    };
    function removeAttachedFile(){
        $("#preview_image").removeAttr('src');
        $("#preview_image").addClass('d-none');
        $(".jsClassPlaceHolderImg").removeClass("d-none");
        $(".jsClassRemoveImg").addClass("d-none");
        $("#browse_file").attr('val','');
    }*/
    //=====  Pic Thumbnail Preview and Remove Functonality  Used in Add note & Profile Page END =====
@endif

$("body").on("click",".n-js-follow",function(){
      var auth = "{{ \Auth::user() ? 1 : 0}}";
      if(auth == 0)
      {
          showToast("Please sign in to follow!");
          return false;
      }

      var user_id = $(this).attr('data-id');
      var action = $(this).attr('data-action');

      $.ajax({
          method : "GET",
          url: "{{ route('web.user.follow') }}",
          data : { 
              user_id : user_id,
              action : action
          },
          cache: false,
          success: function(response){
              if(response.status == 1)
              {
                  showToast(response.message);
                  setTimeout(() => {
                      window.location.reload();
                  }, 1000);
              }
          },error : function(error){ console.log('Error');}
      });

  });//End of click


  $('body').on('click','.js-cat',function () {
    var id = $(this).attr('data-id');
    if ($(this).attr('checked')) {
        var status = "remove";
    } else {
        var status = "add";
    }

    $.ajax({
        method : "GET",
        url: "{{ route('web.user.update-area-of-interest') }}",
        data : {
          id : id,
          status : status
        },
        cache: false,
        success: function(response){
          console.log('Interest success');
        },error : function(error){ 
          console.log('Interest error');
        }
    });

  });

</script>
@endsection