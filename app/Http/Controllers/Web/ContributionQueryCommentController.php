<?php

namespace App\Http\Controllers\Web;

use App\ContributionQueryComment;
use App\Models\ContributionQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class ContributionQueryCommentController extends WebController
{
    public function add(Request $request){
        if (filter_var($request->userInput, FILTER_VALIDATE_URL) === FALSE) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "Note suggestion added."
            ]);
        }else{
            $url = parse_url($request->userInput);
            if(in_array($url['host'], $this->allowedDomain())){
                $contributionQueryComment = ContributionQueryComment::create([
                    'contribution_query_id' =>  $request->id,
                    'user_id'   =>  Auth::user()->id,
                    'comment'   =>  $request->userInput
                ]);
                return response()->json([
                    'status'    =>  1,
                    'message'   =>  "Note suggestion added."
                ]);
            }

            return response()->json([
                'status'    =>  0,
                'message'   =>  "Only Notechit URL are allowed."
            ]);
        }
    }

    public function allowedDomain(){
        if(App::environment() == 'local'){
            return [
                '127.0.0.1',
                '127.0.0.1:8000',
                'localhost'
            ];
        }else{
            return [
                'notechit.com',
                'www.notechit.com'
            ];
        }
    }

    public function delete(Request $request){
        $comment = ContributionQueryComment::find($request->id);
        if($comment->user_id == Auth::user()->id){
            $comment->delete();

            return response()->json([
                'status'    =>  1,
                'message'   =>  "Comment Deleted Successfully."
            ]);
        }else{
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Unauthorized Access"
            ]);
        }
    }

    public function getCommentData(Request $request){
        $oQuery = ContributionQuery::with(['comments'])->find($request->id);
        if($oQuery){
            return response()->json([
                'status'    =>  1,
                'content'   =>  view('web.user.contribution_note_suggesttion_comments')->with('oQuery', $oQuery)->render()
            ]);
        }else{
            return response()->json('Data Not Found', 404);
        }
    }
}
