<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" href="{{route('web.home.about-us')}}">About Us <span class="sr-only">(current)</span></a>
        </li>

    @if(!\Auth::user())
    
    <li class="nav-item">
        <a class="btn btn-sm badge-pill px-4 nc-login-btn" href="#" data-toggle="modal" data-target="#loginModal">Login</a>
    </li>
    
    @else
        <li class="ml-5 mr-3 dropdown show js-show-notifications">
            <a href="#" class="nav-link" role="button" id="notificationLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="far fa-bell"></i>
            </a>
            <div class="dropdown-menu notification-dd" aria-labelledby="notificationLink">
                @include('web.notification_list')
            </div>

        </li>
        <li class="dropdown show">
            <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="user-thumbnail-sm">
                    @if(\Auth::user()->file_name || \Auth::user()->profile_pic_url)
                    <img class="w-100" src="{{ getUserImageUrl(\Auth::user()->file_name,\Auth::user()->profile_pic_url) }}" alt="">
                    @else
                    <span class="user-initials">{{ mb_substr(\Auth::user()->first_name, 0, 1) . mb_substr(\Auth::user()->last_name, 0, 1) }}</span>
                    @endif
                </span>
                <!-- {{ \Auth::user()->first_name ." ". \Auth::user()->last_name }} -->
            </a>

            <div class="dropdown-menu nc-header-dd" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="{{ route('web.user.profile',['id' => Auth::user()->id]) }}">My profile</a>
                <!-- <a class="dropdown-item" href="">Help</a> -->
                <a class="dropdown-item" href="{{ route('web.user.logout') }}">Logout</a>
            </div>
        </li>

   
    @endif

    </ul>
</div>

<script>
$(document).ready(function(){
    _getNotifications();
});
$("body").on("click",".js-show-notifications",function(){
    _getNotifications(true);
});

function _getNotifications(read = false){
    $.ajax({
        method : "GET",
        url: "{{ route('web.user.get-notifications') }}",
        data : {
            read:read,
        },
        cache: false,
        success: function(response){
            $('.notification-list-wrapper').html(response.sHtml);
            if(response.has_new_notification == "1"){
                $('#notificationLink').children('i').addClass('notification-bell-circle');
            }
        },error : function(error){ 
          console.log('get-notifications error');
        }
    });
    return false;
}

$("body").on("click",".notification-btn",function(){
    var id = $(this).attr('data-id');
    var notification_id = $(this).attr('data-notification-id');
    var action = $(this).attr('data-action');
    var userName = $(this).data('user-name');
    _updateNotificationStatus(id,notification_id,action, userName);
});

function _updateNotificationStatus(id,notification_id,action, userName){
    $.ajax({
        method : "POST",
        url: "{{ route('web.user.update-notification-status') }}",
        data : {
            id : id,
            notification_id : notification_id,
            action : action,
            _token : "{{ csrf_token() }}"
        },
        cache: false,
        success: function(response){
            if(action == "accept"){
                showToast(userName+" is now following you");
            }else{
                showToast("status successfully updated");
            }
            setTimeout(() => {
                window.location.reload();
            }, 1000);
        },error : function(error){ 
          console.log('get-notifications error');
        }
    });

    return false;
}

</script>