<?php

namespace App;

use App\Models\ContributionQuery;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class ContributionQueryComment extends Model
{
    protected $fillable = [
        'contribution_query_id',
        'user_id',
        'comment'
    ];

    public function contribution_query(){
        return $this->belongsTo(ContributionQuery::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
