@extends('web.custom_layout')

@section('title')
    {{ trans('messages.home_page') }}
@endsection

@section('header')
    @include('web.primary_header')
    @include('web.login_popup')

@endsection

@section('content')
  <div class="container-fluid aboutus-wrapper">

    <div class="row">
        <div class="col py-5 text-center">
          <img src="{{ asset('web/img/about_top_img.svg') }}" alt="box-image" data-aos="fade-down"/>
          <div class="about-text-wrapper">
            <div class="title-text" data-aos="fade-down">
              All Your Notes At One Place
            </div>
            <div class="sub-text" data-aos="fade-down">
              Create your own notes, save other’s note, keep them all at one place
            </div>
          </div>
        </div>
    </div>

    <div class="row bordered-section" >

      <div class="col-md-6 col-sm-12 right-side-border p-5">
        <div class="wrapper px-5">
            <div class="img-wrapper">
              <img src="{{ asset('web/img/about_searching.svg') }}" alt="search-image"/>
            </div>
            <div class="about-text-wrapper">
              <div class="sub-title-text">
                Easy Searching
              </div>
              <div class="sub-text pr-5">
                Make your notes searchable, let more people reach to your notes. Also, search other notes easily
              </div>
            </div>
        </div>
       
      </div>

      <div class="col-md-6 col-sm-12 right-side-border p-5">
        <div class="wrapper px-5">
            <div class="img-wrapper">
              <img src="{{ asset('web/img/about_download.svg') }}" alt="download-image"/>
            </div>
            <div class="about-text-wrapper">
              <div class="sub-title-text">
                Easy Downloading
              </div>
              <div class="sub-text pr-5">
              Download any notes as a zip file bundle, and some other text also goes here
              </div>
            </div>
        </div>
       
      </div>
      
    </div>

     <div class="row " data-aos="fade-up">

      <div class="col-md-6 col-sm-12 right-side-border p-5">
        <div class=" about-text-wrapper wrapper px-5">
            <div class="d-inline-block">
              <img src="{{ asset('web/img/about_ask.svg') }}" alt="ask-image"/>
            </div>
            <div class="sub-title-text d-inline-block align-bottom">
              Ask for Help
            </div>
           
              <div class="sub-text pr-5 my-4">
              Ask Globally for Note that you are looking for 
              </div>
           
        </div>
       
      </div>

      <div class="col-md-6 col-sm-12 right-side-border p-5">
        <div class=" about-text-wrapper wrapper px-5">
            <div class="d-inline-block">
              <img src="{{ asset('web/img/about_help.svg') }}" alt="ask-image"/>
            </div>
            <div class="sub-title-text d-inline-block align-bottom ml-2">
              Help Others
            </div>
            <div class="sub-text pr-5 my-4">
              Help others  by adding needed notes
            </div>
           
        </div>
       
      </div>

     
      
    </div>


    <div class="row bordered-section" data-aos="fade-up">
      
      <div class="col-12 text-center py-5">
          <div class="about-text-wrapper">
              <div class="sub-title-text">
                  Decide Note Visibility
              </div>
              <div class="sub-text pr-5">
                Make your notes searchable, let more people reach to your notes. Also, search other notes easily
              </div>
          </div>
      </div>


      <div class="container pb-5 about-form">
        <div class="row">
          <div class="col-12">
              <div class="form-group form-control">
                  <div class="row">

                    <div class="col-sm-12 col-md-4">
                      <div class="form-check note-visibility-radio-btn">
                        <input class="form-check-input" type="radio" name="note_type" id="exampleRadios1" value="" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            <b> @lang('messages.public_note') </b><br>
                            @lang('messages.public_note_desc') 
                        </label>
                      </div>
                    </div>

                    <div class="col-sm-12 col-md-4">
                      <div class="form-check note-visibility-radio-btn">
                        <input class="form-check-input" type="radio" name="note_type" id="exampleRadios2" value="">
                        <label class="form-check-label" for="exampleRadios2">
                            <b> @lang('messages.share_with_followers_only') </b><br>
                            @lang('messages.share_with_followers_only_desc') 
                        </label>
                      </div>
                    </div>

                    <div class="col-sm-12 col-md-4">
                      <div class="form-check note-visibility-radio-btn">
                        <input class="form-check-input" type="radio" name="note_type" id="exampleRadios3" value="">
                        <label class="form-check-label" for="exampleRadios3">
                            <b> @lang('messages.only_me') </b><br>
                            @lang('messages.only_me_desc') 
                        </label>
                      </div>
                    </div>

                  </div>

              </div>
          </div>

        </div>
      </div>

    </div>

    <div class="row" data-aos="fade-up">
        <div class="col py-5 text-center">
          <img src="{{ asset('web/img/about_folder.svg') }}" alt="folder-image" class="my-3"/>
          <div class="about-text-wrapper">
            <div class="sub-title-text">
            Manage Your Notes with Buckets
            </div>
            <div class="sub-text">
              Create your own notes, save other’s note, keep them all at one place
            </div>
            <a href="#" data-toggle="modal" data-target="#loginModal" class="start-btn">Start Using Notechit</a>
          </div>
        </div>
    </div>


    <div class="row bordered-section" data-aos="fade-up">
      <div class="col text-center py-5">
          <div class="about-text-wrapper">
            <div class="sub-title-text">
              We would love to hear from you ..!
            </div>
            <div class="sub-text">
              For any Queries, Suggestions, and Feedback, Feel free to get in touch
            </div>
            <a href="{{route('web.home.contact-us')}}" class="connect-link">Connect With Us</a>
          </div>
      </div>
    </div>





  </div>

  

<script>
  AOS.init({
    duration: 1200,
 }); 
</script>

  
@endsection