@extends('web.main')

@section('title')
    {{ trans('messages.home_page') }}
@endsection

@section('content')

<div class="container notes-wrapper">
    @include('web.user._contribution_nav_tab')

@guest
<!-- ===== message for not logged in user Start===== -->
<div class="row ">
    <div class="col no-data-wrapper py-5 ">
    <div class="info-text font-weight-bold  py-1">Post a new query</div>
    <a href="#" data-toggle="modal" data-target="#loginModal" class="cta-btn">Login to Notechit</a>
    </div>
</div>
<!-- ===== message for not logged in user End===== -->
@endguest

@auth
<form name="new-query-post" id="new-query-post" method="post" action="{{ route('web.home.add-query') }}">
    @csrf
    <div class="row">
        <div class="col-12">
            @if(Session::has('message'))
                <p class="alert alert-info">{{ Session::get('message') }}</p>
            @elseif(Session::has('error'))
                <p class="alert alert-danger">{{ Session::get('error') }}</p>
            @endif

            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="alert alert-warning nc-sm-text" role="alert">
                <i class="nc-xs-text fas fa-eye"></i>
                <span class="d-inline-block align-top">
                    <b> Who will see this request? </b>
                    <div class="">People interested in the tagged categories here will see this request.</div>
                </span>
                <br>
                <br>

                <i class="nc-xs-text fas fa-hourglass-half"></i>
                <span class="d-inline-block align-top">
                    <b>    Your request will last upto 15 days.</b>
                    <div class="">Don't worry ..! If you did not get the required note, You can post them again.</div>
                </span>
            </div>
        </div>
        <div class="col-12 form-group">
            <label for="exampleInputEmail1"> @lang('messages.note_category_tags'): </label>
            <select class="js-example-basic-multiple form-control" id="categories" name="categories[]" multiple="multiple">
                @foreach($oCategories as $key => $category)
                    <option value="{{ $category->id }}" data-image="{{getCategoryImageUrl($category->image)}}">{{ $category->category_name }}</option>
                <!-- <option value="{{ $key }}">{{ $category }}</option> -->
                @endforeach
            </select>
        </div>
        <div class="col-12">
            <div class="form-group nc-editor-wrapper">
                <label for="exampleInputEmail1"> @lang('messages.required_note_description') :</label>
                <textarea name="query_text" id="query_text"  cols="30" rows="10" placeholder="Enter description here" class="form-control"></textarea>
            </div>
        </div>
        <input type="submit" id="" class="nc-black-btn nc-save-btn my-3 mx-auto" value="Post">
    </div>
</from>

@endauth

</div>

<script>

    $(document).ready(function() {
      //select2 plugin initialization for category autot suggestion/ multiple selection
        $('.js-example-basic-multiple').select2({
            placeholder: "Select a category",
            allowClear: true,
            templateResult: formatState,
            templateSelection: formatState
        });
    })
    function formatState (opt) {
        var optimage = $(opt.element).attr('data-image'); 
        if(!optimage){
            return opt.text.toUpperCase();
        } else {                    
            var $opt = $('<span><img src="' + optimage + '" width="30px" height="20px" /> ' + opt.text.toUpperCase() + '</span>');
            return $opt;
        }
    };

</script>


@endsection
