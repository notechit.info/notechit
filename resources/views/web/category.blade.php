@extends('web.custom_layout')

@section('title')
    {{ trans('messages.home_page') }}
@endsection

@section('header')
    @include('web.primary_header')
    @include('web.login_popup')

@endsection

@section('content')
  @include('web.secondary_header')

  <div class="container pb-5" >
      <div id="explore_notes" class="row scrolling-pagination">
        @php
          $i = 1;
        @endphp
        @foreach($oNotes as $oNote)
          <div class="col-lg-4 col-md-6 col-sm-12 single-note-card note-card-bg-{{$i}}">
              @php
              if($i > 11){
                  $i=1;
              }
              else{
                  $i++;
              }
              @endphp
              @include('web.notes.single_note_card')
          </div>
        @endforeach
    </div>
    <div class="offset-md-5 clearfix">
        {{ $oNotes->withQueryString()->links() }}
    </div>
  </div>
  <script src="https://unpkg.com/infinite-scroll@4/dist/infinite-scroll.pkgd.min.js"></script>
  <script>

    $('ul.pagination').hide();

    $(function() {
        $('.scrolling-pagination').infiniteScroll({
            path: '.pagination li.active + li a',
            append: '.single-note-card',
            history: false,
        });
    });

    window.onscroll = function() {myFunction()};

    var navbar = document.getElementById("secondary_navbar");
    var sticky = navbar.offsetTop;

    function myFunction() {
      if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
      } else {
        navbar.classList.remove("sticky");
      }
    }

    $(document).ready(function(){
        $("a").on('click', function(event) {

          if (this.hash !== "") {
            event.preventDefault();

            var hash = this.hash;

            $('html, body').animate({
              scrollTop: $(hash).offset().top
            }, 800, function(){

              window.location.hash = hash;
            });
          }
        });
      });
    $('#showIntrestButton').click(function(){
      $('#interestModal').modal('show');
    });
  </script>
@if(Session::has('ask_login') && Session::get('ask_login'))
    <script>
        $('#loginModal').modal('show');    
    </script>
@endif
@endsection