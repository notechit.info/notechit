function showAjaxLoader(parentEle) {
    $(parentEle).css('position', 'relative');
    var html = '<div class="loader-wrapper"><div class="loader"></div></div>';
    $(parentEle).prepend(html);

    // setTimeout(function()
    // {
    //     $(parentEle).find('.loader-wrapper').remove();
    // }, 3000);
}

function hideAjaxLoader() {
    $('.loader-wrapper').remove();
}

function scrollToTop() {
    window.scrollTo(0, 0);
}

// Delete listing data.
$(document).on('click', '.data-delete', function(e) {

    e.preventDefault();
    id = $(this).data('id'),
        href = $(this).attr('href');

    $('#confirm').modal({
            backdrop: 'static',
            keyboard: false
        })
        .one('click', '#delete', function(e) {
            window.location.href = href;
        });
});