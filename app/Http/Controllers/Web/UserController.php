<?php

namespace App\Http\Controllers\Web;

use App\Models\Note;
use App\Models\Rating;
use App\Models\User;
use App\Models\Follow;
use App\Models\UserCategory;
use App\Models\Category;
use App\Models\FollowRequest;
use App\Models\Notification;
use App\Models\Bucket;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\AWS\AwsS3;
use Carbon\Carbon;
use App\Mail\ActivationEmail;
use App\Mail\ResetPasswordEmail;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Web\WebController;

class UserController extends WebController
{
    private function _createUser($oRequest)
    {
        $oUser = User::create([
            'first_name'    => $oRequest->get('first_name'),
            'last_name'     => $oRequest->get('last_name'),
            'email'         => $oRequest->get('email'),
            'password'      => bcrypt($oRequest->get('password')),
            'verified'      => User::UNVERIFIED
        ]);

        return $oUser;
    }

    public function getNotifications(Request $request)
    {
        $isNewNotification = 0;
        $oNotifications = Notification::with(['youUser'])->where('user_id',\Auth::user()->id)->orderBy('id','DESC')->get();

        foreach($oNotifications as $key=>$value){
            if($value->is_read == "0"){
                $isNewNotification = 1;
            }
        }

        if($isNewNotification && $request->get('read') == "true"){
            Notification::where('user_id',\Auth::user()->id)->where('is_read','0')->update(['is_read'=>'1']);
        }

        $sHtml = view("web._notification_list",[
            'oNotifications' => $oNotifications
        ])->render();

        return $this->prepareJsonResponse([
            'status'    => 1,
            'sHtml'     => $sHtml,
            'has_new_notification'=> $isNewNotification
        ]);
    }

    public function updateNotificationStatus(Request $oRequest)
    {
        if($oNotification = FollowRequest::find($oRequest->get('id')))
        {
            $oNotification->ff_status = $oRequest->get('action') == 'accept' ? FollowRequest::ACCEPT : FollowRequest::DECLINE;
            $oNotification->save();

            if($oNewNotification = Notification::find($oRequest->get('notification_id')))
            {
                $oNewNotification->attempt = 1;
                $oNewNotification->entity_id = NULL;
                $oNewNotification->save();
            }
           
            if ($oRequest->get('action') == 'accept') {
                $oFav = Follow::firstOrNew([
                    'follower_user_id' => \Auth::user()->id,
                    'following_user_id' => $oNotification->from_user_id
                ]);
                
                $oFav->activated    = 1;
                $oFav->deleted      = 0;
                $oFav->save();

                /**
                 * Create notification entry
                 */
                Notification::create([
                    'user_id'       => $oNotification->from_user_id,
                    'from_user_id'  => \Auth::user()->id,
                    'type'          => Notification::FF,
                    'action'        => Notification::FOLLOWING,
                    'activated'     => 1,
                    'deleted'       => 0
                ]);
            }
            else{
                /**
                 * Create notification entry
                 */
                Notification::create([
                    'user_id'       => $oNotification->from_user_id,
                    'from_user_id'  => \Auth::user()->id,
                    'type'          => Notification::FF,
                    'action'        => Notification::REJECT,
                    'activated'     => 1,
                    'deleted'       => 0
                ]);
            }

            return $this->prepareJsonResponse([
                'status'    => 1,
                'message'   => 'Successfully updated!'
            ]);
        }

        return $this->prepareJsonResponse([
            'status'    => 0,
            'message'   => 'Something went wrong!'
        ]);
    }

    /**
     * Verify user mail - email from signup
     */
    public function EmailVerificationAction($key = '')
    {
        if(empty($key))
        {
            return back()->with(['error' => trans('messages.unauthorized_request') ]);  
        }

        if(!$user = User::where('unique_key',$key)->where('verified',User::UNVERIFIED)->first())
        {
            return back()->with(['error' => trans('messages.unauthorized_request') ]);
        }

        $user->unique_key = "verified";
        $user->verified = User::VERIFIED;
        $user->email_verified_at = Carbon::now()->toDateTimeString();
        $user->save();

        return redirect(route('web.home.index'))->with(['success' => trans('messages.account_activate_success')]);
    }


    public function SignUpAction(Request $oRequest){
        $messages = [
            'email.unique' => 'Email already exists',
            'conf_password.same' => 'Password and confirm password not matching'
        ];

        $aValidationRequiredFor = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6',
            'conf_password' => 'required|min:3|same:password'
        ];  

        $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor, $messages);
        if($oValidator->fails())
        {
            return $this->prepareJsonResponse([
                'status'        => 0,
                'errors'        => $oValidator->errors()
            ]);
        }

        $sEmail = $oRequest->get('email');
        $oCreatedUser = $this->_createUser($oRequest);

        Bucket::defaultBucket($oCreatedUser->id);

        /**
         * Send verification Email to user
         */
        try
        {
            $key = $this->RandomString(100).date('dymhis').rand(0000,9999);
            $oCreatedUser->unique_key = $key;
            $oCreatedUser->save();
            $link = env('APP_URL')."/verification/".$key;
            \Mail::to($sEmail)->send(new ActivationEmail($link));
        }
        catch (\Exception $e) {
            \Log::error("Error while sending verification mail");
            \Log::error($e);
        }
        return $this->prepareJsonResponse([
            'status'  => 1,
            'message' => trans('messages.signup_success')
        ]);

    }
    
    /**
     * LOGIN ACTION
     */
    public function SignInAction(Request $oRequest){

        $aValidationRequiredFor = [
            'signin_email' => 'required|email',
            'signin_password' => 'required',
        ];  

        $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
        if($oValidator->fails())
        {
            return $this->prepareJsonResponse([
                'status'        => 0,
                'errors'        => $oValidator->errors()
            ]);
        }

        if($oUser = User::where('email', $oRequest->get('signin_email'))->first())
        {
            if($oUser->verified == User::VERIFIED && $oUser->activated == User::YES) {
                
                if(Hash::check($oRequest->get('signin_password'), $oUser->password) && $oUser->user_type == User::NORMAL_USER) {
                    \Auth::login($oUser);

                    $nLoginCount = $oUser->login_count > 0 ? $oUser->login_count + 1 : 1;
                    $oUser->login_count = $nLoginCount;
                    $oUser->save();

                    return $this->prepareJsonResponse([
                        'message'       => trans('messages.login_success'),
                        'status'        => 1,
                        'login_count'   => $nLoginCount
                    ]);

                }
                
                return $this->prepareJsonResponse([
                    'message'       => trans('messages.invalid_credential'),
                    'status'        => 3
                ]);
            }
            else if($oUser->verified == User::UNVERIFIED) 
            {
                return $this->prepareJsonResponse([
                    'status'   => 2,
                    'message'       => trans('messages.account_verify_error'),
                ]);
            }
        }

        return $this->prepareJsonResponse([
            'message'       => trans('messages.invalid_credential'),
            'status'        => 3
        ]);
    }

    /**
     * Forgot password
     */
    public function ForgotPasswordAction(Request $oRequest)
    {
        $aValidationRequiredFor = [
            'fo_email' => 'required|email',
        ];  

        $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
        if($oValidator->fails())
        {
            return $this->prepareJsonResponse([
                'status'        => 0,
                'errors'        => $oValidator->errors()
            ]);
        }

        $sEmail = $oRequest->get('fo_email');
        if(!$oUser = User::where('email',$sEmail)->first())
        {
            return $this->prepareJsonResponse([
                'status'   => 0,
                'errors'  => array('fo_email' => ['Please enter your registered email']),
            ]);
        }

        if($oUser->verified == User::UNVERIFIED)
        {
            return $this->prepareJsonResponse([
                'status'   => 0,
                'errors'  => array('fo_email' => ['Please verify your account first!']),
            ]);
        }

        /**
         * Send verification Email to user
         */
        try
        {
            $key = $this->RandomString(100).date('dymhis').rand(0000,9999);
            $oUser->unique_key = $key;
            $oUser->save();
            $link = env('APP_URL')."/reset-password/".$key;
            \Mail::to($sEmail)->send(new ResetPasswordEmail($link));
        }
        catch (\Exception $e) {}

        return $this->prepareJsonResponse([
            'status'   => 1,
            'message'  => 'Reset password link successfully sent to your email address.',
        ]);
    }

    /**
     * Edit profile
     */
    public function userProfileAction(Request $oRequest,$nUserId){
        if ($oRequest->isMethod("POST")) {

            $aValidationRequiredFor = [
                'edit_first_name' => 'required',
                'edit_last_name' => 'required',
            ];  
    
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                return $this->prepareJsonResponse([
                    'status'        => 0,
                    'errors'        => $oValidator->errors()
                ]);
            }
            
            $oUser = \Auth::user();

            $oUser->first_name = $oRequest->get('edit_first_name');
            $oUser->last_name = $oRequest->get('edit_last_name');
            $oUser->about_me = $oRequest->get('edit_about_me');
            $oUser->save();

            return $this->prepareJsonResponse([
                'status'   => 1,
                'message'  => 'Profile successfully updated',
            ]);
        }

        if(!$oUser = User::find($nUserId))
            abort(404);

        // $nFollowersCount = Follow::where('follower_user_id',\Auth::user()->id)->active()->count();
        $nFollowersCount = Follow::where('follower_user_id',$nUserId)->active()->count();
        // $nFollowingCount = Follow::where('following_user_id',\Auth::user()->id)->active()->count();
        $nFollowingCount = Follow::where('following_user_id',$nUserId)->active()->count();
        $oMyNotesTitle = Note::select('title')->where('user_id',$nUserId)->active()->get();

        $isFollwed = false;
        if (\Auth::user()) {
            if ($isFollwed = Follow::where('follower_user_id', $nUserId)->where('following_user_id', \Auth::user()->id)->active()->first()) {
                $isFollwed = true;
            }
        }

        $isFollwRequested = false;
        if(\Auth::user() && $isFollwed == false){
            $followRequestData = FollowRequest::where([
                    'user_id'       =>      \Auth::user()->id,
                    'to_user_id'    =>      $nUserId,
                    'ff_status'     =>      FollowRequest::PENDING,
                ])
                ->latest()
                ->first();
            $isFollwRequested = $followRequestData ? true : false;
        }

        return view("web.user.profile", compact('oMyNotesTitle', 'nFollowersCount', 'nFollowingCount', 'oUser', 'isFollwed', 'isFollwRequested'));
    }    
    
    /**
     * Change password Action
     */
    public function changePasswordAction(Request $oRequest){
        if ($oRequest->isMethod("POST")) {

            $aValidationRequiredFor = [
                'current_password'  => 'required',
                'new_password'      => 'required',
            ];  
    
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                return $this->prepareJsonResponse([
                    'status'        => 0,
                    'errors'        => $oValidator->errors()
                ]);
            }
            
            $oUser = \Auth::user();

            if (Hash::check($oRequest->get('current_password'), $oUser->password)) {
                $oUser->password = bcrypt($oRequest->get('new_password'));
                $oUser->save();
            }
            else
            {
                return $this->prepareJsonResponse([
                    'status'        => 0,
                    'errors'        => array('current_password' => array('Invalid current password'))
                ]);
            }

            return $this->prepareJsonResponse([
                'status'   => 1,
                'message'  => 'Password successfully changed',
            ]);
        }

        return $this->prepareJsonResponse([
            'status'   => -1,
            'message'  => 'Something went wrong',
        ]);
    }

    /**
     * Upload profile picture Action
     */
    public function uploadProfilePictureAction(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $oUser = \Auth::user();

            $aValidationRequiredFor = [
                'file' => 'sometimes|image',
            ];

            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                return $this->prepareJsonResponse([
                    'status'        => 0,
                    'errors'        => $oValidator->errors(),
                ]);
            }

            $sFileName = $oUser->file_name;
            if($oRequest->file('file')) {
                $sFolder = config('constants.USERMEDIAFOLDER');
                $sFileName = AwsS3::s3UploadImage($oRequest->file('file'),$sFolder,$sFileName = '');
            }

            $oUser->file_name = $sFileName;
            $oUser->save();

            return redirect()->back();
            // return $this->prepareJsonResponse([
            //     'status'          => 1
            // ]);
        }

        return $this->prepareJsonResponse([
            'status'        => 0
        ]);
    }

    public function LogoutAction(Request $oRequest)
    {
        \Auth::logout();
        $oRequest->session()->flush();
        $oRequest->session()->regenerate();
        return redirect(route('web.home.index'));
        //return redirect()->back();
    }

    /**
     * Reset password from forgot password email
     */
    public function ResetPasswordAction($key = '',Request $oRequest){
        
        if ($oRequest->isMethod("POST")) {

            $this->validate($oRequest, [
                'password' => 'required',
                'confirm_password' => 'required|same:password',
            ], [
                'confirm_password.required' => 'The confirm password field is required.',
                'confirm_password.same' => 'Confirm password not same as password.',
            ]);

            if (empty($key)) {
                return back()->with(['error' => 'Unauthorized request! Please try again.']);
            }

            if (!$user = User::where('unique_key', $key)->first()) {
                return back()->with(['error' => 'Unauthorized request! Please try again.']);
            }

            $user->password = bcrypt($oRequest->get('password'));
            $user->unique_key = 'reset-password';
            $user->save();

            return redirect()->route('web.home.index')->with(['success' => 'Password successfully reset ow you can login']);
        }
        
        return view("web.reset_password" , ['key' => $key]);
    } 

    /**
     * User profile action for view user detail for follow
     */
    public function userProfileForFollowAction($id){
        $oUserDetail = User::find($id);
        $oMyNotesTitle = Note::select('title')->where('user_id',$id)->active()->get();

        $isFollwed = false;
        if (\Auth::user()) {
            if ($isFollwed = Follow::where('follower_user_id', $id)->where('following_user_id', \Auth::user()->id)->active()->first()) {
                $isFollwed = true;
            }
        }

        $nFollowersCount = Follow::where('follower_user_id',$id)->active()->count();
        $nFollowingCount = Follow::where('following_user_id',$id)->active()->count();

        return view("web.user.view_profile",[
            'oUserDetail' => $oUserDetail,
            'oMyNotesTitle' => $oMyNotesTitle,
            'isFollwed' => $isFollwed,
            'nFollowersCount' => $nFollowersCount,
            'nFollowingCount' => $nFollowingCount,
            'nMyNoteCount' => $oMyNotesTitle->count()
        ]);
    } 

    public function followAction(Request $oRequest){
        if($oRequest->action != "follow") // unfollow
        {
            if($oFollowRequest = FollowRequest::where([
                'user_id'       => \Auth::user()->id,
                'from_user_id'  => \Auth::user()->id,
                'to_user_id'    => $oRequest->user_id
                ])->first())
                {
                
                    if($oNewNotification = Notification::where('entity_id',$oFollowRequest->id)->first())
                    {
                    $oNewNotification->entity_id = NULL;
                    $oNewNotification->save();
                }
                $oFollowRequest->delete();
            }

            $oFav = Follow::firstOrNew([
                'follower_user_id' => $oRequest->user_id,
                'following_user_id' => \Auth::user()->id
            ]);
            $oFav->activated = 0;
            $oFav->deleted = 0;
            $oFav->save();

            /**
             * Create notification entry
             */
            Notification::create([
                'user_id'       => $oRequest->user_id,
                'from_user_id'  => \Auth::user()->id,
                'type'          => Notification::FF,
                'action'        => Notification::UNFOLLOW,
                'activated'     => 1,
                'deleted'       => 0
            ]);

            return $this->prepareJsonResponse([
                'status'   => 1,
                'message'  => 'Updated!'
            ]);
        }
        else
        {
            $oFollowRequest = FollowRequest::firstOrNew([
                'user_id'       => \Auth::user()->id,
                'from_user_id'  => \Auth::user()->id,
                'to_user_id'    => $oRequest->user_id
            ]);
            
            $oFollowRequest->type        = FollowRequest::FF;
            $oFollowRequest->ff_status   = FollowRequest::PENDING;
            $oFollowRequest->activated   = 1;
            $oFollowRequest->deleted     = 0;
            $oFollowRequest->save();
            
            $oNot = Notification::firstOrNew([
                'user_id'       => $oRequest->user_id,
                'from_user_id'  => \Auth::user()->id,
                'type'          => Notification::FF,
                'action'        => Notification::FOLLOW,
                'entity_id'     => $oFollowRequest->id,
                'attempt'       => 0,
                'activated'     => 1,
                'deleted'       => 0
            ]);
            $oNot->save();

            return $this->prepareJsonResponse([
                'status'   => 1,
                'message'  => 'Request successfully sent'
            ]);
        }
    }

    /**
     * Add user area of interest
     */
    public function userAddAreaOfInterestAction(Request $oRequest){

        $aValidationRequiredFor = [
            'aCategories' => 'required|array'
        ];

        $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
        if($oValidator->fails())
        {
            return $this->prepareJsonResponse([
                'status'        => 0,
                'errors'        => $oValidator->errors(),
            ]);
        }

        $aCategories = $oRequest->get('aCategories');

        foreach ($aCategories as $category) {
            $oCat = UserCategory::firstOrNew([
                'category_id'   => $category,
                'user_id'       => \Auth::user()->id
            ]);

            $oCat->activated = 1;
            $oCat->deleted = 0;
            $oCat->save();
        }

        return $this->prepareJsonResponse([
            'status'   => 1,
            'message'  => trans('messages.user_add_area_of_interest_success')
        ]);
    }

    /**
     * Get followers list
     */
    public function getFollowersAction(Request $oRequest){
        
        $sFrom = '';
        $oNotes = array();
        $oResponse = array();
        $oCategories = array();
        $nStep = 1;
        if($oRequest->get('type') == 'following_tab')
        {
            $oResponse = Follow::getFollowingUserList($oRequest->get('userid'));
            $sFrom = "Following";
        }
        else if($oRequest->get('type') == 'followers_tab')
        {
            $oResponse = Follow::getFollowersUserList($oRequest->get('userid'));
            $sFrom = "Followers";
        }
        else if($oRequest->get('type') == 'notes_tab')
        {
            $nStep = 3;
            $oNotes = Note::getMyNotes($oRequest->get('userid'),false,true);
            $nAvgRating = 0;
            foreach($oNotes as $key => $oNote)
            {
                $nAvgRating = 0;
                $nTotalStars = Rating::where('note_id',$oNote->note_id)->active()->sum('star');
                $nTotalUsers = Rating::where('note_id',$oNote->note_id)->active()->count();
                if($nTotalStars > 0)
                {
                    $nAvgRating = round($nTotalStars / $nTotalUsers);
                }

                $oNotes[$key]['nAvgRating'] = $nAvgRating;
            }
        }
        else
        {
            $nStep = 2;
            // $oResponse = Follow::getFollowersUserList($oRequest->get('userid'));
            $oCategories = Category::active()->get();
            $sFrom = "Followers";
        }

        $sHtml = view("web.user._fusers",[
            'oNotes'        => $oNotes,
            'nStep'         => $nStep,
            'oFollowers'    => $oResponse,
            'sFrom'         => $sFrom,
            'oCategories'   => $oCategories
        ])->render();

        return $this->prepareJsonResponse([
            'status'   => 1,
            'sHtml' => $sHtml
        ]);
    } 


    /**
     * Update user area of interest
     */
    public function userUpdateAreaOfInterestAction(Request $oRequest){

        $aValidationRequiredFor = [
            'id' => 'required',
            'status' => 'required'
        ];

        $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
        if($oValidator->fails())
        {
            return $this->prepareJsonResponse([
                'status'        => 0,
                'errors'        => $oValidator->errors(),
            ]);
        }

        $oCat = UserCategory::firstOrNew([
            'category_id'   => $oRequest->id,
            'user_id'       => \Auth::user()->id
        ]);

        if($oRequest->status == 'add')
        {
            $oCat->activated = 1;
            $oCat->deleted = 0;
        }
        else
        {
            $oCat->activated = 0;
            $oCat->deleted = 1;
        }

        $oCat->save();

        return $this->prepareJsonResponse([
            'status'   => 1,
            'message'  => trans('messages.user_add_area_of_interest_success')
        ]);
    }
}


