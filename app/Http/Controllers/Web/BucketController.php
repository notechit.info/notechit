<?php

namespace App\Http\Controllers\Web;

use App\Models\Bucket;
use App\Models\BucketNotes;
use App\Models\Favourite;
use App\Models\Note;
use App\Models\Rating;
use Illuminate\Http\Request;
use App\Http\Controllers\Web\WebController;
use Illuminate\Support\Facades\Auth;
use Validator;

class BucketController extends WebController
{
    public function userBucket(Request $oRequest){
        $oBuckets = Bucket::with(['bucketNotes'])->where('user_id',\Auth::user()->id)->active()->orderBy('default_bucket','DESC')->orderBy('created_at', 'DESC')->get();
        return view("web.user.user_buckets",[
            'oBuckets' => $oBuckets
        ]);
    }
    
    public function getUserBucket(Request $oRequest){
        $oBuckets = Bucket::with(['bucketNotes'])->where('user_id',\Auth::user()->id)->active()->get();
        $sHtml = view("web.notes._bucket_folder",[
            'oBuckets' => $oBuckets,
        ])->render();

        return $this->prepareJsonResponse([
            'status'   => 1,
            'sHtml'  => $sHtml,
            'nNoteId' => $oRequest->nNoteId
        ]);
    }

    /**
     * create
     */
    public function createBucket(Request $oRequest){

        if ($oRequest->isMethod("POST")) {
            $aValidationRequiredFor = [
                'sBucketName' => 'required'
            ];  
    
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                return $this->prepareJsonResponse([
                    'status'        => 0,
                    'errors'        => $oValidator->errors()
                ]);
            }
            
            if($oRequest->has('id') && $oRequest->get('id') != 0)
            {
                if($oBucket = Bucket::find($oRequest->get('id')))
                {
                    $oBucket->bucket_name = $oRequest->get('sBucketName');
                    $oBucket->save();
                }else{
                    return $this->prepareJsonResponse([
                        'status'   => 3,
                        'message'  => 'No data found for Edit bucket'
                    ]);    
                }
            }
            else
            {
                $oBucket = Bucket::create([
                    'user_id' => \Auth::user()->id,
                    'bucket_name' => $oRequest->get('sBucketName')
                ]);
            }

            $bucketCount = Auth::user()->buckets()->active()->count();
            return $this->prepareJsonResponse([
                'status'   => 1,
                'message'  => 'Bucket successfully created',
                'data'     => [
                    'total' => $bucketCount,
                    'type'  => $oRequest->has('id') && $oRequest->get('id') != 0 ? 'Update' : 'Add',
                    'id'    => $oBucket->id,
                ],
            ]);
        }

        return $this->prepareJsonResponse([
            'status'   => 0,
            'message'  => 'Invalid request'
        ]);

    } 

    public function deleteBucket(Request $oRequest){
        if ($oRequest->isMethod("POST")) {

            $aValidationRequiredFor = [
                'nFolderId' => 'required|numeric'
            ];  
    
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                return $this->prepareJsonResponse([
                    'status'        => 0,
                    'errors'        => $oValidator->errors()
                ]);
            }
            
            if($oBucket = Bucket::with(['bucketNotes'])->find($oRequest->get('nFolderId')))
            {
                $oBucket->activated = 0;
                $oBucket->deleted   = 1;
                $oBucket->save();

                $oBucket->bucketNotes->map(function($oBucket){
                    $oBucket->delete();
                });

                return $this->prepareJsonResponse([
                    'status'   => 1,
                    'message'  => 'Bucket successfully deleted'
                ]);
            }

            return $this->prepareJsonResponse([
                'status'   => 2,
                'message'  => 'No data found for delete bucket'
            ]);
        }

        return $this->prepareJsonResponse([
            'status'   => 0,
            'message'  => 'Invalid request'
        ]);

    } 
    
    public function saveNoteToUserBucket(Request $oRequest){
        if($oRequest->action == 'simple')
        {
            $oFav = Favourite::firstOrNew([
                'note_id' => $oRequest->nodeID,
                'user_id' => \Auth::user()->id
            ]);
            
            $oFav->activated = 1;
            $oFav->deleted = 0;
            $oFav->save();

            return $this->prepareJsonResponse([
                'status'   => 1,
                'message' => trans('messages.note_simple_save_success')
            ]);
        }

        if($oBucketNotes = BucketNotes::where([
            'note_id'   => $oRequest->nodeID,
            'user_id'   => \Auth::user()->id,
            'deleted'   => 0
        ])->active()->first())
        {
            return $this->prepareJsonResponse([
                'status'   => 0,
                'message' => trans('messages.note_already_saved_error')
            ]);
        }

        $oBucketNotes = BucketNotes::firstOrNew([
            'note_id'   => $oRequest->nodeID,
            'bucket_id' => $oRequest->folderId,
            'user_id'   => \Auth::user()->id
        ]);
        
        $oBucketNotes->activated = 1;
        $oBucketNotes->deleted = 0;
        $oBucketNotes->save();

        return $this->prepareJsonResponse([
            'status'   => 1,
            'message' => trans('messages.note_folder_save_success')
        ]);
    }

    public function getBucketNotes(Request $oRequest){
        
        $oBucketNotes = BucketNotes::where('bucket_id',$oRequest->get('bucket_id'))->active()->get()->pluck('note_id')->toArray();
        $oNotes = Note::getBucketNotes($oBucketNotes);
        $nAvgRating = 0;
        foreach($oNotes as $key => $oNote)
        {
            $nAvgRating = 0;
            $nTotalStars = Rating::where('note_id',$oNote->note_id)->active()->sum('star');
            $nTotalUsers = Rating::where('note_id',$oNote->note_id)->active()->count();
            if($nTotalStars > 0)
            {
                $nAvgRating = round($nTotalStars / $nTotalUsers);
            }

            $oNotes[$key]['nAvgRating'] = $nAvgRating;
        }

        // $oBuckets = Bucket::with(['bucketNotes'])->where('user_id',\Auth::user()->id)->active()->orderBy('id','DESC')->get();
        
        $sHtml = view("web.user.bucket_notes",[
            'oNotes' => $oNotes,
            'bucketId' => $oRequest->get('bucket_id'),
        ])->render();

        return $this->prepareJsonResponse([
            'status'   => 1,
            'sHtml'  => $sHtml
        ]);
    }

    public function getUserBucketToMove(Request $request){
        $currentNoteBucket = BucketNotes::where([
                'note_id'       =>  $request->note,
                'user_id'       =>  \Auth::user()->id,
            ])
            ->latest()
            ->first();

        $allBuckets = Bucket::with(['bucketNotes'])->where('user_id',\Auth::user()->id)->active()->get();
        $sHtml = view("web.notes._move_note_bucket_folder",compact('allBuckets','currentNoteBucket'))->render();

        return $this->prepareJsonResponse([
            'status'   => 1,
            'sHtml'  => $sHtml,
            'note' => $request->note,
            'old_bucket' => $currentNoteBucket->bucket_id
        ]);
    }

    public function moveNoteBucket(Request $request){
        $oldtNoteBucket = BucketNotes::where([
                'note_id'   =>  $request->note,
                'user_id'   =>  \Auth::user()->id
            ])
            ->update([
                'activated' =>  0,
                'deleted'   =>  1
            ]);

        $bucketNote = BucketNotes::create([
                'note_id'   =>  $request->note,
                'bucket_id' =>  $request->bucket,
                'user_id'   =>  \Auth::user()->id,
            ]);
            
        return $this->prepareJsonResponse([
            'status'   => 1,
            'message'  => 'Note moved successfully',
        ]);                
    }
}