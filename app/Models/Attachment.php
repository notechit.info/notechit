<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $fillable = [
        'file_name',
        'display_file_name',
        'note_id',
        'activated',
        'deleted'
    ];
}
