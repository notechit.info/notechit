<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function showLogin(){
        return view('web.admin.login');
    }

    public function login(Request $request){
        if(Auth::guard('admin')->attempt($request->only('email','password'),$request->filled('remember'))){
            return redirect()
                ->intended(route('admin.home'))
                ->with('status','You are Logged in as Admin!');
        }

        Session::flash('loginError','Invalid credentials. Please try again.');
        return redirect()
            ->route('admin.show_login')
            ->with('status','You are Logged in as Admin!');
    }

    public function logout(Request $request){
        Auth::guard('admin')->logout();

        $request->session()->flush();    
        $request->session()->regenerate();
        
        return redirect()->route('admin.show_login');
    }
}
