@extends('web.admin.layout')
@section('title')
    Users
@endsection
@section('page_title')
    Users
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table id="users-datatable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Registration Date</th>
                            <th>Added No of Notes</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    var dt = $('#users-datatable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: {
            url:'{{route("admin.users.datatable")}}',
            type:'POST',
        },
        columns: [
            {data: 'DT_RowIndex'},
            {data: 'username'},
            {data: 'email'},
            {data: 'date'},
            {data: 'notes_count'},
            {data: 'action'},
        ]
    });

    $(document).on('click', '.verifyEmail',function(){
        $.ajax({
            url: '{{route('admin.users.verify_email')}}',
            type: "POST",
            data:  {
                id:$(this).data('id'),
            },
            success: function(data){
                dt.ajax.reload();
                toastr.success("User E-mail verified.");
            },
            error: function(){
                toastr.warning('Something went wrong while verifing E-mail.')
            }
        });
    });

    $(document).on('click', '.changeStatus',function(){
        type = $(this).data('type');
        $.ajax({
            url: '{{route('admin.users.change_status')}}',
            type: "POST",
            data:  {
                id:$(this).data('id'),
                type:type,
            },
            success: function(data){
                dt.ajax.reload();
                console.log(type);
                if(type == "Active"){
                    toastr.warning("User Deactivated.");
                }else{
                    toastr.success("User Activated.");
                }
            },
            error: function(){
                toastr.warning('Something went wrong while changing user status.')
            }
        });
    });
</script>
@endsection