<?php

namespace App\Http\Controllers\Api;

use Validator;
use Illuminate\Http\Request;

class HomeController extends ApiController
{
    public function __construct(Request $oRequest)
    {
        parent::__construct($oRequest);
    }

    public function welcome(Request $oRequest){
        if($oRequest->isMethod("GET"))
        {
            $aValidationRequiredFor = [
                'test' => 'bail|required',
            ];

            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                return $this->prepareResponse([
                    'data'          => null,
                    'errors'        => $oValidator->errors(),
                    'status_code'   => config('constants.APIERROR')
                ]);
            }

            return $this->prepareResponse([
                'data'          => null,
                'errors'        => $this->emptyObject(),
                'status_code'   => config('constants.APISUCCESS')
            ]);
        }

        return $this->prepareResponse([
            'data'          => null,
            'errors'        => $this->emptyObject(),
            'status_code'   => config('constants.APIFAIL')
        ]);

    }    
}
