<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['namespace' => 'Api'], function () {

    Route::match(['get','post'], 'user/login-redirect',     'UserController@callLoginRedirect')->name('api.login.redirect');

    Route::get('welcome',  'HomeController@welcome');
    
    Route::match(['get','post'],'user/signup',          'UserController@signup');
    Route::match(['get','post'],'user/login',           'UserController@callLogin');
    Route::match(['get','post'],'user/refresh-token',   'UserController@callRefreshToken');
    Route::match(['get','post'],'user/resend-verification-email','UserController@callResendVerificationEmail');
    Route::match(['get','post'],'user/forgot-password', 'UserController@callForgotPassword');
    
    /**
     * Below route will check authentication
     */
    Route::group(['middleware' => ['auth:api']],function () {
        
        Route::match(['get','post'],'user/edit-profile',    'UserController@callEditProfile');
        Route::match(['get','post'],'user/logout',          'UserController@callLogout');

    });

    /**
     * Notes - return pagination
     */
    Route::get('notes/public',  'NoteController@getNotes');

});
