<?php

return [
    'DEVELOPERSTRING'           => '|][=\/',
    'IOS_USERAGENT'             => 'iOS-app',
    'ANDROID_USERAGENT'         => 'Android-app', //QW5kcm9pZC1hcHA=

    'APIFAIL'                   => -1,
    'APIERROR'                  => 0,
    'APISUCCESS'                => 1,
    'APILOGINERROR'             => 401,

    'WEB_PER_PAGE_RECORDS'      => 20,
    'API_PER_PAGE_RECORDS'      => 10,
    
    'ATTACHMENTFOLDER'          => 'ATTACHMENTS',
    'NOTECOVERFOLDER'           => 'NOTE-COVER',
    'USERMEDIAFOLDER'           => 'USER-PROFILE-PIC',
    'CATEGORYFOLDER'            => 'CATEGORIES',
    
    //AWS
    'MEDIAURL'                  => env('MEDIAURL', 'https://note-chit-local.s3.ap-south-1.amazonaws.com'),

    'CONTACT_EMAIL_RECEIVER'    => 'notechit.info@gmail.com',
    'CONTRIBUTION_QUERY_EXPIRE_DAYS' => 15,
];

?>