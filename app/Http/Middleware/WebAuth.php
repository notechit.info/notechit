<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class WebAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if(Auth::user() && Auth::user()->user_type == \App\Models\User::NORMAL_USER) {
                return $next($request);
            } else {
                return redirect(route('web.user.logout'));
            }
        } else {
            return redirect(route('web.home.index'));
        }
    }
}
