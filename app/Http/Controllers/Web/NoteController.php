<?php

namespace App\Http\Controllers\Web;

use App\Models\Note;
use App\Models\NoteCategory;
use App\Models\Category;
use App\Models\Language;
use App\Models\Favourite;
use App\Models\Rating;
use App\Models\Follow;
use App\Models\NoteView;
use App\Models\NoteReport;
use App\Models\Attachment;
use App\Models\BucketNotes;
use App\Models\NoteDownload;
use Validator;
use Illuminate\Http\Request;
use ZipArchive;
use App\AWS\AwsS3;
use App\Http\Controllers\Web\WebController;
use App\Models\FollowRequest;
use App\Models\Notification;
use App\Models\Bucket;
use DOMDocument;
use Session;

class NoteController extends WebController
{
    protected $allowedCoverPicImageExtensions;
    
    function __construct()
    {
        $this->allowedCoverPicImageExtensions = ['jpg','png','jpeg'];
    }

    /**
     * Add note
     */
    public function addNoteAction(Request $oRequest, $nNoteId = NULL)
    {
        if ($oRequest->isMethod("POST")) {
            if (empty($nNoteId)) //Note Add Logic
            {
                $aValidationRequiredFor = [
                    'title'         => 'required',
                    // 'categories'    => 'required|array',
                    'categories'    => 'required',
                    'language_id'   => 'required|numeric',
                    'note_type'     => 'required',
                    'description'   => 'required',
                    // 'files' => 'required|array',
                    // 'files.*' => 'required|max:10000|mimes:pdf,png,PNG,jpg,JPG,JPEG,jpeg,txt,doc,docx,xls,xlsx,mp4'
                ];

                $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);

                if(!empty($oRequest['file'])) {
                    $aValidationRequiredFor = [
                        'file' => 'required|array|max:15',
                        'file.*' => 'required|mimes:pdf,png,PNG,jpg,JPG,JPEG,jpeg,txt,doc,docx,xls,xlsx,mp4,pptx,txt,ppt,WEBM,MPG,MP2,MPEG,MPE,MPV,OGG,MP4,M4P,M4V,AVI,WMV,MOV,QT,FLV,SWF,AVCHD'
                    ];

                    $aValidationRequiredMsgFor = [
                        'file.*.required' => 'Please upload file',
                        'file.*.mimes' => 'Please upload valid file',
                        'file.*.max' => 'You can upload maximum 10MB file',
                    ];
                    $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor, $aValidationRequiredMsgFor);
                }

                if ($oValidator->fails()) {
                    return $this->prepareJsonResponse([
                        'status'        => 0,
                        'errors'        => $oValidator->errors()
                    ]);
                }

                $file = $oRequest->file('cover');
                $sFileName = NULL;
                $sFileOriginalName = NULL;
                $sFolder = config('constants.NOTECOVERFOLDER');
                if ($file) {
                    $sFileOriginalName = $file->getClientOriginalName();
                    $sFileName = AwsS3::s3UploadImage($file, $sFolder, $sFileName = '');
                }else{
                    $attachmentFiles = $oRequest['file'];
                    if($attachmentFiles != null){
                        foreach($attachmentFiles as $fileSingle){
                            $sFileOriginalName = $fileSingle->getClientOriginalName();
                            $extension = pathinfo($sFileOriginalName, PATHINFO_EXTENSION);
    
                            if(in_array($extension, $this->allowedCoverPicImageExtensions)){
                                $sFileName = AwsS3::s3UploadImage($fileSingle, $sFolder, $sFileName = '');
                            }
                        }
                    }
                }
                $description = $this->imageUploadFromDescription($oRequest->get('description'));

                //Uploading file and changing content for files
                $oNote = Note::create([
                    'title'         => $oRequest->get('title'),
                    'description'   => $description,
                    'file_name'     => $oRequest->has('file_name') ? $oRequest->get('file_name') : NULL,
                    'display_file_name' => $oRequest->get('display_file_name') ? $oRequest->get('display_file_name') : NULL,
                    'user_id'       => \Auth::user()->id,
                    'language_id'   => $oRequest->get('language_id'),
                    'note_type'     => $oRequest->get('note_type'),
                    'file_name'     => $sFileName,
                    'display_file_name' => $sFileOriginalName,
                    'is_video_note'     => $oRequest->get('is_video_note') == 1 ? 1 : 0,
                    'activated'     => 1,
                    'deleted'       => 0
                ]);

                $defaultBucket = Bucket::getDefaultBucket(\Auth::user()->id);
                if($defaultBucket){
                    BucketNotes::addNoteToDefaultBucket($oNote->id, $defaultBucket->id, \Auth::user()->id);
                }

                $oCategories = is_array($oRequest->get('categories')) ? $oRequest->get('categories') : explode(',', trim($oRequest->get('categories')));
                foreach ($oCategories as $oCategory) {
                    NoteCategory::create([
                        'note_id' => $oNote->id,
                        'category_id' => $oCategory,
                        'activated'     => 1,
                        'deleted'       => 0
                    ]);
                }

                if($oNote->note_type == Note::FO){
                    $followers = Follow::getFollowersUserList(\Auth::user()->id);
                    foreach($followers as $follower){
                        Notification::create([
                            'user_id' => $follower->user_id,
                            'from_user_id' => \Auth::user()->id,
                            'type' => 'NOTE',
                            'entity_id' => $oNote->id,
                        ]);
                    }                 
                }
                $sAttachmentFolder = config('constants.ATTACHMENTFOLDER');

                // $files = $oRequest->file('files');
                $files = $oRequest['file'];

                if(!empty($files)) {
                    foreach ($files as $file) {
                        $sFileOriginalName1 = $file->getClientOriginalName();
                        $sFileName1 = AwsS3::s3UploadImage($file, $sAttachmentFolder, $sFileName = '');

                        Attachment::create([
                            'file_name'     => $sFileName1,
                            'display_file_name'  => $sFileOriginalName1,
                            'note_id'       => $oNote->id,
                            'activated'     => 1,
                            'deleted'       => 0
                        ]);
                    }
                }

                return $this->prepareJsonResponse([
                    'status'   => 1,
                    'message'  => 'Note successfully created',
                ]);
            } else //Note update Logic
            {
                $aValidationRequiredFor = [
                    'title'         => 'required',
                    'categories'    => 'required',
                    'language_id'   => 'required|numeric',
                    'note_type'     => 'required',
                    'description'   => 'required',
                ];

                $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);

                if ($oValidator->fails()) {
                    return $this->prepareJsonResponse([
                        'status'        => 0,
                        'errors'        => $oValidator->errors()
                    ]);
                }

                if (!$oNote = Note::find($nNoteId)) {
                    return $this->prepareJsonResponse([
                        'status'   => 0,
                        'message'  => 'No Note Found for this user'
                    ]);
                }

                $file = $oRequest->file('cover');
                $sFileName = $oNote->file_name;
                $sFileOriginalName = $oNote->display_file_name;
                if ($file) {
                    $sFolder = config('constants.NOTECOVERFOLDER');
                    $sFileOriginalName = $file->getClientOriginalName();
                    $sFileName = AwsS3::s3UploadImage($file, $sFolder, $sFileName = '');
                }

                $description = $this->imageUploadFromDescription($oRequest->get('description'));

                $oNote->title = $oRequest->get('title');
                $oNote->description   = $description;
                $oNote->user_id       = \Auth::user()->id;
                $oNote->language_id   = $oRequest->get('language_id');
                $oNote->note_type     = $oRequest->get('note_type');
                $oNote->file_name     = $sFileName;
                $oNote->display_file_name = $sFileOriginalName;
                $oNote->is_video_note     = $oRequest->get('is_video_note') == 1 ? 1 : 0;
                $oNote->activated     = 1;
                $oNote->deleted       = 0;
                $oNote->save();

                $oCategories = is_array($oRequest->get('categories')) ? $oRequest->get('categories') : explode(',', trim($oRequest->get('categories')));
                foreach ($oCategories as $oCategory) {

                    $oNoteCategory = NoteCategory::firstOrNew([
                        'note_id'       => $oNote->id,
                        'category_id'   => $oCategory
                    ]);

                    $oNoteCategory->activated = 1;
                    $oNoteCategory->deleted = 0;
                    $oNoteCategory->save();

                    if ($oNoteCaties = NoteCategory::where('note_id', $oNote->id)->whereNotIn('category_id', $oCategories)->get()) {
                        $oNoteCaties->map(function ($oNewNoteCategory) {
                            $oNewNoteCategory->delete();
                        });
                    }
                }



                if(!empty($oRequest['file'])) {
                    $aValidationRequiredFor = [
                        'file' => 'required|array|max:15',
                        'file.*' => 'required|mimes:video/quicktime,pdf,png,PNG,jpg,JPG,JPEG,jpeg,txt,doc,docx,xls,xlsx,mp4,pptx,txt,ppt,WEBM,MPG,MP2,MPEG,MPE,MPV,OGG,MP4,M4P,M4V,AVI,WMV,MOV,QT,FLV,SWF,AVCHD,qt'
                    ];

                    $aValidationRequiredMsgFor = [
                        'file.*.required' => 'Please upload file',
                        'file.*.mimes' => 'Please upload valid file',
                        'file.*.max' => 'You can upload maximum 40MB file',
                    ];

                    $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor, $aValidationRequiredMsgFor);

                    if ($oValidator->fails()) {
                        return $this->prepareJsonResponse([
                            'status'        => 0,
                            'errors'        => $oValidator->errors()
                        ]);
                    }
                }

                $sAttachmentFolder = config('constants.ATTACHMENTFOLDER');

                $files = $oRequest['file'];
                if(!empty($files)) {
                    foreach ($files as $file) {
                        $sFileOriginalName1 = $file->getClientOriginalName();
                        $sFileName1 = AwsS3::s3UploadImage($file, $sAttachmentFolder, $sFileName = '');

                        Attachment::create([
                            'file_name'     => $sFileName1,
                            'display_file_name'  => $sFileOriginalName1,
                            'note_id'       => $oNote->id,
                            'activated'     => 1,
                            'deleted'       => 0
                        ]);
                    }
                }

                return $this->prepareJsonResponse([
                    'status'   => 1,
                    'message'  => 'Note successfully updated',
                ]);
            }

            return $this->prepareJsonResponse([
                'status'   => 1,
                'message'  => 'Note successfully created',
            ]);
        }

        $oCategories = Category::active()->get();
        $oLanguages = Language::active()->get();

        if ($oNote = Note::with(['categories', 'attachments'])->find($nNoteId)) {
            if ($oNote->user_id != \Auth::user()->id) {
                return redirect()->route('web.note.add-note');
            }
        }

        return view("web.notes.add_chit", [
            'oCategories' => $oCategories,
            'oLanguages' => $oLanguages,
            'nNoteId' => $nNoteId,
            'oNote' => $oNote
        ]);
    }

    /**
     * User notes - my notes
     */
    public function myNotesAction()
    {
        $oNotes = \Auth::user() ? Note::getMyNotes(\Auth::user()->id) : $oNotes = [];

        $nAvgRating = 0;
        foreach ($oNotes as $key => $oNote) {
            $nAvgRating = 0;
            $nTotalStars = Rating::where('note_id', $oNote->note_id)->active()->sum('star');
            $nTotalUsers = Rating::where('note_id', $oNote->note_id)->active()->count();
            if ($nTotalStars > 0) {
                $nAvgRating = round($nTotalStars / $nTotalUsers);
            }
            $oNotes[$key]['nAvgRating'] = $nAvgRating;
        }

        return view("web.user.user_notes", [
            'oNotes' => $oNotes
        ]);
    }

    /**
     * User notes - saved notes
     */
    // public function savedNotesAction(){

    //     if(\Auth::user())
    //     {
    //         $aNoteIds = Favourite::where('user_id',\Auth::user()->id)->active()->get()->pluck('note_id')->toArray();
    //         $oNotes = Note::getSavedNotes($aNoteIds);
    //     }
    //     else
    //     {
    //         $oNotes = [];
    //     }

    //     $nAvgRating = 0;
    //     foreach($oNotes as $key => $oNote)
    //     {
    //         $nAvgRating = 0;
    //         $nTotalStars = Rating::where('note_id',$oNote->note_id)->active()->sum('star');
    //         $nTotalUsers = Rating::where('note_id',$oNote->note_id)->active()->count();
    //         if($nTotalStars > 0)
    //         {
    //             $nAvgRating = round($nTotalStars / $nTotalUsers);
    //         }

    //         $oNotes[$key]['nAvgRating'] = $nAvgRating;
    //     }

    //     return view("web.user.saved_notes",[
    //         'oNotes' => $oNotes
    //     ]);
    // }

    public function savedNotesAction()
    {

        if (\Auth::user()) {
            $aNoteIds = BucketNotes::where('user_id', \Auth::user()->id)->active()->get()->pluck('note_id')->toArray();
            //            $aNoteIds = Favourite::where('user_id',\Auth::user()->id)->active()->get()->pluck('note_id')->toArray();
            $oNotes = Note::getSavedNotes($aNoteIds);
        } else {
            $oNotes = [];
        }

        $nAvgRating = 0;
        foreach ($oNotes as $key => $oNote) {
            $nAvgRating = 0;
            $nTotalStars = Rating::where('note_id', $oNote->note_id)->active()->sum('star');
            $nTotalUsers = Rating::where('note_id', $oNote->note_id)->active()->count();
            if ($nTotalStars > 0) {
                $nAvgRating = round($nTotalStars / $nTotalUsers);
            }

            $oNotes[$key]['nAvgRating'] = $nAvgRating;
        }

        return view("web.user.saved_notes", [
            'oNotes' => $oNotes
        ]);
    }

    /**
     * Following users notes - saved notes
     */
    public function followingUsersNotesAction()
    {

        if (\Auth::user()) {
            $aFollowersUserIds = Follow::where('following_user_id', \Auth::user()->id)->active()->get()->pluck('follower_user_id')->toArray();
            $oNotes = Note::getFollowerUsersNotes($aFollowersUserIds);
        } else {
            $oNotes = [];
        }

        $nAvgRating = 0;
        foreach ($oNotes as $key => $oNote) {
            $nAvgRating = 0;
            $nTotalStars = Rating::where('note_id', $oNote->note_id)->active()->sum('star');
            $nTotalUsers = Rating::where('note_id', $oNote->note_id)->active()->count();
            if ($nTotalStars > 0) {
                $nAvgRating = round($nTotalStars / $nTotalUsers);
            }

            $oNotes[$key]['nAvgRating'] = $nAvgRating;
        }

        return view("web.user.follower_user_notes", [
            'oNotes' => $oNotes
        ]);
    }


    /**
     * Note Detail Action
     */
    public function noteDetailAction($nNoteId = '')
    {
        if (empty($nNoteId))
            return redirect()->back();

        if (!$oNote = Note::getNoteDetail($nNoteId)) {
            abort(404);
        }

        $oNote->note_categories = NoteCategory::getNoteCategories($nNoteId);

        $bAlreadyRated = 0;
        $nRating = 0;
        $isFollwed = false;
        $isSelfNote = false;
        $isFollowRequested = false;

        if (\Auth::user()) {
            if($oNote->user_id == \Auth::user()->id){
                $isSelfNote = true;
            }

            NoteView::saveViewEntry($oNote->id, \Auth::user()->id);

            if ($oMyRating = Rating::where([
                'note_id' => $oNote->id,
                'user_id' => \Auth::user()->id,
                'activated' => 1,
                'deleted' => 0
            ])->first()) {
                $nRating = $oMyRating->star;
                $bAlreadyRated = 1;
            }

            if ($isFollwed = Follow::where('follower_user_id', $oNote->user_id)->where('following_user_id', \Auth::user()->id)->active()->first()) {
                $isFollwed = true;
            }
        }else{
            if($oNote->note_type == Note::FO){
                Session::flash('ask_login', 'true');
                return redirect()->route('web.home.index');
            }
        }

        $nTotalStars = Rating::where('note_id', $oNote->id)->active()->sum('star');
        $nTotalUsers = Rating::where('note_id', $oNote->id)->active()->count();

        $nAvgRating = 0;
        if ($nTotalStars > 0) {
            $nAvgRating = round($nTotalStars / $nTotalUsers);
        }

        if(! $isSelfNote && $oNote->note_type == Note::FO && ! $isFollwed ){
             $followRequestPending = FollowRequest::where([
                    'user_id'   => \Auth::user()->id, 
                    'to_user_id'=> $oNote->user_id,
                    'ff_status' => FollowRequest::PENDING,
                ])
                ->active()
                ->count();
            if($followRequestPending > 0){
                $isFollowRequested = true;
            } 
        }

        return view("web.notes.note_detail", [
            'oNote'         => $oNote,
            'nRating'       => $nRating,
            'bAlreadyRated' => $bAlreadyRated,
            'nAvgRating'    => $nAvgRating,
            'isFollwed'     => $isFollwed,
            'isSelfNote'    => $isSelfNote,
            'isFollowRequested' => $isFollowRequested,
        ]);
    }

    /**
     * Add fav note or saved note
     */
    public function addNoteToFavouriteAction(Request $oRequest)
    {
        $oFav = Favourite::firstOrNew([
            'note_id' => $oRequest->note_id,
            'user_id' => \Auth::user()->id
        ]);

        $oFav->activated = $oRequest->action == "add" ? 1 : 0;
        $oFav->deleted = 0;
        $oFav->save();

        return $this->prepareJsonResponse([
            'status'   => 1,
            'message'  => $oRequest->action == "add" ? trans('messages.add_fav_success') : trans('messages.remove_fav_success')
        ]);
    }

    /**
     * Remove note from folder
     */
    public function removeNoteFromFolderAction(Request $oRequest)
    {
        $oBucketNotes = BucketNotes::where([
            'note_id' => $oRequest->note_id,
            'user_id' => \Auth::user()->id,
            'deleted' => 0
        ])->update([
            'activated' =>  0,
            'deleted' =>  1,
        ]);

        return $this->prepareJsonResponse([
            'status'   => 1,
            'message'  => $oRequest->action == "add" ? trans('messages.add_fav_success') : trans('messages.remove_fav_success')
        ]);
    }


    /**
     * Add rating
     */
    public function addRatingAction(Request $oRequest)
    {

        $aValidationRequiredFor = [
            'star'         => 'required|numeric|in:1,2,3,4,5'
        ];

        $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
        if ($oValidator->fails()) {
            return $this->prepareJsonResponse([
                'status'        => 0,
                'errors'        => $oValidator->errors()
            ]);
        }

        $oNote = Note::find($oRequest->note_id);
        if ($oNote->user_id == \Auth::user()->id) {
            return $this->prepareJsonResponse([
                'status'   => 2,
                'message'  => 'Sorry! You can not add rating to your own note'
            ]);
        }

        $oRating = Rating::firstOrNew([
            'note_id' => $oRequest->note_id,
            'user_id' => \Auth::user()->id
        ]);

        if ($oRating->exists) {
            $bRatingExists = 1;
        } else {
            $bRatingExists = 0;
            $oRating->star = $oRequest->star;
            $oRating->activated = 1;
            $oRating->deleted = 0;
            $oRating->save();
        }

        return $this->prepareJsonResponse([
            'status'   => 1,
            'message'  => 'success',
            'bRatingExists' => $bRatingExists
        ]);
    }

    /**
     * Add Report
     */
    public function addReportAction(Request $oRequest)
    {

        $aValidationRequiredFor = [
            'problem' => 'required',
            'reason' => 'required',
            'note_id' => 'required|exists:notes,id'
        ];

        $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
        if ($oValidator->fails()) {
            return $this->prepareJsonResponse([
                'status'        => 0,
                'errors'        => $oValidator->errors()
            ]);
        }

        NoteReport::create([
            'note_id' => $oRequest->note_id,
            'user_id' => \Auth::user()->id,
            'problem' => $oRequest->problem,
            'reason' => $oRequest->reason
        ]);

        return $this->prepareJsonResponse([
            'status'   => 1,
            'message'  => trans('messages.add_report_success')
        ]);
    }

    public function searchNoteAction(Request $oRequest, $sSearchKey = '')
    {
        $sSearchKey = $oRequest->get('term');
        $oNotes = Note::searchNotes($sSearchKey);

        if($oNotes->count() > 0){
            $return_data = $oNotes->map(function ($oNote) {
                return [
                    'note_id'    => $oNote->note_id,
                    'title'  => $oNote->title,
                    'name'  => $oNote->full_name,
                    'image' => $oNote->is_video_note == 0 ? getNoteCoverImageUrl($oNote->file_name) : asset('web/img/video-icon.svg'),
                ];
            });
            
            $searchPlaceHolder = $oNotes->count() . ' Results found for search term "'. $sSearchKey .'"';
            return $this->prepareJsonResponse([
                'response' => array_merge([['note_id' => 0, 'title' => $searchPlaceHolder, 'search_term'=> $sSearchKey]], json_decode(json_encode($return_data)))
            ]);    
        }else{
            return $this->prepareJsonResponse([
                'response' => array_merge([['note_id' => 0, 'title' => $sSearchKey]], [['note_id'=> null, 'title'=> 'No notes found. Try some different keywords!']])
            ]);    
        }
    }

    /*
    public function searchNoteAction(Request $oRequest,$sSearchKey = '')
    {
        $sSearchKey = $oRequest->get('query');

        if(!empty($sSearchKey))
            $oNotes = Note::searchNotes($sSearchKey);
        else
            $oNotes = Note::getNotes(Note::PUB);

        $sHtml = 'Not found';

        if($oNotes && $oNotes->count() > 0){
            $sHtml = view("web.notes.notes", [
                'oNotes' => $oNotes
            ])->render();
        }

        return $this->prepareJsonResponse([
            'success'   => 1,
            'html' => $sHtml,
            'message'  => 'Get Notes successfully',
        ]);
    }
    */


    /**
     * Delete Note
     */
    public function callDeleteNoteAction(Request $oRequest)
    {
        $aValidationRequiredFor = [
            'note_id' => 'required|exists:notes,id'
        ];

        $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
        if ($oValidator->fails()) {
            return $this->prepareJsonResponse([
                'status'        => 0,
                'errors'        => $oValidator->errors()
            ]);
        }

        try {
            \DB::transaction(function () use ($oRequest) {

                $oNote = Note::with([
                    'attachments',
                    'bucketNotes',
                    'favourites',
                    'categories',
                    'noteDownloads',
                    'noteReports',
                    'noteViews',
                    'ratings'
                ])->find($oRequest->get('note_id'));

                if ($oNote->attachments) {
                    $oNote->attachments->map(function ($attachment) {
                        $attachment->delete();
                    });
                }

                if ($oNote->bucketNotes) {
                    $oNote->bucketNotes->map(function ($bucketNote) {
                        $bucketNote->delete();
                    });
                }

                if ($oNote->favourites) {
                    $oNote->favourites->map(function ($favourite) {
                        $favourite->delete();
                    });
                }

                if ($oNote->categories) {
                    $oNote->categories->map(function ($category) {
                        $category->delete();
                    });
                }

                if ($oNote->noteDownloads) {
                    $oNote->noteDownloads->map(function ($noteDownload) {
                        $noteDownload->delete();
                    });
                }

                if ($oNote->noteReports) {
                    $oNote->noteReports->map(function ($noteReport) {
                        $noteReport->delete();
                    });
                }

                if ($oNote->noteViews) {
                    $oNote->noteViews->map(function ($noteView) {
                        $noteView->delete();
                    });
                }

                if ($oNote->ratings) {
                    $oNote->ratings->map(function ($rating) {
                        $rating->delete();
                    });
                }

                $oNote->delete();
            }); // end of transaction
        } catch (\Exception $e) {
            return $this->prepareJsonResponse([
                'success'   => 0,
                'message'  => 'Exception : Something went wrong to deleted note',
            ]);
        }

        return $this->prepareJsonResponse([
            'success'   => 1,
            'message'  => 'Note successfully deleted',
        ]);
    }

    /**
     * Delete Note attachment
     */
    public function callDeleteNoteAttachmentAction(Request $oRequest)
    {
        $aValidationRequiredFor = [
            'attachment_id' => 'required|exists:attachments,id'
        ];

        $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
        if ($oValidator->fails()) {
            return $this->prepareJsonResponse([
                'status'        => 0,
                'errors'        => $oValidator->errors()
            ]);
        }

        if ($oAttachment = Attachment::find($oRequest->get('attachment_id'))) {
            $oAttachment->delete();

            return $this->prepareJsonResponse([
                'success'   => 1,
                'message'  => 'Note successfully deleted',
            ]);
        }

        return $this->prepareJsonResponse([
            'success'   => 0,
            'message'  => 'Something went wrong'
        ]);
    }

    public function downloadFileFromS3(Request $oRequest)
    {
        $oAttachment = Attachment::find($oRequest->get('id'));
        $sAttachmentFolder = config('constants.ATTACHMENTFOLDER');
        $file_url = getNoteAttachmentUrl($oAttachment->file_name);

        //$this->downloadFileToLocalFolder($file_url);

        die;

        /*
        $file_url = public_path().'/AllDocuments.zip';
        $file_name = "AllDocuments.zip";
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"".$file_name."\"");
        flush();
        readfile($file_url);
        exit;*/
    }

    /**
     * Download file to local public folder function
     *
     * @return boolean
     */
    public function downloadFileToLocalFolder($url)
    {
        // $url = 'https://contribute.geeksforgeeks.org/wp-content/uploads/gfg-40.png';
        $file_name = basename($url);
        file_put_contents("ziparchive/" . $file_name, file_get_contents($url));

        /*
        if(file_put_contents("ziparchive/".$file_name,file_get_contents($url))) {
            echo "File downloaded successfully";
        }
        else {
            echo "File downloading failed.";
        }*/

        return true;
    }

    public function callDownloadZipAction(Request $oRequest, $nNoteId)
    {
        if (!$oNote = Note::find($nNoteId)) {
            return redirect()->back();
        }

        NoteDownload::saveDownloadEntry($nNoteId, \Auth::user()->id);

        $oAttachments = Attachment::where('note_id', $nNoteId)->get();
        $aNewAttachments = [];
        foreach ($oAttachments as $oAttachment) {
            $this->downloadFileToLocalFolder(getNoteAttachmentUrl($oAttachment->file_name));
            //array_push($aNewAttachments,getNoteAttachmentUrl($oAttachment->file_name));
            array_push($aNewAttachments, public_path() . '/ziparchive/' . $oAttachment->file_name);
        }

        $this->downloadZip($aNewAttachments, $oNote->title);

        return redirect()->back();
        exit;
    }

    /**
     * Download Zip
     */
    public function downloadZip($files = array(), $title)
    {
        //$files = array(public_path().'/310321051526_1617167726.jpeg', public_path().'/310321054320_1617169400.doc');

        $public_dir = public_path();
        $zipFileName = $title . "_" . date("dmyhis") . '.zip';
        $zip = new ZipArchive;

        $filetopath = $public_dir . '/temp-ziparchive/' . $zipFileName;

        if ($zip->open($filetopath, ZipArchive::CREATE) === TRUE) {
            //$zip->addFile(file_path,'file_name');
            foreach ($files as $file) {
                $new_filename = substr($file, strrpos($file, '/') + 1);
                $zip->addFile($file, $new_filename);
                //$zip->addFile($file);
            }
            $zip->close();
        }

        $headers = array(
            'Content-Type' => 'application/octet-stream',
        );

        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . $zipFileName . "\"");
        flush();
        readfile($filetopath);
    }

    public function getNoteViewData(Request $request){
        $note = Note::with(['noteViews.user'])->find($request->note_id);
        if(! $note){
            return response()->json('Data Not Found', 404);
        }

        $return_data = $note->noteViews->map(function ($noteView) {
            return [
                'id'    =>  $noteView->id,
                'name'  =>  $noteView->user->full_name,
                'image' =>  getUserImageUrl($noteView->user->file_name,$noteView->user->profile_pic_url),
                'uid'   =>  $noteView->user->id
            ];
        });
        
        return response()->json($return_data, 200);
    }

    /**
     * Upload images from description and change description image's src attribute value
     *
     * @param string $description
     * @return string $description
     */
    public function imageUploadFromDescription($description){
        $dom = new DomDocument();
        $dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    

        $images = $dom->getElementsByTagName('img');  
        foreach($images as $k => $img){
            $data = $img->getAttribute('src');
            list($type, $data) = explode(';', $data);
            list($type, $data) = explode(',', $data);
            $data = base64_decode($data);
            $image_name= "/note_images/" . time().$k.'.png';
            $path = public_path() . $image_name;
            file_put_contents($path, $data);
            $img->removeAttribute('src');
            $img->setAttribute('src', $image_name);
        }

        return $dom->saveHTML();
    }
}
