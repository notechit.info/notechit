@extends('web.admin.layout')
@section('title')
    Language
@endsection
@section('page_title')
    Language
@endsection
@section('css')
<style>
div.dt-buttons {
    float: right;
}
</style>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table id="language-datatable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>name</th>
                            <th>No of Notes</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addLanguageModel" tabindex="-1" role="dialog" aria-labelledby="addLanguageModelLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form id="form">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="addLanguageModelLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Name:</label>
                            <input type="text" name="language_name" id="language_name" class="form-control">
                            <span id="language_name-error" style="color:red;"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btnSave"></button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('js')
<script>
    var dt = $('#language-datatable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: {
            url:'{{route("admin.language.datatable")}}',
            type:'POST',
        },
        columns: [
            {data: 'DT_RowIndex'},
            {data: 'name'},
            {data: 'notes_count'},
            {data: 'action'},
        ],
        dom: 'Bfrtip',
        buttons: [
            {
                text: 'Add Language',
                className : 'btn-sm ml-2 mr-2 text-right',
                action: function ( e, dt, node, config ) {
                    showAdd();
                }
            }
        ]
    });
    
    function showAdd(){
        clearForm();
        $('#addLanguageModelLabel').text('Add Language');
        $('#addLanguageModel').modal('show');
        $('#btnSave').text('Add');
        $('#btnSave').addClass('saveLanguage');
    }
    
    $(document).on('click', '.saveLanguage', function(){
        language = $('#language_name').val();

        if(language != ''){
            $.ajax({
                type:'POST',
                url: '{{route('admin.language.store')}}',
                data:{
                    language:language,
                },
                success:function(data){
                    if(data.status == "Error"){
                        $('#language_name-error').text(data.message);
                    }else{
                        toastr.success(data.message);
                        clearForm();
                        dt.ajax.reload();
                    }
                },
                error: function(data){
                    console.log(data);
                }
            });
        }else{
            $('#language_name-error').text("This field is required.");
            $('#language_name').focus();
        }
    });

    function clearForm(){
        $('#language_name').val('');
        $('#addLanguageModel').modal('hide');
        $('#language_name-error').text('');
        $('#btnSave').text('');
        $('#btnSave').removeClass('saveLanguage');
        $('#btnSave').removeClass('updateLanguage');
        $('#btnSave').removeAttr('data-id');
    }
    $(document).ready(function(){
        $("form").submit(function(e){
            e.preventDefault();
        });
    });

    $(document).on('click', '.editLanguage',function(){
        clearForm();
        $('#addLanguageModelLabel').text('Edit Language');
        data_id = $(this).data('id');
        data_name = $(this).data('name');
        $('#btnSave').text('Update');
        $('#btnSave').attr('data-id', data_id);
        $('#btnSave').addClass('updateLanguage');
        $('#language_name').val(data_name);
        $('#addLanguageModel').modal('show');
    });
    $(document).on('click', '.updateLanguage',function(){
        language = $('#language_name').val();
        data_id = $(this).data('id');

        let url = "{{ route('admin.language.update', ':id') }}";
        url = url.replace(':id', data_id);        
        if(language != ''){
            $.ajax({
                type:'PUT',
                url: url,
                data:{
                    language:language,
                },
                success:function(data){
                    if(data.status == "Error"){
                        $('#language_name-error').text(data.message);
                    }else{
                        toastr.success(data.message);
                        clearForm();
                        dt.ajax.reload();
                    }
                },
                error: function(data){
                    console.log(data);
                }
            });
        }else{
            $('#language_name-error').text("This field is required.");
            $('#language_name').focus();
        }
    });
    $(document).on('click', '.deleteLanguage',function(){
        var data_id = $(this).data('id');
        let url = "{{ route('admin.language.destroy', ':id') }}";
        url = url.replace(':id', data_id);

        $.ajax({
            type:'DELETE',
            url: url,
            success:function(data){
                toastr.success(data);
                dt.ajax.reload();
            },
            error: function(data){
                toastr.warning(data);
            }
        });
    });
    </script>
@endsection