
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('web/img/fav/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('web/img/fav/favicon-96x96.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('web/img/fav/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('web/img/fav/manifest.json') }}">


        <link type="text/css" rel="stylesheet" href="{{ asset('web/css/bootstrap.min.css') }}">
        <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" /> -->
        
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="{{ asset('web/css/summernote-bs4.min.css') }}" rel="stylesheet" />

        <link type="text/css" rel="stylesheet" href="{{ asset('web/css/nc_animation.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
        
        <link type="text/css" rel="stylesheet" href="{{ asset('web/css/aos.css') }}">
        <link type="text/css" rel="stylesheet" href="{{ asset('web/css/nc_styles.css') }}">
        <link type="text/css" rel="stylesheet" href="{{ asset('web/css/nc_media_queries.css') }}">


        <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="{{ asset('web/js/autocomplete/js/jquery-ui.js') }}"></script>
        <!-- // jquery error fixes -->
        <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script type="text/javascript" src="{{ asset('web/js/bootstrap.min.js') }}"></script>
        <script src="https://kit.fontawesome.com/5761ef49f8.js" crossorigin="anonymous"></script>
        
        <script type="text/javascript" src="{{ asset('web/js/jquery.form.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/js/fileinput.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/themes/fa/theme.js"></script>
        <script src="{{ asset('web/js/aos.js') }}"></script>
        <script src="{{ asset('web/js/summernote-bs4.min.js') }}"></script>
        <script src="{{ asset('web/js/common.js') }}"></script>

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

