<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    const IN_ACTIVE = 0 , ACTIVE = 1;

    protected $fillable = [
        'name',
        'activated',
        'deleted'
    ];

    public function scopeActive($query)
    {
        return $query->where('languages.activated',self::ACTIVE);
    }

    public function notes(){
        return $this->hasMany(Note::class);
    }
}
