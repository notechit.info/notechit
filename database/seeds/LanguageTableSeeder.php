<?php

use Illuminate\Database\Seeder;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aNames = array('ENGLISH','HINDI','GUJARATI','TAMIL','BANGLA','TELGU','ORIYA','MARATHI','KONKANI','BANGLA','KANNADA','NEPALI','MANIPURI','ASSAMESE','KASHMIRI','MAITHIL','SINDHI','URDU','MALAYALAM');

        foreach ($aNames as $name) {
            
            $oUser = \App\Models\Language::firstOrNew([
                'name'  => $name,
                'activated' => 1,
                'deleted'   => 0
            ]);

            $oUser->save(); 
        }
    }
}