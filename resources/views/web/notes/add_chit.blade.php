@extends('web.main')

@section('title')
{{ trans('messages.add_note') }}
@endsection

@section('content')

<div class="container add-chit-wrapper jsClassReplaceData position-relative">
  <div class="text-center hide py-3 jsClassNoteSavedSuccess new-loader">
    <div class="circle-loader">
      <div class="checkmark draw">
      </div>
    </div>
  </div>
  <h5 class="text-center add-chit-title">
    @lang('messages.add_chit')
    <span class="close-btn">x</span>
  </h5>

  <?php

  $action = route('web.note.add-note');
  if (!empty($nNoteId)) {
    $action = route('web.note.add-note', ['nNoteId' => $nNoteId]);
  }
  ?>

  <form id="note_form" class="add-chit-form" action="{{ $action }}" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" id="noteId" name="noteId" value="{{$nNoteId}}" />
    <div class="row">

      <div class="col-sm-12 col-md-8">
        <div class="form-group">
          <label for="note_title"> @lang('messages.note_title') :</label>
          <input type="text" name="title" class="form-control" id="title" autoFocus aria-describedby="emailHelp" placeholder="" data-toggle="tooltip" data-placement="bottom" title="@lang('messages.note_title_tootltip')" value="{{ isset($oNote) ? $oNote->title : '' }}">
        </div>
        <div class="form-group row">
          <div class="col-sm-12 col-md-8">
            <label for="exampleInputEmail1"> @lang('messages.note_category_tags'): </label>
            <select class="js-example-basic-multiple form-control" id="categories" name="categories[]" multiple="multiple">
                @foreach($oCategories as $nIndex => $oCategory)
                    <option value="{{ $oCategory->id }}" {{ isset($oNote) ? $oNote->categories->contains('category_id',$oCategory->id) ? 'selected' : '' : '' }} data-image="{{getCategoryImageUrl($oCategory->image)}}">{{ $oCategory->category_name }}</option>
                @endforeach
            </select>
          </div>
          <div class=" col-sm-12 col-md-4">
            <label for="exampleInputEmail1"> @lang('messages.note_launguage'): </label>
            <select name="language_id" id="language_id" class="form-control">
              @foreach($oLanguages as $oLanguage)
              <option value="{{ $oLanguage->id }}" {{ isset($oNote) ? ($oNote->language_id == $oLanguage->id ? 'selected' : '') : '' }}>{{ $oLanguage->name }}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
      <!-- <div class="col-2 ">
          <div class="form-group">
          <label for="exampleInputEmail1"> @lang('messages.note_cover_pic') :</label>
          <input type="email" class="form-control" id="exampleInputEmail1"
              aria-describedby="emailHelp" placeholder=""
              data-toggle="tooltip" data-placement="bottom" title="@lang('messages.note_title_tootltip')">
          </div>
      </div> -->
      <div class="col-sm-12 col-md-4">
        <div class="form-group nc-cover-pic-wrapper full-height">
          <label for="exampleInputEmail1"> @lang('messages.note_cover_pic') :</label>
          <div class="form-control thumbnail-wrapper">
            <input type="file" id="browse_file" name="cover" class="" />
            @if(!isset($oNote))
                <img class="jsClassPlaceHolderImg" src="{{asset('web/img/dimensions.png')}}" />
            @else
                @if($oNote->file_name)
                    <img class="jsClassPlaceHolderImg" src="{{ getNoteCoverImageUrl($oNote->file_name) }}" />
                @else
                    <img class="jsClassPlaceHolderImg" src="{{asset('web/img/dimensions.png')}}" />
                @endif
            @endif
            <img id="preview_image" class="d-none cover-thumbnail-img" src="" />
            <div class="jsClassRemoveImg remove-file d-none" onClick="removeAttachedFile();">
                @lang('messages.remove_image')
            </div>
          </div>
        </div>
      </div>

      <div class="col-12">
        <label for="exampleInputEmail1"> @lang('messages.note_visibility') :</label>
        <div class="form-group form-control">
          <div class="row">
            <div class="col-sm-12 col-md-4">
              <div class="form-check note-visibility-radio-btn">
                <input class="form-check-input" type="radio" name="note_type" id="exampleRadios1" value="{{ \App\Models\Note::PUB }}" {{ !isset($oNote) ? 'checked' : '' }} {{ isset($oNote) && ($oNote->note_type == \App\Models\Note::PUB) ? 'checked' : '' }}>
                <label class="form-check-label" for="exampleRadios1">
                  <b> @lang('messages.public_note') </b><br>
                  @lang('messages.public_note_desc')
                </label>
              </div>
            </div>
            <div class="col-sm-12 col-md-4">
              <div class="form-check note-visibility-radio-btn">
                <input class="form-check-input" type="radio" name="note_type" id="exampleRadios2" value="{{ \App\Models\Note::FO }}" {{ isset($oNote) && ($oNote->note_type == \App\Models\Note::FO) ? 'checked' : '' }}>
                <label class="form-check-label" for="exampleRadios2">
                  <b> @lang('messages.share_with_followers_only') </b><br>
                  @lang('messages.share_with_followers_only_desc')
                </label>
              </div>
            </div>
            <div class="col-sm-12 col-md-4">
              <div class="form-check note-visibility-radio-btn">
                <input class="form-check-input" type="radio" name="note_type" id="exampleRadios3" value="{{ \App\Models\Note::PVT }}" {{ isset($oNote) && ($oNote->note_type == \App\Models\Note::PVT) ? 'checked' : '' }}>
                <label class="form-check-label" for="exampleRadios3">
                  <b> @lang('messages.only_me') </b><br>
                  @lang('messages.only_me_desc')
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12">
        <div class="form-group nc-editor-wrapper">
          <label for="exampleInputEmail1"> @lang('messages.note_description') :</label>
          <textarea id="summernote" required class="summernote" data-msg="Please enter descriptioon">{{ isset($oNote) ? $oNote->description : '' }}</textarea>
          <input type="hidden" id="description" name="description">
        </div>
      </div>

      <div class="col-12">
        <div class="form-group">
          <label for="exampleInputEmail1"> @lang('messages.upload_files') :</label>
          <div class="custom-control custom-switch float-right d-d-inline-block">
            <input type="checkbox" class="custom-control-input" name="is_video_note" value="1" id="switch1" {{ isset($oNote) && ($oNote->is_video_note == 1) ? 'checked' : '' }}>
            <label class="custom-control-label" for="switch1"> @lang('messages.video_note')</label>
          </div>
          <div class="dropzone" id="myDropzone"></div>
        </div>
      </div>
      @if(isset($oNote) && count($oNote->attachments) != 0 )
          <div class="col-12">
              <div class="form-group">
                  <label> Uploaded Files </label>
                  <ul>
                      @foreach($oNote->attachments as $oAttachment)
                          <li id="li-attachment-{{ $oAttachment->id }}"><i class="fas fa-file" style="color:gray;"></i> {{ $oAttachment->display_file_name }} -
                              <button type="button" class="nc-black-btn nc-xs-text nc-radius n-js-delete-attachment" data-action="delete" data-id="{{ $oAttachment->id }}" data-toggle="tooltip" data-placement="bottom" title="@lang('messages.delete_file')">
                                  <i class="fas fa-trash-alt"></i>
                              </button>
                          </li>
                      @endforeach
                  </ul>
              </div>
          </div>
      @endif
      <input type="button" id="note_form_submit_btn" class="nc-black-btn nc-save-btn my-3 mx-auto" value="@lang('messages.save')">
    </div>
  </form>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>

{{ Html::script('plugins/jquery-validation/dist/jquery.validate.min.js') }}
{{ Html::script('plugins/jquery-validation/dist/additional-methods.min.js') }}

<script>
  Dropzone.options.myDropzone = {
    url: "{{ $action }}",
    headers: {
      'x-csrf-token': $('input[name="_token"]').val(),
    },
    autoProcessQueue: false,
    uploadMultiple: true,
    parallelUploads: 5,
    // maxFiles: 5,
    // maxFilesize: 1,
    // acceptedFiles: 'image/*',
    addRemoveLinks: true,
    maxFilesize: 10, //10 mb is here the max file upload size constraint
    acceptedFiles: ".pdf,.png,.PNG,.jpg,.JPG,.JPEG,.jpeg,.txt,.doc,.docx,.xls,.xlsx,.mp4,.pptx,.txt,.ppt,.WEBM,.MPG,.MP2,.MPEG,.MPE,.MPV,.OGG,.MP4,.M4P,.M4V,.AVI,.WMV,.MOV,.QT,.FLV,.SWF,.AVCHD",
    init: function() {
      dzClosure = this; // Makes sure that 'this' is understood inside the functions below.

      // for Dropzone to process the queue (instead of default form behavior):
      document.getElementById("note_form_submit_btn").addEventListener("click", function(e) {

        if ($('#note_form').valid()) {

            $('.jsClassNoteSavedSuccess').removeClass('hide');
          // Make sure that the form isn't actually being sent.
          e.preventDefault();
          // e.stopPropagation();

          if (dzClosure.files.length) {
            dzClosure.processQueue(); // upload files and submit the form
          } else {
            // $('#note_form').submit(); // just submit the form
            removeErroClass();
            var summernoteCode = $('#summernote').summernote('code');
            $("#description").val(summernoteCode);

            $('#note_form').ajaxForm(noteOptions).submit();
          }
          // dzClosure.processQueue();
        }
      });

      //send all the form data along with the files:
      this.on("sendingmultiple", function(data, xhr, formData) {
        formData.append("title", jQuery("#title").val());
        formData.append("categories", jQuery("#categories").val());
        // formData.append("categories", jQuery("#categories").val().split(","));
        formData.append("language_id", jQuery("#language_id").val());
        formData.append("note_type", $('input[name="note_type"]:checked').val());

        var summernoteCode = $('#summernote').summernote('code');
        formData.append("description", summernoteCode);
        formData.append("is_video_note", $('input[name="is_video_note"]:checked').val());

        formData.append("cover", $('#browse_file')[0].files[0]);
      });

      this.on('complete', function(data) {
        // console.log("data", data);
        if (!data.status || data.status == 'error') {
            $('.jsClassNoteSavedSuccess').addClass('hide');
          if (data.errors)
            alert(data.errors);
          else
            alert("Please check selected files. Some of the file is not valid or it's more than 10MB.");
        }
        else {
          noteSuccessMessage()
          // window.location.href = '{{ route("web.note.my-notes") }}';
        }
      })
    },
    success: function(file, response) {
    //   console.log("success file", file);
    //   console.log("success response", response);
      // var imgName = response;
      // file.previewElement.classList.add("dz-success");
      // console.log("Successfully uploaded :" + imgName);
    },
    error: function(file, response) {
      // file.previewElement.classList.add("dz-error");
    //   console.log("error file", file);
      console.log("error response", response);
      $('.jsClassNoteSavedSuccess').addClass('hide');
      $(file.previewElement).remove();
    }
  }

  $('body').on("click", '.n-js-delete-attachment', function(e) {
    var attachment_id = $(this).attr('data-id');
    _deleteAttachment(attachment_id);
  });

  function _deleteAttachment(attachment_id) {

    swal({
        title: "Are you sure you want to delete?",
        text: "You will not be able to recover this file!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {

          $.ajax({
            method: "POST",
            url: "{{ route('web.note.delete-attachment') }}",
            data: {
              _token: "{{ csrf_token() }}",
              attachment_id: attachment_id
            },
            cache: false,
            success: function(response) {
              $("#li-attachment-" + attachment_id).remove();
            },
            error: function(error) {
              console.log('Error on delete attachment');
            }
          });
          return false;

        } else {
          swal("Your file is safe!");
        }
      });
  }

  $(document).ready(function() {

    var validateForm = $('#note_form').validate({
      // ignore: ":hidden, [contenteditable='true']:not([name])",
      errorElement: "div",
      errorClass: 'is-invalid',
      validClass: 'is-valid',
      ignore: ':hidden:not(.summernote),.note-editable.card-block',
      errorPlacement: function(error, element) {
        // Add the `help-block` class to the error element
        error.addClass("invalid-feedback");
        // console.log(element);
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.siblings("label"));
        } else if (element.hasClass("summernote")) {
          error.insertAfter(element.siblings(".note-editor"));
        } else if (element.hasClass('js-example-basic-multiple')) {
          error.insertAfter(element.siblings(".select2-container"));
        } else if (element.attr('id') == 'browse_file') {
          error.insertAfter(element.closest('.thumbnail-wrapper'));
        } else {
          error.insertAfter(element);
        }
      },
      onfocusout: false,
        invalidHandler: function(form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
      rules: {
        title: {
          required: true
        },
        'categories[]': {
          required: true
        },
        description: {
          required: true
        },
        cover: {
          // required: true,
          // required: function(element) {
          //   return !$("#noteId").val();
          // },
          // required: function(element){
          //     return '{{ empty($nNoteId) }}';
          // },
          extension: "pdf|png|PNG|jpg|JPG|JPEG|jpeg|txt|doc|docx|xls|xlsx|mp4|pptx|txt|ppt|WEBM|MPG|MP2|MPEG|MPE|MPV|OGG|MP4|M4P|M4V|AVI|WMV|MOV|QT|FLV|SWF|AVCHD"
        }
      },
      messages: {
        title: {
          required: "Enter title"
        },
        'categories[]': {
          required: "Select category"
        },
        description: {
          required: "Enter description"
        },
        cover: {
          required: "Select cover file",
          extension: "Select valid file"
        }
      }
    });

    //select2 plugin initialization for category autot suggestion/ multiple selection
    $('.js-example-basic-multiple').select2({
        templateResult: formatState,
        templateSelection: formatState
    });

    function formatState (opt) {
        if (!opt.id) {
            return opt.text.toUpperCase();
        } 

        var optimage = $(opt.element).attr('data-image'); 
        if(!optimage){
            return opt.text.toUpperCase();
        } else {                    
            var $opt = $('<span><img src="' + optimage + '" width="30px" height="20px" /> ' + opt.text.toUpperCase() + '</span>');
            return $opt;
        }
    };
    /* Summernote Validation */
    var summernoteElement = $('.summernote');

    summernoteElement.summernote({
      height: 300,
      callbacks: {
        onChange: function(contents, $editable) {
          // Note that at this point, the value of the `textarea` is not the same as the one
          // you entered into the summernote editor, so you have to set it yourself to make
          // the validation consistent and in sync with the value.
          summernoteElement.val(summernoteElement.summernote('isEmpty') ? "" : contents);

          // You should re-validate your element after change, because the plugin will have
          // no way to know that the value of your `textarea` has been changed if the change
          // was done programmatically.
          validateForm.element(summernoteElement);
        }
      }
    });

    //summernote plugin initialization => for note description
    $('.summernote').summernote({
      minHeight: 200,
      placeholder: 'Write here ...',
      disableDragAndDrop: true,
      toolbar: [
        // [groupName, [list of button]]
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link']],
        ['view', ['fullscreen', 'codeview', ]],
      ],
    });


    //Bootstrap plugin initialization => for Note file attachments

    //     $("#input-pr-rev").fileinput({
    //     uploadUrl: "/file-upload-batch/1",
    //     showUpload: false,
    //     theme: 'explorer-fas',
    //     uploadAsync: true,
    //     reversePreviewOrder: true,
    //     initialPreviewAsData: true,
    //     overwriteInitial: false,
    //     maxTotalFileCount: 8,
    //     initialPreview: [],
    //     initialPreviewConfig: [],
    //     // allowedFileExtensions: ["pdf","xls","xlsx","doc","JPG","jpeg","jpg","PNG","png","gif","svg","docx"]
    //     allowedFileExtensions: ["pdf","png","PNG","jpg","JPG","JPEG","jpeg","txt","doc","docx","xls","xlsx","mp4","pptx","txt","ppt","WEBM","MPG","MP2","MPEG","MPE","MPV","OGG","MP4","M4P","M4V","AVI","WMV","MOV","QT","FLV","SWF","AVCHD"]
    //   });

  });

  function noteSuccessMessage() {
    var html = '<div class="text-center py-3 jsClassNoteSavedSuccess"><div class="circle-loader"> <div class="checkmark draw"></div></div></div>';
    $(".jsClassReplaceData").html(html);

    setTimeout(function() {
      $('.circle-loader').toggleClass('load-complete');
      // $('.circle-loader').addClass('hide');
      $('.checkmark').toggle();
      var htmlNewNote = '<div class="text-center note-saved-success">Note Saved ! <div> <a href="{{ route('web.note.my-notes') }}" class="mx-auto my-2 nc-black-btn nc-save-btn  rounded px-3 d-inline-block">View Note</a> <a href="{{ route('web.home.index') }}" class="my-3 px-3 d-block">Explore other Notes</a></div></div>'
      $('.note-saved-success').length == 0 && $('.jsClassNoteSavedSuccess').append(htmlNewNote);
    }, 3000);

  }

  /**Submit note form*/
  $.ajaxSetup({
    headers: {
      'X-CSRF-Token': $('meta[name=_token]').attr('content')
    }
  });

  var noteOptions = {
    beforeSubmit: showNoteRequest,
    success: showNoteResponse,
    error: showNoteError,
    dataType: 'json'
  };

  function showNoteRequest() {
    $("#validation-errors").hide().empty();
    return true;
  }

  function showNoteResponse(response) {
    if (response.status == 0) {
      // showErrorToast("Please fill all the information to add note!");

      $.each(response.errors, function(key, val) {
        $('#' + key).addClass('error');

        showErrorToast(val);
        return false;

        // console.log(key);
        if (key == "files.0") {
          $('.file-caption-main').addClass('error');
        }
      });

      return false;
    }
    scrollToTop();
    noteSuccessMessage(); // show success message

    return false;

    var sHtml = '';
    sHtml += '<div class="alert alert-success">'
    sHtml += '<button type="button" class="close" data-dismiss="alert">x</button>'
    sHtml += '<strong>Success!</strong>'
    sHtml += ' ' + response.message + ' '
    sHtml += '</div>'

    $('#edit-profile-alert').html(sHtml);

  }

  function showNoteError(xhr) {
    console.log("inside error");
  }

  //   $('body').on('click','#note_form_submit_btn',function(){
  //     // showAjaxLoader('body');
  //     // return false;
  //     removeErroClass();
  //     var summernoteCode = $('#summernote').summernote('code');
  //     $("#description").val(summernoteCode);

  //     $('#note_form').ajaxForm(noteOptions).submit();
  //   });

  function removeErroClass() {
    $('.file-caption-main').removeClass('error');
    $('#title').removeClass('error');
  }

  /**Submit note form*/
  //=====  Pic Thumbnail Preview and Remove Functonality  Used in Add note & Profile Page START =====
  document.getElementById("browse_file").onchange = function() {
    // console.log("onChange Call");
    var reader = new FileReader();

    reader.onload = function(e) {
      // get loaded data and render thumbnail.
      document.getElementById("preview_image").src = e.target.result;
    };

    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
    $(".jsClassPlaceHolderImg").removeClass("d-none");
    $(".jsClassPlaceHolderImg").addClass("d-none");
    $(".jsClassRemoveImg").removeClass("d-none");
    $("#preview_image").removeClass("d-none");
  };

  function removeAttachedFile() {
    $("#preview_image").removeAttr('src');
    $("#preview_image").addClass('d-none');
    $(".jsClassPlaceHolderImg").removeClass("d-none");
    $(".jsClassRemoveImg").addClass("d-none");
    $("#browse_file").attr('val', '');
  }
  //=====  Pic Thumbnail Preview and Remove Functonality  Used in Add note & Profile Page END =====
</script>

@endsection
