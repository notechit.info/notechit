<?php
namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Bucket;
use App\Models\User;
use Socialite;
use Exception;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Auth;

class FacebookController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToFacebook() {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleFacebookCallback(Request $request) {
        try {

            $user = Socialite::driver('facebook')->user();

            $name = explode($user->getName(), ' ');
            $create['first_name'] = $name[0];
            $create['last_name'] = isset($name[1]) ? $name[1] : '';
            $create['email'] = $user->getEmail();
            $create['username'] = $user->getEmail();
            $create['file_name'] = $user->getAvatar();
            $create['login_with'] = 'FB';
            $create['login_id'] = $user->getId();
            $create['login_count'] = 1;

            $currentUsers = User::where('email', $create['email'])->first();

            if(empty($currentUsers)) {
                $currentUsers = User::create($create);
                Bucket::defaultBucket($currentUsers->id);

                Session::put('isRegistered', true);
            }
            else if($currentUsers->login_with != 'FB' ) {
                $currentUsers->update([
                    'login_with' => 'FB',
                    'login_id' => $create['login_id'],
                    'login_count' => ($currentUsers->login_count + 1)
                ]);
            }
            else {
                $currentUsers->login_count = ++$currentUsers->login_count;
                $currentUsers->save();
            }

            Auth::login(User::find($currentUsers->id));

            return redirect()
                ->route('web.home.index');
        }
        catch (Exception $e) {
            return redirect()
                ->route('web.home.index')
                ->with('error', trans('messages.error_message'));
        }
    }
}
