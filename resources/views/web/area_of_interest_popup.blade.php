<div class="modal fade interest-modal" id="interestModal" tabindex="-1" role="dialog" aria-labelledby="interestModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg md-modal" role="document">
      <div class="modal-content">
        <div class="modal-body modal-body-padding">
          <button type="button" class="popup-close-btn close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <div class="col-12 text-center">
            <div class="nc-xlg-text modal-title text-center">@lang('messages.choose_your_interests')</div>
            <div class="nc-sm-text"> @lang('messages.interest_info')</div>
            <input id="filter" type="text" class="animate-search" name="search" placeholder="@lang('messages.search_categories')">

            <div class="category-list-wrapper">
              <ul class="cat-checkbox-wrapper text-center">

                @foreach($oCategories as $nIndex => $oCategory)
                <li clas="">
                  <input type="checkbox" name="user_area_of_interest" value="{{ $oCategory->id }}" id="myCheckbox{{ $nIndex + 1 }}" />
                  <label for="myCheckbox{{ $nIndex + 1 }}">
                    <img class="" src="{{getCategoryImageUrl($oCategory->image)}}" alt="{{ $oCategory->category_name }}">
                    <div class="">{{ $oCategory->category_name }}</div> 
                  </label>
                </li>
                @endforeach

                <!--<li class="">
                  <input type="checkbox" id="myCheckbox2" />
                  <label for="myCheckbox2">
                    <img class="" src="{{asset('web/img/default-category.svg')}}" alt="category">
                    <div class="">Physics</div> 
                  </label>
                </li>
                <li class="">
                  <input type="checkbox" id="myCheckbox3" />
                  <label for="myCheckbox3">
                    <img class="" src="{{asset('web/img/default-category.svg')}}" alt="category">
                    <div class="">Biology</div> 
                  </label>
                </li>-->

              </ul>
            </div>

            <button class=" nc-black-btn nc-register-btn" id="nc-js-user-interest-btn">Save</button>
            <div class="nc-xs-text cursor-pointor text-center my-2" data-dismiss="modal" onClick="window.location.reload();">Skip</div>

          </div>
        </div>
      </div>
    </div>
</div>

<script>
  $("body").on("click","#nc-js-user-interest-btn",function(){
      var aCategories = [];
      $("input:checkbox[name=user_area_of_interest]:checked").each(function(){
          aCategories.push($(this).val());
      });

      if(aCategories.length === 0){
        showToast("Please select category!");
        return false;
      }

      $.ajax({
          method : "GET",
          url: "{{ route('web.user.add-area-of-interest') }}",
          data : { 
            aCategories : aCategories
          },
          cache: false,
          success: function(response){
              if(response.status == 1)
              {
                  //showToast(response.message);
                  $('#interestModal').modal('toggle');
                  window.location.reload();
              }
          },error : function(error){ 
            console.log('Area of interest AJAX Error');
            setTimeout(() => {
              window.localtion.reload();
            }, 1000);
          }
      });
  });//End of click

  $(document).ready(function(){
    $("#filter").keyup(function(){

        // Retrieve the input field text and reset the count to zero
        var filter = $(this).val().trim(), count = 0;
        // if(!filter){ // hide is no text
        //     $(".cat-checkbox-wrapper li").hide();
        //     return;
        // }

        var regex = new RegExp(filter, "i"); // Create a regex variable outside the loop statement

        // Loop through the comment list
        $(".cat-checkbox-wrapper li").each(function(){

            // If the list item does not contain the text phrase fade it out
            if ($(this).text().search(regex) < 0) { // use the variable here
                $(this).hide();

            // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).show();
                count++;
            }
        });
    });
  });

</script>