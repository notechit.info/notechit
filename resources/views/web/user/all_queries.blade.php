@extends('web.main')

@section('title')
    {{ trans('messages.home_page') }}
@endsection

@section('content')

<div class="container notes-wrapper">
    @include('web.user._contribution_nav_tab')
    <div class="all-queries-wrapper">
        @guest
            <!-- ===== message for not logged in user Start===== -->
            <div class="row ">
                <div class="col no-data-wrapper py-5 ">
                <div class="info-text font-weight-bold  py-1">@lang('messages.help_others_by_adding_notes')</div>
                <a href="#" data-toggle="modal" data-target="#loginModal" class="cta-btn">Login to Notechit</a>
                </div>
            </div>
            <!-- ===== message for not logged in user End===== -->
        @endguest
        @auth
            @if($oQueries && $oQueries->count())
                <div class="row intro-text">
                    <div class="col text-center">
                        <img class="img-fluid" src="{{asset('web/img/plane.svg')}}" alt="plane">
                        @lang('messages.can_you_help')
                    </div>
                </div>
                @foreach($oQueries as $key=>$oQuery)
                    <div class="row single-query-wrapper">
                        <div class="col-10">
                            <span class="author-thumbnail-wrapper ">
                                @if($oQuery->user->file_name == "" && $oQuery->user->profile_pic_url == '')
                                    <img class="img-fluid" src="{{asset('web/img/author.png')}}" alt="Profile image {{$oQuery->user->full_name}}">
                                @else 
                                    <img class="img-fluid" src="{{ getUserImageUrl($oQuery->user->file_name,$oQuery->user->profile_pic_url) }}" alt="Profile image {{$oQuery->user->full_name}}">
                                @endif
                            </span>
                            <span class="d-inline-block author-name nc-sm-text">
                                
                            <a href="{{ route('web.user.profile',['id' => $oQuery->user->id]) }}">
                                {{ $oQuery->user->first_name.' '.$oQuery->user->last_name  }}
                            </a>
                                <div class=" note-date">{{ $oQuery->created_at }}</div>
                            </span>
                            <div class="request-content">
                                {{ $oQuery->query_text }}
                            </div>
                            @if($oQuery->categories)
                            <div class="request-category">
                                @foreach($oQuery->categories as $category)
                                <span class="note-tag nc-tag-bg-2">{{$category->category_name}}</span>
                                @endforeach
                                {{--<span class="note-tag nc-tag-bg-3">Science</span>--}}
                            </div>
                            @endif
                        </div>
                        <div class="col-2">
                            <div class="float-right">
                                @if($oQuery->user_id == Auth::user()->id)
                                    <a href="{{ route('web.home.delete-query', $oQuery->id) }}" class="nc-black-btn nc-radius nc-sm-text">
                                        <i class="fa fa-times nc-xs-text" aria-hidden="true"></i>
                                        @lang('messages.remove')
                                    </a>
                                    <a href="#" class="nc-black-btn nc-radius nc-sm-text">
                                        <i class="fa fa-exclamation-triangle nc-xs-text" aria-hidden="true"></i>
                                        @lang('messages.not_requested')
                                    </a>
                                @else
                                    <a href="{{ route('web.note.add-note') }}" class="nc-black-btn nc-radius nc-sm-text">
                                        <i class="fa fa-plus nc-xs-text" aria-hidden="true"></i>
                                        @lang('messages.add_note')
                                    </a>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row" id="suggestionSection{{$oQuery->id}}">
                                @include('web.user.contribution_note_suggestions')
                                @include('web.user.contribution_note_suggesttion_comments')
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <!-- ===== No data message for all queries Start===== -->
                <div class="row ">
                    <div class=" col no-data-wrapper py-5">
                    <div class="info-text py-1">@lang('messages.no_requests_found')</div>
                    </div>
                </div>
                <!-- ===== No data message for all queries End===== -->
            @endif
        @endauth
    </div>
</div>
@endsection

@section('custom-footer-js')
<script>
    $(document).on('keyup keydown', '.suggestion-input', function(){
        dataId = $(this).data('query-id');
        userInput = $('#suggestionInput'+dataId).val();
        $('#suggestionInputError'+dataId).text('');
        errorText = '';
        if(userInput == ""){
            errorText = "Please add note URL.";
        }else if(validURL(userInput) == false){
            errorText = "Please enter valid URL.";
        }else if(checkDomain(userInput) == false){
            errorText = "Please enter Notechit URL.";
        }

        if(errorText != ""){
            $('#suggestionInputError'+dataId).text(errorText);
        }
    });

    $("body").on('click','.suggestionButton',function(){
        dataId = $(this).data('query-id');
        userInput = $('#suggestionInput'+dataId).val();
        $('#suggestionInputError'+dataId).text('');
        errorText = '';
        if(userInput == ""){
            errorText = "Please add note URL.";
        }else if(validURL(userInput) == false){
            errorText = "Please enter valid URL.";
        }else if(checkDomain(userInput) == false){
            errorText = "Please enter Notechit URL.";
        }

        if(errorText == ""){
            $.ajax({
                type:'POST',
                url: '{{route("web.contribution-query-comment-add")}}',
                data: {
                    "_token"    : "{{csrf_token()}}",
                    "id"        : dataId,
                    "userInput" : userInput,
                },
                success: (data) => {
                    showToast(data.message);
                    $('#suggestionInput'+dataId).val('');
                    refreshComments(dataId);
                },
                error: function(data){
                    showErrorToast(data);
                }
            });
        }else{
            $('#suggestionInputError'+dataId).text(errorText);
        }
    });

    function checkDomain(userInput){
        url = new URL('', userInput);
        domain = url.hostname;
        if(domain === "notechit.com" || domain === "www.notechit.com" || domain == "127.0.0.1"){
            return true;
        }else{
            return false;
        }
    }

    function validURL(str) {
        var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
            '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
        return !!pattern.test(str);
    }

    $(document).on('click', '.rm-contribution-comment', function(){
        dataId = $(this).data('id');
        dataQuery = $(this).data('query');
        $.ajax({
            type:'POST',
            url: '{{route("web.contribution-query-comment-remove")}}',
            data: {
                "_token"    : "{{csrf_token()}}",
                "id"        : dataId,
            },
            success: (data) => {
                if(data.status == 1){
                    $('#CommentSingle'+dataId).remove();
                    showToast(data.message);
                    refreshComments(dataQuery);
                }else{
                    showErrorToast(data.message);
                }
            },
            error: function(data){
                showErrorToast(data);
            }
        });
    });

    function refreshComments(dataId){
        $.ajax({
            type:'GET',
            url: '{{route("web.get-query-suggestion-comments")}}',
            data: {
                "id"        : dataId,
            },
            success: (data) => {
                if(data.status == 1){
                    $('#suggestionComments'+dataId).remove();
                    $('#suggestionSection'+dataId).append(data.content);
                }
            },
            error: function(data){
                showErrorToast(data);
            }
        });
    }
</script>
@endsection