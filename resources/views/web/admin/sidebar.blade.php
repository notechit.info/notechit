<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('admin.home')}}" class="brand-link">
    <img src="{{asset('web/img/logo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">{{ trans("messages.app_name") }}</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{route('admin.home')}}" class="nav-link {{ Route::current()->getName() == "admin.home" ? 'active' : '' }}">
                        <i class="nav-icon fas fa-chart-bar"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.users')}}" class="nav-link {{ Route::current()->getName() == "admin.users" ? 'active' : '' }}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Users
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.notes')}}" class="nav-link  {{ Route::current()->getName() == "admin.notes" ? 'active' : '' }}">
                        <i class="nav-icon fas fa-sticky-note"></i>
                        <p>
                            Notes
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.reported.notes')}}" class="nav-link {{ Route::current()->getName() == "admin.reported.notes" ? 'active' : '' }}">
                        <i class="nav-icon fas fa-sticky-note"></i>
                        <p>
                            Reported Notes
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.category')}}" class="nav-link  {{ Route::current()->getName() == "admin.category" ? 'active' : '' }}">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Category
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.language.index')}}" class="nav-link  {{ Route::current()->getName() == "admin.language.index" ? 'active' : '' }}">
                        <i class="nav-icon fas fa-language"></i>
                        <p>
                            Language
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>

<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>