<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    const PVT = 'PVT', PUB = 'PUB', FO = 'FO';
    const ACTIVE = 1, IN_ACTIVE = 0;
    const YES = 1, NO = 0;

    protected $fillable = [
        'title',
        'description',
        'file_name',
        'display_file_name',
        'user_id',
        'language_id',
        'note_type',
        'is_video_note',
        'activated',
        'deleted'
    ];

    public function scopeActive($query)
    {
        return $query->where('notes.activated',self::ACTIVE)
                    ->where('notes.deleted',self::IN_ACTIVE);
    }

    public function attachments(){
        return $this->hasMany(Attachment::class);
    }

    public function bucketNotes(){
        return $this->hasMany(BucketNotes::class);
    }

    public function favourites(){
        return $this->hasMany(Favourite::class);
    }

    public function categories(){
        return $this->hasMany(NoteCategory::class);
    }

    public function noteDownloads(){
        return $this->hasMany(NoteCategory::class);
    }

    public function noteReports(){
        return $this->hasMany(NoteReport::class);
    }

    public function noteViews(){
        return $this->hasMany(NoteView::class);
    }

    public function ratings(){
        return $this->hasMany(Rating::class);
    }

    public static function getNotes($sNoteType = Note::PUB, $bIsPaginate = false, $searchData = '')
    {
        $nLoggedInUserId = \Auth::user() ? \Auth::user()->id : null;

        $query = Note::from('notes as n')
                ->leftJoin('users as u','n.user_id','=','u.id')
                ->leftJoin('bucket_notes as bn', function($join) use($nLoggedInUserId) {
                    $join->on('bn.note_id', '=', 'n.id')
                        ->where('bn.user_id','=',$nLoggedInUserId)
                        ->where('bn.deleted', 0);
                })
                ->leftJoin('buckets as b','b.id','=','bn.bucket_id')
                ->where('n.activated',1)
                ->where('n.deleted',0)
                ->where('n.note_type',$sNoteType)
                ->where(function($query) use ($searchData) {
                    if(!empty($searchData)) {
                        $query->where('n.title', 'LIKE', '%' . $searchData . '%');
                    }
                })
                ->select(
                    'b.bucket_name as bucket_name',
                    'n.id as note_id',
                    'n.user_id as user_id',
                    'n.title as title',
                    'n.is_video_note as is_video_note',
                    'n.created_at as created_at',
                    'n.file_name as file_name',
                    'u.first_name as first_name',
                    'u.last_name as last_name',
                    'u.file_name as user_file_name',
                    'u.profile_pic_url as user_profile_pic_url',
                    'bn.id as bn_id',
                    'bn.deleted as bn_deleted',
                    \DB::raw('(SELECT count(*) as note_id FROM note_views AS nv WHERE nv.note_id = n.id AND activated = 1 AND deleted = 0) as total_views')
                )
                ->orderBy('n.id','DESC');
        if($bIsPaginate)
            return $query->paginate(config("constants.WEB_PER_PAGE_RECORDS"));

        return $query->get();
    }

    public static function getMyNotes($nUserId,$bIsPaginate = false,$bReturnOnlyPublicNote = false)
    {
        $nLoggedInUserId = \Auth::user() ? \Auth::user()->id : null;

        $query = Note::from('notes as n')
                ->leftJoin('users as u','n.user_id','=','u.id')
                ->leftJoin('bucket_notes as bn', function($join) use($nLoggedInUserId) {
                    $join->on('bn.note_id', '=', 'n.id')
                        ->where('bn.user_id','=',$nLoggedInUserId)
                        ->where('bn.deleted', 0);
                })
                ->leftJoin('buckets as b','b.id','=','bn.bucket_id')
                ->where('n.activated',1)
                ->where('n.deleted',0)
                ->where('n.user_id',$nUserId)
                ->when($bReturnOnlyPublicNote == true,function($query){
                    return $query->where('n.note_type',Note::PUB);
                })
                ->select(
                    'b.bucket_name as bucket_name',
                    'n.id as note_id',
                    'n.user_id as user_id',
                    'n.title as title',
                    'n.is_video_note as is_video_note',
                    'n.created_at as created_at',
                    'n.file_name as file_name',
                    'n.note_type as note_type',
                    'u.first_name as first_name',
                    'u.last_name as last_name',
                    'u.file_name as user_file_name',
                    'u.profile_pic_url as user_profile_pic_url',
                    'bn.id as bn_id',
                    'bn.deleted as bn_deleted',
                    \DB::raw('(SELECT count(*) as note_id FROM note_views AS nv WHERE nv.note_id = n.id AND activated = 1 AND deleted = 0) as total_views')
                )
                ->orderBy('n.id','DESC');

        if($bIsPaginate)
            return $query->paginate(config("constants.WEB_PER_PAGE_RECORDS"));

        return $query->get();
    }

    /**
     * Get saved notes
     */
    public static function getSavedNotes($aNoteIds = array(),$bIsPaginate = false)
    {
        $nLoggedInUserId = \Auth::user() ? \Auth::user()->id : null;

        $query = Note::from('notes as n')
            ->leftJoin('users as u','n.user_id','=','u.id')

            ->leftJoin('bucket_notes as bn', function($join) use($nLoggedInUserId){
                $join->on('bn.note_id', '=', 'n.id')
                    ->where('bn.user_id','=',$nLoggedInUserId)
                    ->where('bn.deleted', 0);
            })
            ->leftJoin('buckets as b','b.id','=','bn.bucket_id')
            ->where('n.activated',1)
            ->where('n.deleted',0)
            ->whereIn('n.id',$aNoteIds)
            ->select(
                'b.bucket_name as bucket_name',
                'n.id as note_id',
                'n.user_id as user_id',
                'n.title as title',
                'n.is_video_note as is_video_note',
                'n.created_at as created_at',
                'n.file_name as file_name',
                'u.first_name as first_name',
                'u.last_name as last_name',
                'u.file_name as user_file_name',
                'u.profile_pic_url as user_profile_pic_url',
                'bn.id as bn_id',
                'bn.deleted as bn_deleted',
                \DB::raw('(SELECT count(*) as note_id FROM note_views AS nv WHERE nv.note_id = n.id AND activated = 1 AND deleted = 0) as total_views')
            )
            ->orderBy('n.id','DESC');

        if($bIsPaginate)
            return $query->paginate(config("constants.WEB_PER_PAGE_RECORDS"));

        return $query->get();
    }

    /**
     * Follower users notes
     */
    public static function getFollowerUsersNotes($aUserIds = array(),$bIsPaginate = false)
    {
        $nLoggedInUserId = \Auth::user() ? \Auth::user()->id : null;

        $query = Note::from('notes as n')
            ->leftJoin('users as u','n.user_id','=','u.id')
            ->leftJoin('bucket_notes as bn', function($join) use($nLoggedInUserId){
                $join->on('bn.note_id', '=', 'n.id')
                    ->where('bn.user_id','=',$nLoggedInUserId)
                    ->where('bn.deleted', 0);
            })
            ->leftJoin('buckets as b','b.id','=','bn.bucket_id')
            ->where('n.activated',1)
            ->where('n.deleted',0)
            ->where('n.note_type',Note::FO)
            ->whereIn('n.user_id',$aUserIds)
            ->select(
                'b.bucket_name as bucket_name',
                'n.id as note_id',
                'n.user_id as user_id',
                'n.title as title',
                'n.is_video_note as is_video_note',
                'n.created_at as created_at',
                'n.file_name as file_name',
                'u.first_name as first_name',
                'u.last_name as last_name',
                'u.file_name as user_file_name',
                'u.profile_pic_url as user_profile_pic_url',
                'bn.id as bn_id',
                'bn.deleted as bn_deleted',
                \DB::raw('(SELECT count(*) as note_id FROM note_views AS nv WHERE nv.note_id = n.id AND activated = 1 AND deleted = 0) as total_views')
            )
            ->orderBy('n.id','DESC');

        if($bIsPaginate)
            return $query->paginate(config("constants.WEB_PER_PAGE_RECORDS"));

        return $query->get();
    }

    public static function getNoteDetail($nNoteId)
    {
        $nLoggedInUserId = \Auth::user() ? \Auth::user()->id : null;

        return Note::from('notes as n')
            ->leftJoin('users as u','n.user_id','=','u.id')
            ->leftJoin('bucket_notes as bn', function($join) use($nLoggedInUserId){
                $join->on('bn.note_id', '=', 'n.id')
                    ->where('bn.user_id','=',$nLoggedInUserId)
                    ->where('bn.deleted', 0);
            })
            ->leftJoin('buckets as b','b.id','=','bn.bucket_id')
            ->where('n.activated',1)
            ->where('n.deleted',0)
            ->where('n.id',$nNoteId)
            ->select(
                'b.bucket_name as bucket_name',
                'n.id as id',
                'n.user_id as user_id',
                'n.title as title',
                'n.is_video_note as is_video_note',
                'n.description as description',
                'n.note_type as note_type',
                'n.created_at as created_at',
                'n.file_name as file_name',
                'u.first_name as first_name',
                'u.last_name as last_name',
                'u.file_name as user_file_name',
                'u.profile_pic_url as user_profile_pic_url',
                'bn.id as bn_id',
                'bn.deleted as bn_deleted',
                \DB::raw('(SELECT count(*) as note_id FROM note_views AS nv WHERE nv.note_id = n.id AND activated = 1 AND deleted = 0) as total_views'),
                \DB::raw('(SELECT count(*) as note_id FROM note_downloads AS nd WHERE nd.note_id = n.id AND activated = 1 AND deleted = 0) as total_downloads')
            )->first();
    }

    public static function searchNotes($sKeyWord = '')
    {
        return Note::from('notes as n')
            ->where('n.activated',1)
            ->where('n.deleted',0)
            ->where('n.note_type',self::PUB)
            ->where("n.title","LIKE", "%" . $sKeyWord . "%")
            ->leftJoin('users as u','n.user_id','=','u.id')
            ->select(
                'n.id as note_id',
                'n.title as title',
                'n.file_name',
                'n.is_video_note',
                \DB::raw('CONCAT(u.first_name, " ", u.last_name) AS full_name'),
            )
            ->get();
    }

    public static function getBucketNotes($aNoteIds,$bIsPaginate = false)
    {
        $query = Note::from('notes as n')
            ->leftJoin('users as u','n.user_id','=','u.id')
            ->where('n.activated',1)
            ->where('n.deleted',0)
            ->whereIn('n.id',$aNoteIds)
            ->select(
                'n.id as note_id',
                'n.user_id as user_id',
                'n.title as title',
                'n.is_video_note as is_video_note',
                'n.created_at as created_at',
                'n.file_name as file_name',
                'u.first_name as first_name',
                'u.last_name as last_name',
                'u.file_name as user_file_name',
                'u.profile_pic_url as user_profile_pic_url',
                \DB::raw('(SELECT count(*) as note_id FROM note_views AS nv WHERE nv.note_id = n.id AND activated = 1 AND deleted = 0) as total_views')
            )
            ->orderBy('n.id','DESC');

        if($bIsPaginate)
            return $query->paginate(config("constants.WEB_PER_PAGE_RECORDS"));

        return $query->get();
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
