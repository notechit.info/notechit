<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NoteReport extends Model
{
    const IN_ACTIVE = 0 , ACTIVE = 1;

    protected $fillable = [
        'note_id',
        'user_id',
        'problem',
        'reason',
        'activated',
        'deleted'
    ];

    public function scopeActive($query)
    {
        return $query->where('note_reports.activated',self::ACTIVE);
    }

    public function note(){
        return $this->belongsTo(Note::class);
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
