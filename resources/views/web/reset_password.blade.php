@extends('web.includes_layout')

@section('title')
    {{ trans('messages.home_page') }}
@endsection
@section('content')

<style>
.help-block
{
  color:red;
}
</style>

<div class="container-fluid">
  <div class="row justify-content-md-center">
      <div class="col-4 text-center">

        <a href="{{ route('web.home.index') }}">
          <img src="{{asset('web/img/logo.png')}}" width="30" height="30" class="d-inline-block align-top" alt="">
        </a>

        <div class="nc-xlg-text modal-title text-center">@lang('messages.reset_password')</div>

        @if(Session::has('error'))
            <div class="alert bg-danger alert-styled-right">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                <span class="text-semibold">Oh snap!</span> {{ Session::get('error') }}</a>.
            </div>
        @endif

        @if(Session::has('success'))
        <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold">Success!</span> {{ Session::get('success') }}.
        </div>
        @endif

        <form id="signin_form" action="{{ route('web.user.reset-password',['key' => $key]) }}" method="POST">
                {{ csrf_field() }}
                
                <div class="form-group">
                    <input type="password" class="form-control" autocomplete="off" id="password" name="password" placeholder="@lang('messages.new_passowrd')">
                    @foreach($errors->get('password') as $error)
                        <span class="help-block">{{ $error }}</span>
                    @endforeach
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" autocomplete="off" id="confirm_password" name="confirm_password" placeholder="@lang('messages.retype_password')">
                    @foreach($errors->get('confirm_password') as $error)
                      <span class="help-block">{{ $error }}</span>
                    @endforeach
                </div>

                <button type="submit" id="" class=" nc-black-btn nc-register-btn">@lang('messages.reset')</button>
              </form>
      </div>
  </div>
</div>

@endsection
