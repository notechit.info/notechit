<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    const IN_ACTIVE = 0 , ACTIVE = 1;
    const PENDING = 'PENDING',ACCEPT = 'ACCEPT',DECLINE = 'DECLINE';
    const FF = 'FF', NOTE = 'NOTE';
    const FOLLOW = 'FOLLOW',UNFOLLOW ='UNFOLLOW',FOLLOWING = 'FOLLOWING',REJECT = 'REJECT';

    protected $fillable = [
        'user_id',
        'from_user_id',
        'type',
        'action',
        // 'ff_status',
        'entity_id',
        'attempt',
        'activated',
        'is_read',
        'deleted'
    ];

    public function scopeActive($query)
    {
        return $query->where('notifications.activated',self::ACTIVE);
    }

    public function youUser(){
        return $this->belongsTo(User::class,'from_user_id','id');
    }
}
