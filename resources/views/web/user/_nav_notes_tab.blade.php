  <div class="text-center ">
    
    <div class="notes-tabs-wrapper" id="nav-tab" role="tablist">
      <a class="{{ \Request::route()->getName() == 'web.note.my-notes' ? 'active' : '' }}" href="{{ route('web.note.my-notes') }}" role="tab" aria-selected="true">
        <span>My Notes</span>  
      </a>
      <a class="{{ \Request::route()->getName() == 'web.note.following-users-notes' ? 'active' : '' }}" href="{{ route('web.note.following-users-notes') }}" role="tab" aria-selected="false">
        <span> Shared With Me </span> 
      </a>

      <!-- <a class="{{ \Request::route()->getName() == 'web.note.saved-notes' ? 'active' : '' }}" href="{{ route('web.note.saved-notes') }}" role="tab" aria-selected="false">
        <span>Saved Notes</span> 
      </a> -->

      <a class="{{ \Request::route()->getName() == 'web.note.saved-notes' ? 'active' : '' }}" href="{{ route('web.note.saved-notes') }}" role="tab" aria-selected="false">
        <span>Saved Notes</span> 
      </a>

    </div>
   
  </div>