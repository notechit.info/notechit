  <div class="text-center ">
    
    <div class="notes-tabs-wrapper" id="nav-tab" role="tablist">
      <a class="{{ \Request::route()->getName() == 'web.home.all-queries' ? 'active' : '' }}" href="{{ route('web.home.all-queries') }}" role="tab" aria-selected="true">
        <span>All Queries</span>  
      </a>
      <a class="{{ \Request::route()->getName() == 'web.home.my-queries' ? 'active' : '' }}" href="{{ route('web.home.my-queries') }}" role="tab" aria-selected="false">
        <span> My Queries </span> 
      </a>
      <a class="{{ \Request::route()->getName() == 'web.home.new-query' ? 'active' : '' }}" href="{{ route('web.home.new-query') }}" role="tab" aria-selected="false">
        <span> Post new query </span> 
      </a>
     
    </div>
   
  </div>