@extends('web.main')

@section('title')
    {{ trans('messages.note_detail') }}
@endsection

@section('content')

<div class="container notes-wrapper">
  @if(count($oNotes) > 0)
    <div class="add-note-sticky-btn text-center">
      <a href="{{route('web.note.add-note')}}" class="d-inline-block">
          <i class="fa fa-plus nc-xs-text"></i>
          @lang('messages.add_chit')
      </a>
    </div>
  @endif
  
  @include('web.user._nav_notes_tab')
   
  <div class="row">

    @if(!Auth::user())
      <!-- ======= Display for users who have not logged in  START ===== -->
      <div class="col no-data-wrapper py-5">
        <div class="info-text font-weight-bold">
          @lang('messages.start_adding_notes')
        </div>
        <a href="#" data-toggle="modal" data-target="#loginModal" class="cta-btn">@lang('messages.login_to_notechit')</a> 
      </div>
      <!-- ======= Display for users who have not logged in  End ===== -->
    @else

      @if(count($oNotes) > 0)
        <?php $i = 1; ?>  
        @foreach($oNotes as $oNote)  
        <div class="col-lg-4 col-md-6 col-sm-12 note-card-bg-{{$i}}">
          <?php 
              if($i > 11){
                $i=1;
              }
              else{
                $i++; 
              }
            ?>  
            @include('web.notes.single_note_card')
          </div>
        @endforeach
      @else
        <div class="col no-data-wrapper py-5">
          <div class="info-text">@lang('messages.no_added_notes')</div>
          <a href="{{route('web.note.add-note')}}" class="cta-btn">@lang('messages.no_added_sub_notes')</a> 
        </div>
      @endif

    @endif

  </div>
    
</div>

@include('web.notes._bucket_modal')

@endsection