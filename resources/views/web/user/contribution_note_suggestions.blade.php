<div class="col-md-12" id="suggestionInputSection{{$oQuery->id}}">
    <input id="suggestionInput{{$oQuery->id}}" class="nc-top-searchbar suggestion-input" data-query-id="{{$oQuery->id}}" placeholder="Suggest Notechit Note">
    <button class="note-card-save-btn nc-black-btn suggestionButton" data-query-id="{{$oQuery->id}}" data-action="add">Save</button>
    <span id="suggestionInputError{{$oQuery->id}}" class="suggestion-error"></span>
</div>