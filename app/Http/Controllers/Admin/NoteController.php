<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use App\Models\Note;
use App\Models\NoteReport;
class NoteController extends Controller
{
    public function index(){
        return view('web.admin.notes');
    }

    public function datatable(Request $request){
        $data = Note::with('user:id,first_name,last_name')->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('author_name', function ($data) {
                return $data->user->first_name.' '.$data->user->last_name;
            })
            ->editColumn('is_video_note', function ($data) {
                return $data->video_note == 1 ? 'Video Note' : 'Text Note'; 
            })
            ->addColumn('action', function ($data) {
                $str = '';
                if($data->deleted == 0){
                    $str .= '<button class="btn btn-danger btn-sm mr-2 deleteNote" data-id="'.$data->id.'"><i class="fa-solid fas fa-trash"></i></button>';
                }

                return $str;
            })
            ->make(true);
    }

    public function delete(Request $request){
        $note = Note::find($request->id);
        if($note){
            $note->deleted = 1;
            $note->save();
            return response()->json('Note Deleted.', 200);
        }else{
            return response()->json('Note not found',404);
        }
    }

    public function reportedNotes(){
        return view('web.admin.reported_notes');
    }

    public function reportedNoteDatatable(){
        $data = Note::has('noteReports')->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('reports_count', function ($data) {
                return $data->noteReports->count();
            })
            ->editColumn('action', function ($data) {
                return '<button class="btn btn-info btn-sm mr-2 viewNoteReport" data-id="'.$data->id.'"><i class="fa-solid fas fa-eye"></i></button>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }
    public function getReportedNoteData(Request $request){
        $noteReports = NoteReport::with(['user:id,first_name,last_name,email'])
            ->where('note_id',$request->id)
            ->get();

        if($noteReports){
            return response()->json($noteReports, 200);
        }else{
            return response()->json('Note report not found.', 404);
        }
    }
}
