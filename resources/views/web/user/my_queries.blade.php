@extends('web.main')

@section('title')
    {{ trans('messages.home_page') }}
@endsection

@section('content')

<div class="container notes-wrapper">
    @include('web.user._contribution_nav_tab')
    <div class=" all-queries-wrapper">
        @guest
        <!-- ===== message for not logged in user Start===== -->
        <div class="row ">
            <div class="col no-data-wrapper py-5 ">
            <div class="info-text font-weight-bold  py-1">@lang('messages.ask_for_the_note')</div>
            <a href="#" data-toggle="modal" data-target="#loginModal" class="cta-btn">Login to Notechit</a>
            </div>
        </div>
        <!-- ===== message for not logged in user End===== -->
        @endguest
        @auth

        <!-- ===== No data message for all queries Start===== -->
        <div class="row ">

            <div class="col-12">
                @include('elements.comman.flash_message')
            </div>

            <div class=" col no-data-wrapper py-5">
            <div class="info-text py-1">@lang('messages.post_note_request')</div>
                <a href="{{ route('web.home.new-query') }}" class="cta-btn">Post Request</a>
            </div>
        </div>
        <!-- ===== No data message for all queries End===== -->

        @if($oQueries && $oQueries->count())
            @foreach($oQueries as $key=>$oQuery)
                <div class="row single-query-wrapper">
                    <div class="col-10">
                        <span class="author-thumbnail-wrapper ">
                            @if($oQuery->user->file_name == "" && $oQuery->user->profile_pic_url == '')
                                <img class="img-fluid" src="{{asset('web/img/author.png')}}" alt="Profile image {{$oQuery->user->full_name}}">
                            @else 
                                <img class="img-fluid" src="{{ getUserImageUrl($oQuery->user->file_name,$oQuery->user->profile_pic_url) }}" alt="Profile image {{$oQuery->user->full_name}}">
                            @endif
                        </span>
                        <span class="d-inline-block author-name nc-sm-text">
                            <!-- By You -->
                            <a href="{{ route('web.user.profile',['id' => $oQuery->user->id]) }}">
                                {{ $oQuery->user->first_name.' '.$oQuery->user->last_name  }}
                            </a>
                            <div class=" note-date"> {{ $oQuery->created_at }}</div>
                        </span>
                        <div class="request-content">
                            <div class="nc-vibrant-color nc-xs-text">( Your request will expire at {{ $oQuery->expiry_date->format('j F, Y') }} )</div>
                            {{ $oQuery->query_text }}
                        </div>
                        @if($oQuery->categories)
                        <div class="request-category">
                            @foreach($oQuery->categories as $category)
                            <span class="note-tag nc-tag-bg-2">{{$category->category_name}}</span>
                            @endforeach
                            {{--<span class="note-tag nc-tag-bg-3">Science</span>--}}
                        </div>
                        @endif
                    </div>
                    <div class="col-2">
                        <div class="float-right">
                            <a href="{{ route('web.home.delete-query', [ 'id' => $oQuery->id]) }}" class="nc-black-btn nc-radius nc-sm-text p-1 data-delete">
                                <i class="fa fa-trash nc-xs-text" aria-hidden="true"></i>
                                @lang('messages.remove')
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
       @endauth
    </div>
</div>

@include('elements.model.delete', [
    'name' => 'query'
])
@endsection
