<div class="note-card-wrapper">
    <div class="note-cover-img">
        <a href="{{ route('web.note.note-detail',['nNoteId' => $oNote->note_id]) }}">
            {!! getNoteTitleImageSectionHtml($oNote) !!}
        </a>
        @if($oNote->is_video_note)
            <span class="video-note-indicator">
                <img class="" src="{{asset('web/img/video-icon.svg')}}" alt="note-cover">
            </span>
        @endif
        <span class="note-visibility-indicator">
            @if($oNote->note_type == \App\Models\Note::PVT)
                <i class="fas fa-lock" data-toggle="tooltip" data-placement="bottom" title="@lang('messages.private_note')" ></i>
            @elseif($oNote->note_type == \App\Models\Note::FO)
                <i class="fas fa-users" data-toggle="tooltip" data-placement="bottom" title="@lang('messages.shared_with_followers')"></i>
            @else
                <!-- <i class="fas fa-globe"></i> -->
            @endif
        </span>
    </div>
    <div class="p-2">
        <span class="note-card-title">
            {{ $oNote->title }}
        </span>
        @if(\Request::route()->getName() == 'web.note.saved-notes' || \Request::route()->getName() == 'web.bucket.get-bucket-notes')
            @if(isset($oNote->bucket_name) && !empty($oNote->bucket_name))
                <span class="nc-xs-text font-weight-bold" data-note-id="{{ $oNote->note_id }}" data-action="add" data-id="{{ $oNote->note_id }}">{{ $oNote->bucket_name }}</span>
            @endif
            <i class="fa fa-arrows note-card-save-btn nc-black-btn btn-move-note" title="Move Note" data-id="{{ $oNote->note_id }}"></i>
            <span class="note-card-save-btn nc-black-btn n-js-fav" data-action="remove" data-id="{{ $oNote->note_id }}">Remove</span>
        @else
            <!-- <span class="note-card-save-btn nc-black-btn js-bucket-modal" data-note-id="{{ $oNote->note_id }}" data-action="add" data-id="{{ $oNote->note_id }}">Save</span> -->
            @if(isset($oNote->bucket_name) && !empty($oNote->bucket_name) && isset($oNote->bn_deleted) && $oNote->bn_deleted != 1)
                <span class="nc-xs-text">Saved</span>
                <div class="nc-xs-text font-weight-bold" data-note-id="{{ $oNote->note_id }}" data-action="add" data-id="{{ $oNote->note_id }}">
                    <span class="fa fa-folder"></span>
                    {{ $oNote->bucket_name }}
                    {{-- Remove save note option disabled for home page --}}
                    {{-- <span class="note-card-save-btn nc-black-btn n-js-fav" data-action="remove" data-id="{{ $oNote->note_id }}">Remove</span> --}}
                </div>
            @else
                <span class="note-card-save-btn nc-black-btn js-bucket-modal" data-note-id="{{ $oNote->note_id }}" data-action="add" data-id="{{ $oNote->note_id }}" data-save-type="home">Save</span>
            @endif
        @endif
    </div>
    <div class="note-card-bottom p-2">
        <span class="author-info">
            <span class="author-thumbnail-wrapper">
                @if(!empty($oNote->user_file_name) || !empty($oNote->user_profile_pic_url))
                    <a href="{{ route('web.user.profile',['id' => $oNote->user_id]) }}">
                        <img class="img-fluid" src="{{ getUserImageUrl($oNote->user_file_name,$oNote->user_profile_pic_url) }}" alt="note-cover">
                    </a>
                @else
                    <a href="{{ route('web.user.profile',['id' => $oNote->user_id]) }}" class="">
                        <span class="user-initials">{{ mb_substr($oNote->first_name, 0, 1) . mb_substr($oNote->last_name, 0, 1) }}</span>
                    </a>
                @endif
            </span>
            <a href="{{ route('web.user.profile',['id' => $oNote->user_id]) }}" data-toggle="tooltip" data-placement="bottom" title="@lang('messages.view_profile')">
            <span class="author-name">
                <span>{{ getUserName($oNote->first_name,$oNote->last_name) }}</span>
                <!-- <div class="note-date">07/07/2020</div> -->
                <div class="note-date">{{ \Carbon\Carbon::parse($oNote->created_at)->format('d/m/Y') }}</div>
            </span>
            </a>
        </span>
        <span class="note-info">
            <div class="note-rating">
                <!-- <span class="fa fa-star nc-black-color"></span>
                <span class="fa fa-star nc-black-color"></span>
                <span class="fa fa-star-o "></span>
                <span class="fa fa-star-o"></span>
                <span class="fa fa-star-o"></span> -->

                @if($oNote->nAvgRating > 0)
                    <?php 
                        for($i=0;$i<5;$i++)
                        {
                            if($i<$oNote->nAvgRating){
                                echo '<span class="fa fa-star nc-black-color"></span>';
                            }
                            else{
                                echo '  <span class="fa fa-star-o "></span>';
                            }   
                        }
                    ?>
                @else
                <!-- <span class="fa fa-star nc-black-color"></span> -->
                <span class="fa fa-star-o "></span>
                <span class="fa fa-star-o"></span>
                <span class="fa fa-star-o"></span>
                <span class="fa fa-star-o"></span>
                <span class="fa fa-star-o"></span>
                @endif
            </div>
            @if($oNote->total_views > 0)
                <div class="note-views">
                    {{ $oNote->total_views }} @lang('messages.views')
                </div>
            @endif
        </span>
    </div>
</div>