<div class="col-12 text-center">
  <h4 class="">
     @lang('messages.edit_your_interest')
  </h4>
  <div class="nc-sm-text"> @lang('messages.interest_info')</div>
  <input type="text" class="animate-search" name="search" placeholder="@lang('messages.search_categories')">

  <div class="category-list-wrapper">
    <ul class="cat-checkbox-wrapper ">

      <li clas="">
        <input type="checkbox" id="myCheckbox1" />
        <label for="myCheckbox1">
          <img class="" src="{{asset('web/img/default-category.svg')}}" alt="category">
          <div class="">Chemestry</div> 
        </label>
      </li>
      <li class="">
        <input type="checkbox" id="myCheckbox2" />
        <label for="myCheckbox2">
          <img class="" src="{{asset('web/img/default-category.svg')}}" alt="category">
          <div class="">Physics</div> 
        </label>
      </li>
      <li class="">
        <input type="checkbox" id="myCheckbox3" />
        <label for="myCheckbox3">
          <img class="" src="{{asset('web/img/default-category.svg')}}" alt="category">
          <div class="">Biology</div> 
        </label>
      </li>

    </ul>
  </div>

</div>

