<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens , Notifiable;

    const YES = 1, NO = 0;
    const INACTIVE = 0, ACTIVE = 1;
    const VERIFIED = 1, UNVERIFIED = 0;
    const MASTER_ADMIN = 'MA', NORMAL_USER = 'NU';
    const NORMAL = 'NORMAL', FB = 'FB' , GMAIL = 'GMAIL';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'first_name', 
        'last_name', 
        'email', 
        'password',
        'mobile',
        'file_name',
        'profile_pic_url',
        'unique_key',
        'refresh_token',
        'refresh_token_expires_at',
        'login_id',
        'login_with',
        'about_me',
        'user_type',
        'verified',
        'login_count',
        'email_verified_at',
        'activated',
        'deleted'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'full_name',
    ];

    public function categories(){
        return $this->hasMany(UserCategory::class)->where('activated','=', 1);
    }

    public function scopeActive($query)
    {
        return $query->where('users.verified',self::ACTIVE)
                    ->where('users.activated',self::YES)
                    ->where('users.deleted',self::NO);
    }

    public function getFullNameAttribute(){
        return $this->first_name.' '.$this->last_name;
    }

    public function buckets(){
        return $this->hasMany(Bucket::class);
    }

    public function notes(){
        return $this->hasMany(Note::class);
    }
}
