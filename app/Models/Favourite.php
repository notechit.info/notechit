<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    const ACTIVE = 1, IN_ACTIVE = 0;

    protected $fillable = [
        'note_id',
        'user_id',
        'activated',
        'deleted'
    ];

    public function scopeActive($query)
    {
        return $query->where('favourites.activated',self::ACTIVE)
                    ->where('favourites.deleted',self::IN_ACTIVE);
    }
}
