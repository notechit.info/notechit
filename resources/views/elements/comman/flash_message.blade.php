<?php
/**
 * Flash Message
 * This is used for display flash messages
 */
?>

@if (Session::has('error'))
    <div class="alert alert-danger custom-flash-message <?php echo (isset($class) ? $class : ''); ?>" onclick=hideFlashMessage()>
        {{ Session::get('error') }}
    </div>
@endif

@if (Session::has('success'))
    <!-- <div class="alert alert-success custom-flash-message" onclick="this.classList.add('d-none');"> -->
    <div class="alert alert-success custom-flash-message <?php echo (isset($class) ? $class : ''); ?>" onclick=hideFlashMessage()>
        {{ Session::get('success') }}
    </div>
@endif
