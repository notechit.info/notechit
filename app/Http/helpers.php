<?php

use Illuminate\Support\Facades\Cache;

    function getUserImageUrl($sImgName,$sProfilePicUrl)
    {
        $sImageUrl = asset('web/img/author.png');
        $priority = 0;
        if(!empty($sImgName))
        {
            $sImageUrl = config('constants.MEDIAURL').'/'.config('constants.USERMEDIAFOLDER').'/'.$sImgName;
            $priority = 1;
        }

        if(!empty($sProfilePicUrl) && $priority == 0)
        {
            $sImageUrl = $sProfilePicUrl;
        }

        return $sImageUrl;
    }

    /**
     * Note cover image URL
     */
    function getNoteCoverImageUrl($sFileName = '')
    {
        $sUrl = asset('web/img/note-cover.png');
        if(!empty($sFileName))
            $sUrl = config('constants.MEDIAURL').'/'.config('constants.NOTECOVERFOLDER').'/'.$sFileName;
        return $sUrl;
    }

    /**
     * create user name based on first name and last name
     */
    function getUserName($sFirstName = '', $sLastName = '')
    {
        return $sFirstName . " ". $sLastName;
    }

    /**
     * Note attachment URL
     */
    function getNoteAttachmentUrl($sFileName = '')
    {
        $sUrl = "";
        if(!empty($sFileName))
            $sUrl = config('constants.MEDIAURL').'/'.config('constants.ATTACHMENTFOLDER').'/'.$sFileName;
        return $sUrl;
    }

    function getCategoryImageUrl($sFileName = '')
    {
        return config('constants.MEDIAURL').'/'.config('constants.CATEGORYFOLDER').'/'.$sFileName;
    }

    /**
     * Function will get tilte class for note listing page
     *
     * @return string $className
     */
    function getTitleClass(){
        $currentIndex = Cache::get('titleClassIndex', 1);
        $classList = config('custom.title_classes');
        $totalClassCount = count($classList);

        if($totalClassCount <= $currentIndex){
            $currentIndex = 1;
        }else{
            $currentIndex = $currentIndex + 1;
        }

        Cache::forever('titleClassIndex', $currentIndex);

        return $classList[$currentIndex - 1];
    }

    function getNoteTitleImageSectionHtml($noteData){
        if($noteData->file_name != ''){
            return '<img class="img-fluid" src="'.getNoteCoverImageUrl($noteData->file_name).'" alt="note-cover">';
        }else{
            return '<div class="'.getTitleClass().'">'.$noteData->title.'</div>';
        }
    }
?>