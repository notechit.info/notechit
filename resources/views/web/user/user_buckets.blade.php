@extends('web.main')

@section('title')
    {{ trans('messages.home_page') }}
@endsection

@section('content')
<div class="container position-relative">
    <div class="row chit-bucket-wrapper">
        <div class="col chit-bucket-title nc-xlg-text font-weight-bold">
            @lang('messages.chit_buckets') : 
            <span id="BucketCount" style="color: red;">{{count($oBuckets)}}</span>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <ul class="bucket-list jsClassBucketList">
                <li class="create-bucket jsClassCreateBucket">
                    <i class="fa fa-plus nc-md-text "></i>
                    <div class="bucket-name">
                        @lang('messages.create_bucket')
                    </div> 
                </li>
                @foreach($oBuckets as $oBucket)
                    <li class="single-bucket " data-id="{{ $oBucket->id }}">
                        <div class="">
                            <img class="bucket-icon" src="{{asset('web/img/bucket-icon.svg')}}" alt="Bucket-icon">   
                            <?php $sClass = "";?>
                            @if($oBucket->default_bucket != 1)
                            <span class="chit-actions float-right">
                                <i class="fa fa-edit nc-xs-text" onclick="editBucketName(this);"
                                    data-toggle="tooltip" data-placement="top" data-id="{{$oBucket->id}}" title="@lang('messages.rename')"></i>
                                <i class="fa fa-trash nc-xs-text js-delete-folder"
                                data-toggle="tooltip" data-folder-id="{{ $oBucket->id }}" data-placement="top" title="@lang('messages.delete')"></i>
                            </span>
                            <?php $sClass = "jsClassBucketName";?>
                            @endif
                        </div>
                        <div class="js-li-bucket bucket-name {{ $sClass }}" data-id="{{ $oBucket->id }}" data-type="edit">
                            {{ $oBucket->bucket_name }}
                        </div>
                        <div class="bucket-items">
                            {{ $oBucket->bucketNotes()->count() }} notes
                        </div>
                    </li>
                @endforeach
                <!-- <li class="single-bucket">
                    <div class="">
                        <img class="bucket-icon" src="{{asset('web/img/bucket-icon.svg')}}" alt="Bucket-icon">
                        <span class="chit-actions float-right">
                            <i class="fa fa-edit nc-xs-text" onclick= "editBucketName(this)";
                                data-toggle="tooltip" data-placement="top" title="@lang('messages.rename')"></i>
                            <i class="fa fa-trash nc-xs-text"
                            data-toggle="tooltip" data-placement="top" title="@lang('messages.delete')"></i>
                        </span>
                    </div>
                    <div class="bucket-name jsClassBucketName">
                        Physics Notes
                    </div>
                    <div class="bucket-items">
                        20 notes
                    </div>
                </li> -->
            </ul>
        </div>
    </div>
    <div class="jsClassBucketNotesList bucket-notes-list ">
        <div class=" mb-3 bucket-name-title">
            <span class=" jsClassBackBtn mr-1 bucket-back-btn" >
                <i class="fas fa-chevron-left"></i>
            </span>
            <span class="jsClassPutBucketName nc-lg-text">Bucket NAme</span>
        </div>
        <div class="bucket-notes row"></div>
    </div>
    <div class="add-note-sticky-btn text-center">
      <a href="{{route('web.note.add-note')}}" class="d-inline-block">
          <i class="fa fa-plus nc-xs-text"></i>
          @lang('messages.add_chit')
      </a>
    </div>
</div>
<div class="modal fade md-modal" id="moveBucketListModal" tabindex="-1" role="dialog" aria-labelledby="moveBucketListModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="nc-xlg-text modal-title text-center ">@lang('messages.move_note_to')</div>
        <div class="bucketListPopupWrapper">
            <ul class="moveBucketListUl">   
            </ul>
        </div>  
        <button id="moveNote" class="nc-black-btn nc-register-btn">@lang('messages.save')</button>
      </div>
    </div>
  </div>
</div>

@include('web.notes._bucket_modal')

<script>
    $(".jsClassCreateBucket").click(function(){
        var sNewBucketHtml = ' <li class="single-bucket new-added-item" style="display:none;"> <div class=""><img class="bucket-icon" src="{{asset("web/img/bucket-icon.svg")}}" alt="Bucket-icon">'+
                                '<span class="chit-actions float-right">  <i class="fa fa-edit nc-xs-text" onclick="editBucketName(this);" data-toggle="tooltip" data-placement="top" title="Rename"></i> <i class="fa fa-trash nc-xs-text js-delete-folder" data-toggle="tooltip" data-placement="top" title="Delete"></i> </span>' +
                               ' </div> <input type="text" class="form-control bucket-name-input jsClassBucketName mt-2" value="Untitled Bucket" onblur = "saveBucket(this)"/> <div class="bucket-items">  start adding notes </div></li>';

        $(sNewBucketHtml).insertAfter('.jsClassBucketList li:nth-child(2)').fadeIn('slow');
        $(".jsClassBucketName").select();
        $('[data-toggle="tooltip"]').tooltip();

        setTimeout(function(){
            $(".single-bucket").removeClass('new-added-item');
        }, 3000);
    });

    function saveBucket(input,id=''){
        var bucketName = '';
        if(input.value){
            var sHtml= "<div class='bucket-name jsClassBucketName'>"+input.value+"</div>";
            $( input ).replaceWith(sHtml);
            bucketName = input.value;
        }
        else{
            var sHtml= "<div class='bucket-name jsClassBucketName'>Untitled Bucket</div>";
            $( input ).replaceWith(sHtml);
            bucketName = 'Untitled Bucket';
        }
        _createBucket(bucketName,id);
    }

    function _createBucket(sBucketName,id)
    {
        $.ajax({
            method : "POST",
            url: "{{ route('web.bucket.create-bucket') }}",
            data : {
                sBucketName : sBucketName,
                _token : "{{ csrf_token() }}",
                id : id
            },
            cache: false,
            success: function(response){
                if(response.status == "1"){
                    $('#BucketCount').html(response.data.total);
                }
                if(response.data.type == "Add"){
                    $('.single-bucket[style=""]').attr('data-id', response.data.id);
                    $('.single-bucket[style=""]').children('div').children('span').children('i').attr('data-id', response.data.id);
                    $('.single-bucket[style=""]').children('div').children('span').children('i').attr('data-folder-id', response.data.id);
                    $('.single-bucket[style=""]').removeAttr('style');
                    showToast('success on create bucket');
                }else{
                    showToast('success on edit bucket');
                }
            },error : function(error){ 
                console.log('Error on create bucket');
            }
        });
        return false;
    }

    function editBucketName(ele){
        var id = $(ele).data('id');
        if(id == undefined)
        {
            id = 0;
        }

        var replaceEle = $(ele).parents(".single-bucket").find(".jsClassBucketName");
        var oldBucketValue = $.trim(replaceEle.text());
        var replaceEleWith = '<input type="text" class="form-control jsClassBucketName mt-2" value="'+oldBucketValue+'" onblur = "saveBucket(this,'+id+')"/>';
        $( replaceEle ).replaceWith(replaceEleWith);
        $(".jsClassBucketName").select();
    }

    $('body').on("keypress", '.jsClassBucketName', function(e) {
        var keyC = e.keyCode;
        if (keyC == 13) {
            $(this).blur();
        }
    });

    $('body').on("dblclick", '.jsClassBucketName', function(e) {
        console.log("key poresses");
        editBucketName(this);
    });

    // ****  Back button ***
    $('body').on("click", '.jsClassBackBtn', function(e) {
       $(".jsClassBucketNotesList").hide("fast");
       $(".jsClassBucketNotesList").find('.bucket-notes').html("");
    });

    /**
     * Get bucket notes
     */
    $('body').on("click", '.js-li-bucket', function(e) {
        var bucket_id = $(this).attr('data-id');
        _getBucketNotes(bucket_id);
        $( ".jsClassBucketNotesList" ).show( "fast" );
        var sBucketName = $(this).html();
        $(".jsClassPutBucketName").text(sBucketName);
    });

    function _getBucketNotes(bucket_id)
    {
        $.ajax({
            method : "GET",
            url: "{{ route('web.bucket.get-bucket-notes') }}",
            data : {
                bucket_id : bucket_id,
            },
            cache: false,
            success: function(response){
                $('.bucket-notes').html(response.sHtml);
            },error : function(error){ 
                console.log('Error on create bucket');
            }
        });
        return false;
    }

    /**
     * Get bucket notes
     */
    $('body').on("click", '.js-delete-folder', function(e) {
        var nFolderId = $(this).attr('data-folder-id');
        _deleteFolder(nFolderId);
    });

    function _deleteFolder(nFolderId) {
        swal({
            title: "Are you sure you want to delete?",
            text: "You will not be able to recover notes of this folder!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {

                $.ajax({
                    method : "POST",
                    url: "{{ route('web.bucket.delete-bucket') }}",
                    data : {
                        _token : "{{ csrf_token() }}",
                        nFolderId : nFolderId
                    },
                    cache: false,
                    success: function(response){
                        swal("Folder has been deleted!",{
                            icon: "success",
                        }).then(function() {
                            window.location.reload();
                        });
                    },error : function(error){ 
                        console.log('Error on delete bucket');
                    }
                });
                return false;
            } else {
                swal("Your folder is safe!");
            }
        });
    }

    $(document).on('click', '.btn-move-note', function(){
        note = $(this).data('id');

        $.ajax({
            method : "GET",
            url: "{{ route('web.bucket.get-user-buckets-list-move') }}",
            data : { note : note },
            cache: false,
            success: function(response){
                $('.moveBucketListUl').html(response.sHtml);
                $('#moveBucketListModal').modal('show');
                $('#moveNote').attr("data-note", response.note);
                $('#moveNote').attr("data-old-bucket", response.old_bucket);
            },error : function(error){ 
                console.log('Bucket Move Popup Folders Error');
            }
        });
    });

    function selectForMoveBucket(ele,bucketId){
        $(ele).siblings().removeClass('active');
        $(ele).addClass('active');
        $('#moveNote').attr("data-bucket", bucketId);
    }

    $('#moveNote').click(function(){
        note = $(this).attr('data-note');
        oldBucket = $(this).attr('data-old-bucket');
        bucket = $(this).attr('data-bucket');

        if(note != '' && oldBucket != '' && bucket != ''){
            $.ajax({
                method : "POST",
                url: "{{ route('web.bucket.move-note-bucket') }}",
                data : { 
                    _token : "{{ csrf_token() }}",
                    note : note, 
                    old_bucket : oldBucket, 
                    bucket : bucket 
                },
                cache: false,
                success: function(response){
                    if(response.status == 1){
                        $('#moveBucketListModal').modal('hide');
                        swal(response.message,{icon: "success"})
                        .then(function() {
                            window.location.reload();
                        });
                    }
                },error : function(error){ 
                    console.log('Bucket Move Popup Folders Error');
                }
            });
 
        }
    });
</script>
@endsection