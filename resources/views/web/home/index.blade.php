@extends('web.custom_layout')

@section('title')
    {{ trans('messages.home_page') }}
@endsection

@section('header')
    @include('web.primary_header')
    @include('web.login_popup')

@endsection

@section('content')

  @if(!\Auth::user())
    @include('web.hero_section')
  @else
    <div class="add-note-sticky-btn text-center">
      <a href="{{route('web.note.add-note')}}" class="d-inline-block">
        <i class="fa fa-plus nc-xs-text"></i>
        @lang('messages.add_chit')
      </a>
    </div>
  @endif

  @include('web.secondary_header')

  <div class="container pb-5" >
    <!-- <div class="" onClick=showAjaxLoader('body');>show loader</div> -->

    @if(isset($showCategorySelection) && $showCategorySelection)
      <div class="row intro-text">
        <div class="col-md-12 text-center">
          <div class="mb-3">
            <img src="{{asset('web/img/telescope_1.svg')}}" alt="">
          </div>
           
            Checkout Notes From Your <span class="nc-vibrant-color">Interested </span> Category
            <div id="showIntrestButton" class="nc-light-color cursor-pointor mt-3"> <u> Edit My Interests </u></div>
              <!-- <button type="button" class="btn btn-secondary btn-sm float-right" >Show me</button> -->
        </div>
      </div>
    @endif

      <div id="explore_notes" class="row scrolling-pagination">

      <!-- include('web.notes.notes') -->

      @php
        $i = 1;
      @endphp
      @foreach($oNotes as $oNote)
        <div class="col-lg-4 col-md-6 col-sm-12 single-note-card note-card-bg-{{$i}}">
            @php
            if($i > 11){
                $i=1;
            }
            else{
                $i++;
            }
            @endphp
            @include('web.notes.single_note_card')
        </div>

      @endforeach

      <!--<div class="col-lg-4 col-md-6 col-sm-12 note-card-bg-1">
          include('web.notes.single_note_card')
      </div>-->

    </div>


      <div class="offset-md-5 clearfix">
          {{ $oNotes->withQueryString()->links() }}
      </div>


  </div>

  @include('web.notes._bucket_modal')

  @include('web.area_of_interest_popup')

  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jscroll/2.4.1/jquery.jscroll.min.js"></script> -->

  <script src="https://unpkg.com/infinite-scroll@4/dist/infinite-scroll.pkgd.min.js"></script>

  <script>

    $('ul.pagination').hide();

    $(function() {
        /*$('.scrolling-pagination').jscroll({
            debug: true,
            autoTrigger: true,
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.scrolling-pagination',
            callback: function() {
                $('ul.pagination').remove();
            }
        });*/

        // $('div.jscroll-inner').addClass('col-sm-12');

        $('.scrolling-pagination').infiniteScroll({
            // options
            path: '.pagination li.active + li a',
            append: '.single-note-card',
            history: false,
        });
    });

    // $('#interestModal').modal('toggle');

    // ===== Make Secondary tab bar sticky on Scroll for home page Start =====
    window.onscroll = function() {myFunction()};

    var navbar = document.getElementById("secondary_navbar");
    var sticky = navbar.offsetTop;

    function myFunction() {
      if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
      } else {
        navbar.classList.remove("sticky");
      }
    }
    // ===== Make Secondary tab bar sticky on Scroll for home page END =====

    // ===== Add smooth Scrolling for home page Explore button START =====
    $(document).ready(function(){
        // Add smooth scrolling to all links
        $("a").on('click', function(event) {

          // Make sure this.hash has a value before overriding default behavior
          if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
              scrollTop: $(hash).offset().top
            }, 800, function(){

              // Add hash (#) to URL when done scrolling (default click behavior)
              window.location.hash = hash;
            });
          } // End if
        });
      });
    // ===== Add smooth Scrolling for home page Explore button END =====

    $('#showIntrestButton').click(function(){
      $('#interestModal').modal('show');
    });
    @if(isset($isFirstLogin) && $isFirstLogin)
      $('#interestModal').modal('show');
    @endif
  </script>
@if(Session::has('ask_login') && Session::get('ask_login'))
    <script>
        $('#loginModal').modal('show');    
        </script>
@endif
@endsection