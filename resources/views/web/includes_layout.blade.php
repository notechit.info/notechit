<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ trans("messages.app_name") }} - @yield('title')</title>
        
        @include('web.includes')
        
       
        


        @yield('custom-header-css')
        @yield('custom-header-js')

    </head>
    <body>
        
     

        @yield('content')
        
        <script>
        // ===== common js initializations =====
        // ===== Bootstrap Tooltip show =====
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
        </script>
    
    </body>
</html>
