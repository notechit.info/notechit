<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BucketNotes extends Model
{
    const IN_ACTIVE = 0 , ACTIVE = 1;

    protected $fillable = [
        'note_id',
        'bucket_id',
        'user_id',
        'activated',
        'deleted'
    ];

    public function scopeActive($query)
    {
        return $query->where('bucket_notes.activated',self::ACTIVE);
    }

    /**
     * Function will add note to default bucket
     *
     * @param int $noteid
     * @param int $bucketId
     * @param int $userId
     * @return void
     */
    public static function addNoteToDefaultBucket($noteid, $bucketId, $userId){
        return self::create([
            'note_id'       =>  $noteid,
            'bucket_id'     =>  $bucketId,
            'user_id'       =>  $userId
        ]);
    }
}
