<div class="container-fluid hero-bg-image">
    <div class="row">
        <div class="col-lg-7 col-md-12 sml-device-chit-container">
            <div class="chits-container">
                <div class="chit chit-5" data-aos="fade-up" data-aos-duration="500"> <img src="{{ asset('web/img/chit-pink.png') }}" alt=""/> </div>
                <div class="chit chit-4" data-aos="fade-up" data-aos-duration="1000"> <img src="{{ asset('web/img/chit-recepi.png') }}" alt=""/> </div>
                <!-- <div class="chit chit-1" data-aos="fade-up" data-aos-duration="1100"> <img src="{{ asset('web/img/chit-blue.svg') }}" alt=""/> </div> -->
                <div class="chit chit-1" data-aos="fade-up" data-aos-duration="1100"> <img src="{{ asset('web/img/chit-blue.png') }}" alt=""/> </div>
                <div class="chit chit-2" data-aos="fade-up" data-aos-duration="1200"> <img src="{{ asset('web/img/chit-paper.png') }}" alt=""/> </div>
                <div class="chit chit-3" data-aos="fade-up" data-aos-duration="1300"> <img src="{{ asset('web/img/chit-greenBlue.png') }}" alt=""/> </div>
                <div class="chit chit-6" data-aos="fade-up" data-aos-duration="1400"> <img src="{{ asset('web/img/chit-orange.png') }}" alt=""/> </div>
                <div class="chit chit-9" data-aos="fade-up" data-aos-duration="600"> <img src="{{ asset('web/img/chit-purple.png') }}" alt=""/> </div>
                <div class="chit chit-11" data-aos="fade-up" data-aos-duration="500" > <img src="{{ asset('web/img/chit-graph.png') }}" alt=""/> </div>
                <div class="chit chit-10" data-aos="fade-up" data-aos-duration="400"> <img src="{{ asset('web/img/chit-sanskrit.png') }}" alt=""/> </div>
                <div class="chit chit-12" data-aos="fade-up" data-aos-duration="300"> <img src="{{ asset('web/img/chit-orange-bottom.png') }}" alt=""/> </div>
                <div class="chit chit-8" data-aos="fade-up" data-aos-duration="200"> <img src="{{ asset('web/img/chit-two.png') }}" alt=""/> </div>
                <div class="chit chit-7" data-aos="fade-up" data-aos-duration="100"> <img src="{{ asset('web/img/chit-map.svg') }}" alt=""/> </div>
                <div class="chit chit-13" data-aos="fade-up" data-aos-duration="700"> <img src="{{ asset('web/img/chit-photos.png') }}" alt=""/> </div>
            </div>
        </div>
        <div class="col-lg-5 col-md-12 sml-device-text-container">
            <h1 class="hero-text">
                A Place to <br>
                <span  class="nc-vibrant-color typewrite font-weight-bold" data-period="2000" data-type='[ "SEARCH ", "SAVE ", "SHARE" ]'>
                    <span class="wrap"></span>
                </span>
                Notes
            </h1>
            <a href="#explore_notes" class="explore-btn">
                Explore Notes
                <i class="fa fa-chevron-down nc-xs-text ml-2" aria-hidden="true"></i>
            </a>
            
        </div>
       

    
    </div>

</div>
<div class="col-12 text-center know-more-text py-3">
    <a href="{{route('web.home.about-us')}}" class="my-1 py-2 d-block  nc-lg-text">@lang('messages.know_more')</a>

    <a href="#" class="signup-btn my-3" data-toggle="modal" data-target="#loginModal">
        Join Notechit
        <i class="fa fa-chevron-right nc-xs-text ml-2" aria-hidden="true"></i>
    </a>
   
</div>

<script>
    // ===== JS for Typing Effect Start =====
var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
        }

        setTimeout(function() {
        that.tick();
        }, delta);
    };

    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
              new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
        document.body.appendChild(css);
    };
    // ===== JS for Typing Effect END =====

    AOS.init({
    duration: 1200,
 }); 

</script>