<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->string('mobile')->nullable();
            $table->text('unique_key')->nullable();
            $table->string('file_name')->nullable()->comment('for Profile Picture');
            $table->text('profile_pic_url')->nullable()->comment('For FB and Gmail');
            $table->text('refresh_token')->nullable();
            $table->text('about_me')->nullable();
            $table->dateTime('refresh_token_expires_at')->nullable();
            $table->text('login_id')->nullable();
            $table->enum('login_with',['NORMAL','FB','GMAIL'])->default('NORMAL')->comment('NORMAL:Normal Login, FB : Facebook Login , GMAIL : Gmail Login ');
            $table->enum('user_type',['MA','NU'])->default('NU')->comment('MA:Master Admin, NU : Normal User');
            $table->tinyInteger('verified')->default(0);
            $table->timestamp('email_verified_at')->nullable();
            $table->integer('login_count')->nullable();
            $table->tinyInteger('activated')->default(1);
            $table->tinyInteger('deleted')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
