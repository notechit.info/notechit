@extends('web.custom_layout')

@section('title')
    {{ trans('messages.home_page') }}
@endsection

@section('content')
  <div class="container-fluid">
      <div class="row error-note-top-margin">
        <div class="col-12 text-center">
          <a href="{{ route('web.home.index') }}">
            <img src="{{asset('web/img/logo.png')}}" class="d-inline-block align-top" alt="">
            <div>Notechit</div>
          </a>
        </div>
          <div class=" text-center col-md-4 offset-md-4 error-note-wrapper animate__animated animate__swing">
          <img src="{{ asset('web/img/paperclip.svg') }}" alt="paperclip" class="error-clip"/>
            <div class="error-code">
              <span>4</span>
              <span><i class="far fa-frown"></i></span>
              <span>4</span>
            </div>
            <div class="error-message">
              @lang('messages.page_not_found_msg')
            </div>
            <a href="{{ route('web.home.index') }}" class="error-cta">
              @lang('messages.back_to_home_page')
            
            </a>
          
          </div>
      </div>
  </div>
  
@endsection